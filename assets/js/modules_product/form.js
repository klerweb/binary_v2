$('#product_lot_date').daterangepicker({
	format: 'DD-MM-YYYY',
	singleDatePicker: true,
	calender_style: "picker_4"
});

$("#tblLot").DataTable({
	responsive: true,
	bFilter: false,               
	bLengthChange: false,
	aoColumnDefs: [ { bSortable: false, aTargets: [ ] } ],
	order: [[ 0, "asc" ]]
});

$("#frmProduct").validate();
$("#frmLot").validate({
	rules: {
		product_lot_amount: {
			required: true,
			digits: true
		},
		product_lot_price: {
			required: true,
			number: true
		}
	}
});

$("#btnLot").on("click", function() {
	if($("#frmLot").valid())
	{
		$.post(base_url+"product/save_lot", $("#frmLot").serialize(), function(data) {
			switch (data)
			{
				case 'success_add': data = 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ';
					break;
				case 'error_add': data = 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้';
					break;
				case 'success_edit': data = 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ';
					break;
				case 'error_edit': data = 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้';
					break;
			}
			
			alert(data);
			
			load_table()
			clear_modal();
			
			 $("#modal-lot").modal("toggle");
		});
	}
});

$("#btnLotCancel").on("click", function() {
	$("#modal-lot").modal("toggle");
});

function load_table()
{
	$("#divLot").load(base_url+"product/lot/"+$("#product_id").val());
}

$("#modal-lot").on('hidden.bs.modal', function () {
	clear_modal();
});

function clear_modal()
{
	$("#product_lot_id").val("");
	$("#product_lot_date").val(date_now);
	$("#product_lot_amount").val("0");
	$("#product_lot_price").val("0.00");
	$("#product_lot_desc").val("");
}

function edit_lot(id)
{
	$("#product_lot_id").val(id);
	$("#product_lot_date").val($("#lot_date_"+id).val());
	$("#product_lot_amount").val($("#lot_amount_"+id).val());
	$("#product_lot_price").val($("#lot_price_"+id).val());
	$("#product_lot_desc").val($("#lot_desc_"+id).val());
	
	 $("#modal-lot").modal("show");
}

function del_lot(type, id)
{
	if(confirm('คุณต้องการลบข้อมูล?'))
	{
		$.post(base_url+"product/del_lot", {type: type, id: id}, function(data) {
			
			switch (data)
			{
				case 'success_del': data = 'ลบข้อมูลเรียบร้อยแล้วค่ะ';
					break;
				case 'error_del': data = 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้';
					break;
			}
			
			alert(data);
			
			load_table();
		});
	}
}
$("#chkAll").on("ifChecked", function(event){
	$(".chk").iCheck("check");
});

$("#chkAll").on("ifUnchecked", function(){ 
	$(".chk").iCheck("uncheck");
});

$("#tblEmployee").DataTable({
	responsive: true,
	aoColumnDefs: [ { bSortable: false, aTargets: [ 0, 5 ] } ],
	order: [[ 1, "asc" ]]
});
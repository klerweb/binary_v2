$('#employee_startdate').daterangepicker({
	format: 'DD-MM-YYYY',
	singleDatePicker: true,
	calender_style: "picker_4"
});

$("#frmEmployee").validate({
	rules: {
		employee_email: {
			email: true
		},
		employee_password_confirm: {
			required: true,
			equalTo: "#employee_password"
		}
	}
});
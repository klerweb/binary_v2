$("#frmPass").validate({
	rules: {
		employee_password_confirm: {
			required: true,
			equalTo: "#employee_password"
		}
	}
});
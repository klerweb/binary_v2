$("#employee_id, #customer_id").select2();
 
$(function () {
	$("#frmServiceRequest").parsley().on("field:validated", function() {
		var valid = $(".parsley-error").length === 0;
		$(".bs-callout-info").toggleClass("hidden", !valid);
		$(".bs-callout-warning").toggleClass("hidden", valid);
	})
	.on("#frmServiceRequest:submit", function() {
		return false;
	});
});

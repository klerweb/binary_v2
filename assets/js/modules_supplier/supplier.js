$("#chkAll").on("ifChecked", function(event){
	$(".chk").iCheck("check");
});

$("#chkAll").on("ifUnchecked", function(){ 
	$(".chk").iCheck("uncheck");
});

$("#tblSupplier").DataTable({
	responsive: true,
	aoColumnDefs: [ { bSortable: false, aTargets: [ 0, 4 ] } ],
	order: [[ 1, "asc" ]]
});
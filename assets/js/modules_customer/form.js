$("#tblBranch").DataTable({
	responsive: true,
	bFilter: false,               
	bLengthChange: false,
	aoColumnDefs: [ { bSortable: false, aTargets: [ ] } ],
	order: [[ 0, "asc" ]]
});

$("#frmCustomer").validate();
$("#frmBranch").validate();

$("#btnBranch").on("click", function() {
	if($("#frmBranch").valid())
	{
		$.post(base_url+"customer/save_branch", $("#frmBranch").serialize(), function(data) {
			switch (data)
			{
				case 'success_add': data = 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ';
					break;
				case 'error_add': data = 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้';
					break;
				case 'success_edit': data = 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ';
					break;
				case 'error_edit': data = 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้';
					break;
			}
			
			alert(data);
			
			load_table()
			clear_modal();
			
			 $("#modal-branch").modal("toggle");
		});
	}
});

$("#btnBranchCancel").on("click", function() {
	$("#modal-branch").modal("toggle");
});

function load_table()
{
	$("#divBranch").load(base_url+"customer/branch/"+$("#customer_id").val());
}

$("#modal-branch").on('hidden.bs.modal', function () {
	clear_modal();
});

function clear_modal()
{
	$("#customer_branch_id").val("");
	$("#customer_branch_name").val("");
	$("#customer_branch_contact").val("");
	$("#customer_branch_tel").val("");
	$("#customer_branch_email").val("");
	$("#customer_branch_address").val("");
	$("#customer_branch_latitude").val("");
	$("#customer_branch_longitude").val("");
}

function edit_branch(id)
{
	$("#customer_branch_id").val(id);
	$("#customer_branch_name").val($("#branch_name_"+id).val());
	$("#customer_branch_contact").val($("#branch_contact_"+id).val());
	$("#customer_branch_tel").val($("#branch_tel_"+id).val());
	$("#customer_branch_email").val($("#branch_email_"+id).val());
	$("#customer_branch_address").val($("#branch_address_"+id).val());
	$("#customer_branch_latitude").val($("#branch_latitude_"+id).val());
	$("#customer_branch_longitude").val($("#branch_longitude_"+id).val());
	
	 $("#modal-branch").modal("show");
}

function del_branch(type, id)
{
	if(confirm('คุณต้องการลบข้อมูล?'))
	{
		$.post(base_url+"customer/del_branch", {type: type, id: id}, function(data) {
			
			switch (data)
			{
				case 'success_del': data = 'ลบข้อมูลเรียบร้อยแล้วค่ะ';
					break;
				case 'error_del': data = 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้';
					break;
			}
			
			alert(data);
			
			load_table();
		});
	}
}
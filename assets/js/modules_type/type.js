$("#chkAll").on("ifChecked", function(event){
	$(".chk").iCheck("check");
});

$("#chkAll").on("ifUnchecked", function(){ 
	$(".chk").iCheck("uncheck");
});

$("#tblType").DataTable({
	responsive: true,
	aoColumnDefs: [ { bSortable: false, aTargets: [ 0, 3 ] } ],
	order: [[ 1, "asc" ]]
});
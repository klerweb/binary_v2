/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : binary_db

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-07-12 01:16:47
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `binary_catogory`
-- ----------------------------
DROP TABLE IF EXISTS `binary_catogory`;
CREATE TABLE `binary_catogory` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `category_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `category_parent` int(11) NOT NULL,
  `catogory_visible` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `catogory_createby` int(11) DEFAULT NULL,
  `catogory_createdate` datetime DEFAULT NULL,
  `catogory_updateby` int(11) DEFAULT NULL,
  `catogory_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_catogory
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_customer`
-- ----------------------------
DROP TABLE IF EXISTS `binary_customer`;
CREATE TABLE `binary_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `customer_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `customer_lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `customer_tel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `customer_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_visible` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `customer_createby` int(11) DEFAULT NULL,
  `customer_createdate` datetime DEFAULT NULL,
  `customer_updateby` int(11) DEFAULT NULL,
  `customer_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_customer
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_customer_address`
-- ----------------------------
DROP TABLE IF EXISTS `binary_customer_address`;
CREATE TABLE `binary_customer_address` (
  `customer_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `customer_address_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `customer_address_visible` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `customer_address_createby` int(11) DEFAULT NULL,
  `customer_address_createdate` datetime DEFAULT NULL,
  `customer_address_updateby` int(11) DEFAULT NULL,
  `customer_address_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_customer_address
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_department`
-- ----------------------------
DROP TABLE IF EXISTS `binary_department`;
CREATE TABLE `binary_department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_department
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_employee`
-- ----------------------------
DROP TABLE IF EXISTS `binary_employee`;
CREATE TABLE `binary_employee` (
  `employee_id` int(11) NOT NULL,
  `employee_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employee_lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employee_startdate` date NOT NULL,
  `department_id` int(11) NOT NULL,
  `employee_position` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employee_tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `employee_password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `employee_visible` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `employee_createby` int(11) DEFAULT NULL,
  `employee_createdate` datetime DEFAULT NULL,
  `employee_updateby` int(11) DEFAULT NULL,
  `employee_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_employee
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_employee_module`
-- ----------------------------
DROP TABLE IF EXISTS `binary_employee_module`;
CREATE TABLE `binary_employee_module` (
  `employee_module_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `employee_module_createby` int(11) DEFAULT NULL,
  `employee_module_createdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_employee_module
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_expenses`
-- ----------------------------
DROP TABLE IF EXISTS `binary_expenses`;
CREATE TABLE `binary_expenses` (
  `expenses_id` int(11) NOT NULL AUTO_INCREMENT,
  `expenses_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expenses_date` date NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `expenses_amount` double(10,2) DEFAULT NULL,
  `expenses_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expenses_visible` enum('0','1') COLLATE utf8_unicode_ci DEFAULT NULL,
  `expenses_createby` int(11) DEFAULT NULL,
  `expenses_createdate` datetime DEFAULT NULL,
  `expenses_updateby` int(11) DEFAULT NULL,
  `expenses_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`expenses_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_expenses
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_job`
-- ----------------------------
DROP TABLE IF EXISTS `binary_job`;
CREATE TABLE `binary_job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `job_startdate` date NOT NULL,
  `job_enddate` date NOT NULL,
  `job_type` enum('employee','subcontract') COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `subcontract_id` int(11) DEFAULT NULL,
  `job_summarry` text COLLATE utf8_unicode_ci,
  `customer_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `job_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_desc` text COLLATE utf8_unicode_ci,
  `job_visible` enum('0','1','2','3','4') COLLATE utf8_unicode_ci NOT NULL COMMENT '0 = delete / 1 = waiting / 2 = in progress / 3 = success / 4 = reject',
  `job_createby` int(11) DEFAULT NULL,
  `job_createdate` datetime DEFAULT NULL,
  `job_updateby` int(11) DEFAULT NULL,
  `job_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_job
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_module`
-- ----------------------------
DROP TABLE IF EXISTS `binary_module`;
CREATE TABLE `binary_module` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_module
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_po`
-- ----------------------------
DROP TABLE IF EXISTS `binary_po`;
CREATE TABLE `binary_po` (
  `po_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_date` date NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `po_type` enum('in','out') COLLATE utf8_unicode_ci NOT NULL,
  `po_source` enum('customer','supplier','subcontract') COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `subcontract_id` int(11) DEFAULT NULL,
  `po_amount` double(10,2) NOT NULL,
  `po_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_visible` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `po_createby` int(11) DEFAULT NULL,
  `po_createdate` datetime DEFAULT NULL,
  `po_updateby` int(11) DEFAULT NULL,
  `po_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`po_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_po
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_product`
-- ----------------------------
DROP TABLE IF EXISTS `binary_product`;
CREATE TABLE `binary_product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `catogory_id` int(11) NOT NULL,
  `product_visible` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `product_createby` int(11) DEFAULT NULL,
  `product_createdate` datetime DEFAULT NULL,
  `product_updateby` int(11) DEFAULT NULL,
  `product_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_product
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_product_photo`
-- ----------------------------
DROP TABLE IF EXISTS `binary_product_photo`;
CREATE TABLE `binary_product_photo` (
  `product_photo_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_photo_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_photo_createby` int(11) DEFAULT NULL,
  `product_photo_createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`product_photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_product_photo
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_product_stock`
-- ----------------------------
DROP TABLE IF EXISTS `binary_product_stock`;
CREATE TABLE `binary_product_stock` (
  `product_stock_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_stock_num` int(11) NOT NULL,
  `product_stock_type` enum('a','d','s','h','c','sr') COLLATE utf8_unicode_ci NOT NULL COMMENT 'a = เพิ่ม / d = ลบ / s = ขาย / h = จอง / c = ยกเลิก / sr = service requesr',
  `service_product_id` int(11) DEFAULT NULL,
  `product_stock_createby` int(11) DEFAULT NULL,
  `product_stock_createdate` datetime DEFAULT NULL,
  `product_stock_updateby` int(11) DEFAULT NULL,
  `product_stock_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`product_stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_product_stock
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_quotation`
-- ----------------------------
DROP TABLE IF EXISTS `binary_quotation`;
CREATE TABLE `binary_quotation` (
  `quotation_id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_date` date NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `quotation_type` enum('in','out') COLLATE utf8_unicode_ci NOT NULL,
  `quotation_source` enum('customer','supplier','subcontract') COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `subcontract_id` int(11) DEFAULT NULL,
  `quotation_amount` double(10,2) NOT NULL,
  `quotation_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quotation_visible` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `quotation_createby` int(11) DEFAULT NULL,
  `quotation_createdate` datetime DEFAULT NULL,
  `quotation_updateby` int(11) DEFAULT NULL,
  `quotation_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`quotation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_quotation
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_service`
-- ----------------------------
DROP TABLE IF EXISTS `binary_service`;
CREATE TABLE `binary_service` (
  `service_id` int(11) NOT NULL,
  `service_code` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_date` date NOT NULL,
  `service_desc` text COLLATE utf8_unicode_ci,
  `service_summary` text COLLATE utf8_unicode_ci,
  `service_type_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_address_id` int(11) NOT NULL,
  `service_visible` enum('0','1','2','3','4','5','6') COLLATE utf8_unicode_ci NOT NULL COMMENT '0 = ลบจาก request / 1 = request / 2 = service / 3 = ยกเลิก  / 4 = ลบจาก service / 5 = success /  6 = ยกเลิก',
  `service_createby` int(11) DEFAULT NULL,
  `service_createdate` datetime DEFAULT NULL,
  `service_updateby` int(11) DEFAULT NULL,
  `service_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_service
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_service_product`
-- ----------------------------
DROP TABLE IF EXISTS `binary_service_product`;
CREATE TABLE `binary_service_product` (
  `service_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `service_product_amount` int(11) NOT NULL,
  `service_product_price` double(10,2) NOT NULL,
  `service_prodcut_visible` enum('0','1') COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_product_createby` int(11) DEFAULT NULL,
  `service_product_createdate` datetime DEFAULT NULL,
  `service_product_updateby` int(11) DEFAULT NULL,
  `service_product_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`service_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_service_product
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_service_type`
-- ----------------------------
DROP TABLE IF EXISTS `binary_service_type`;
CREATE TABLE `binary_service_type` (
  `service_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`service_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_service_type
-- ----------------------------
INSERT INTO `binary_service_type` VALUES ('1', 'งานแจ้งบริการ');
INSERT INTO `binary_service_type` VALUES ('2', 'งานบริการหลังการขาย');
INSERT INTO `binary_service_type` VALUES ('3', 'งานขาย');
INSERT INTO `binary_service_type` VALUES ('4', 'งานโครงการ');

-- ----------------------------
-- Table structure for `binary_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `binary_sessions`;
CREATE TABLE `binary_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_sessions
-- ----------------------------
INSERT INTO `binary_sessions` VALUES ('e28632cf14201883acf19d3aedbe7f653d686f8c', '::1', '1498979958', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439383937393935353B);
INSERT INTO `binary_sessions` VALUES ('31b8f328699bdc307880507b24541d169e384efe', '::1', '1498980111', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439383938303130383B);
INSERT INTO `binary_sessions` VALUES ('d01e4ac53d8ebd781ec6d2a69642086e83aa0e0c', '::1', '1499025442', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393032353434323B);
INSERT INTO `binary_sessions` VALUES ('5530b29894356b697b9d67ad401d1b20ff3a8de0', '::1', '1499278253', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393237383139313B);
INSERT INTO `binary_sessions` VALUES ('68257a367da65bdeece3093088a32e23b1633ab1', '::1', '1499278913', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393237383838353B);
INSERT INTO `binary_sessions` VALUES ('985dfa759ff7ffcbaa9cbe6c99f4818c11bf6c00', '::1', '1499279479', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393237393432343B);
INSERT INTO `binary_sessions` VALUES ('b599f2fd4ecc8da7a0faacc00d666f0c1ecfcfa2', '::1', '1499280070', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393237393834373B);
INSERT INTO `binary_sessions` VALUES ('2dabb87efa45027d34b8e77d0f84e4d29087ed9c', '::1', '1499280223', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393238303136303B);
INSERT INTO `binary_sessions` VALUES ('e293f4e0cfcba35aa32486c677691796ca2b1c4b', '::1', '1499281515', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393238313531353B);
INSERT INTO `binary_sessions` VALUES ('60d9b82decc43d97f76d827c274b13afaefdb54f', '::1', '1499332719', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333323731373B);
INSERT INTO `binary_sessions` VALUES ('1d4bd696d7e3d1cfc2ec44a8695e6f6e73239055', '::1', '1499333317', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333333139323B);
INSERT INTO `binary_sessions` VALUES ('6622439093ebe32830d86099ca35e1fc74a63bd6', '::1', '1499333944', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333333635393B);
INSERT INTO `binary_sessions` VALUES ('ac61c2ac1e5b642df9f57d5cc3525e22b8610019', '::1', '1499334046', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333333938343B);
INSERT INTO `binary_sessions` VALUES ('3c6a2f182448fe1b5b4c4c140682939579da2120', '::1', '1499335139', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333353133393B);
INSERT INTO `binary_sessions` VALUES ('c8ff25eedcfc989bbf69cb5e2db9b674313bfa6d', '::1', '1499337702', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333373536353B);
INSERT INTO `binary_sessions` VALUES ('0f7b39657ff455926ae5efd62fc74b8d566fba33', '::1', '1499338525', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333383239323B);
INSERT INTO `binary_sessions` VALUES ('28f0b7b2eec20d1b8888cef65657658a4369bed0', '::1', '1499339031', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333383735343B);
INSERT INTO `binary_sessions` VALUES ('eda4e1b90cfd96de1c69f2cbd4f46e49e814a442', '::1', '1499339236', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393333393232373B);
INSERT INTO `binary_sessions` VALUES ('2bc6d93527bd61312bda5cedc2f38ecd56d00a2a', '::1', '1499414062', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393431343035353B);
INSERT INTO `binary_sessions` VALUES ('604ce8911206c98c2d2ae8de26b17a04ecb5cc0e', '::1', '1499414888', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393431343639363B);
INSERT INTO `binary_sessions` VALUES ('23c8b73715bec044329b0a5b206af25050342f36', '::1', '1499416796', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393431363738353B);
INSERT INTO `binary_sessions` VALUES ('b554e87e5c9355dbd8489dfe16b4302cbed72531', '::1', '1499420090', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432303039303B);
INSERT INTO `binary_sessions` VALUES ('14f87c3664c958bcfa0873f85430404e6f0660f1', '::1', '1499421221', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432313138363B);
INSERT INTO `binary_sessions` VALUES ('f03f34b13ab3897826355764467e0a7828bbd8c8', '::1', '1499421721', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432313439383B);
INSERT INTO `binary_sessions` VALUES ('df36434c374727b9a5fe58a49bbcbdb75ecd74f6', '::1', '1499422004', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432313835343B);
INSERT INTO `binary_sessions` VALUES ('d3af50374c19d09649931aaa0dd31d6344143056', '::1', '1499422672', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432323338383B);
INSERT INTO `binary_sessions` VALUES ('c603956dfb4f02296edb9fada5f72c5067bcc472', '::1', '1499422970', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432323733323B);
INSERT INTO `binary_sessions` VALUES ('782783e82992fb191721f9a7aa04d850dcce9a21', '::1', '1499423383', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432333039323B);
INSERT INTO `binary_sessions` VALUES ('1fa6281f9975eb3a1b74a210303cf00f2401b9e2', '::1', '1499423903', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432333930333B);
INSERT INTO `binary_sessions` VALUES ('5a69d52faab980d893c946305a625d9771709141', '::1', '1499424610', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393432343336363B);
INSERT INTO `binary_sessions` VALUES ('2f552851153e77ffe20f26a72dc9f12f927564a4', '::1', '1499788424', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393738383235313B);
INSERT INTO `binary_sessions` VALUES ('62e343b05866495905fbc6183d722171552a2c2b', '::1', '1499789279', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393738393237393B);
INSERT INTO `binary_sessions` VALUES ('e2aa177fb067ec58e83401af5f5db3cbfc155675', '::1', '1499790409', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393739303430393B);
INSERT INTO `binary_sessions` VALUES ('313426bb2a1e827042a81921c3fa486df2836d50', '::1', '1499795032', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393739343936313B);
INSERT INTO `binary_sessions` VALUES ('60b3abe744093c43f1bd642ab6b92faa5604fb55', '::1', '1499795591', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393739353331313B);
INSERT INTO `binary_sessions` VALUES ('36b4fb6af0498c59436cfe5f19bd616a092d5541', '::1', '1499795886', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393739353636353B);
INSERT INTO `binary_sessions` VALUES ('740512a19433a88ca3df09443c1a42e1cb06f6b0', '::1', '1499796889', 0x5F5F63695F6C6173745F726567656E65726174657C693A313439393739363738323B);

-- ----------------------------
-- Table structure for `binary_subcontract`
-- ----------------------------
DROP TABLE IF EXISTS `binary_subcontract`;
CREATE TABLE `binary_subcontract` (
  `subcontract_id` int(11) NOT NULL,
  `subcontract_company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `subcontract_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subcontract_lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subcontract_tel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subcontract_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcontract_address` text COLLATE utf8_unicode_ci NOT NULL,
  `subcontract_visible` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `subcontract_createby` int(11) DEFAULT NULL,
  `subcontract_createdate` datetime DEFAULT NULL,
  `subcontract_updateby` int(11) DEFAULT NULL,
  `supplier_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`subcontract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_subcontract
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `binary_supplier`;
CREATE TABLE `binary_supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `supplier_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_tel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_address` text COLLATE utf8_unicode_ci NOT NULL,
  `supplier_visible` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_createby` int(11) DEFAULT NULL,
  `supplier_createdate` datetime DEFAULT NULL,
  `supplier_updateby` int(11) DEFAULT NULL,
  `supplier_updatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_supplier
-- ----------------------------

-- ----------------------------
-- Table structure for `binary_title`
-- ----------------------------
DROP TABLE IF EXISTS `binary_title`;
CREATE TABLE `binary_title` (
  `title_id` int(11) NOT NULL,
  `title_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of binary_title
-- ----------------------------
INSERT INTO `binary_title` VALUES ('1', 'นาย');
INSERT INTO `binary_title` VALUES ('2', 'นาง');
INSERT INTO `binary_title` VALUES ('3', 'นางสาว');

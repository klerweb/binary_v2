<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model {
	
	function __construct() {
        parent::__construct();
    }
	
	function prepare($cfg = NULL, $where_only=FALSE) {
	    if(!$cfg)
		return FALSE;
	    if(isset($cfg->where))
		if(is_array($cfg->where))
		    $this->db->where($cfg->where);
		if(isset($cfg->or_where))
		if(is_array($cfg->or_where))
		    $this->db->or_where($cfg->or_where);
		if(isset($cfg->like))	
		if(is_array($cfg->like))
		    $this->db->like($cfg->like);
		if(isset($cfg->or_like))	
		if(is_array($cfg->or_like))
		    $this->db->or_like($cfg->or_like);
	    if((isset($cfg->where_in)))
		if(is_array($cfg->where_in))
		    foreach($cfg->where_in as $where=>$in)
			$this->db->where_in($where, $in);
		else
		    $this->db->where_in($cfg->where_in[0],$cfg->where_in[1]);
	    if($where_only)
		return TRUE;
	    if(isset($cfg->select))
		$this->db->select($cfg->select);
	    if((isset($cfg->order_by)) && is_array($cfg->order_by))
		foreach($cfg->order_by as $key=>$direction)
		    $this->db->order_by($key, $direction);
	    if(isset($cfg->pagesize))
		if(($cfg->pagesize>0))
		    if(isset($cfg->start))
			$this->db->limit((int)$cfg->pagesize, (int)$cfg->start);
		    else
			$this->db->limit((int)$cfg->pagesize, 0);
	}
	
	function get_data($table=NULL, $key_id=NULL, $cfg=NULL, $id=NULL){
		$this->prepare($cfg);
		if($id)
		return $this->db->where($key_id, $id)
			->limit(1)
			->get($table)
			->row();
		return $this->db->get($table)->result();
	}
	
	function count($table=NULL, $cfg=NULL){
	    $this->prepare($cfg);
	    return $this->db->count_all_results($table);
	}
	
	function delete($table=NULL, $key_id=NULL, $id=0){
	    if(!$id)
		return FALSE;
	    $this->db->where($key_id, $id)
			->delete($table);
	} 
    
    function maxcode($table=NULL,$fieds=NULL,$position_cut=3){
       if(empty($table) && empty($fieds)) return false ;
        
        $query = $this->db->query('SELECT max(SUBSTR('.$fieds.', '.$position_cut.')) as maxid_group FROM (`'.$table.'`)'); 
        return $query->row()->maxid_group ;
    }
    
}
?>
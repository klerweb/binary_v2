<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller 
{
	public $title = 'หน้าหลัก';
	public $menu = array('menu' => 'dashboard', 'sub' => '');
	
	function __construct()
	{
		parent::__construct();
		
		if(empty($_SESSION['binary_login']))
		{
			header('Location: '.base_url());
			 exit();
		}
	}
	
	public function index()
	{
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('index', array(), TRUE),
		);
		
		$this->parser->parse('main', $data);
	}
}

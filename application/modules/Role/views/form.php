<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br <?php echo in_array('1', $module)? 'checked ' : ''; ?>/>
				<form id="frmRole" name="frmRole" method="post" enctype="multipart/form-data" action="<?php echo base_url().'role/save'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="role_id" name="role_id" value="<?php echo $role_id; ?>" <?php echo in_array('1', $module)? 'checked ' : ''; ?>/>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">สิทธิ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="role_name" name="role_name" class="form-control" value="<?php echo $role_name; ?>" required <?php echo in_array('1', $module)? 'checked ' : ''; ?>/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ฟังก์ชั่น</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>รายการ</th>
										<th>แสดง</th>
										<th>เพิ่ม</th>
										<th>แก้ไข</th>
										<th>ลบ</th>
									</tr>
								</thead>
								<tbody>
									<tr>
				                        <td>สิทธิ</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="1" class="flat" <?php echo in_array('1', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="2" class="flat" <?php echo in_array('2', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="3" class="flat" <?php echo in_array('3', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="4" class="flat" <?php echo in_array('4', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>แผนก</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="5" class="flat" <?php echo in_array('5', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="6" class="flat" <?php echo in_array('6', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="7" class="flat" <?php echo in_array('7', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="8" class="flat" <?php echo in_array('8', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>พนักงาน</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="9" class="flat" <?php echo in_array('9', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="10" class="flat" <?php echo in_array('10', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="11" class="flat" <?php echo in_array('11', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="12" class="flat" <?php echo in_array('12', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>ลูกค้า</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="13" class="flat" <?php echo in_array('13', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="14" class="flat" <?php echo in_array('14', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="15" class="flat" <?php echo in_array('15', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="16" class="flat" <?php echo in_array('16', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>ตัวแทนจำหน่าย</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="17" class="flat" <?php echo in_array('17', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="18" class="flat" <?php echo in_array('18', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="19" class="flat" <?php echo in_array('19', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="20" class="flat" <?php echo in_array('20', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>ผู้รับเหมา</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="21" class="flat" <?php echo in_array('21', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="22" class="flat" <?php echo in_array('22', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="23" class="flat" <?php echo in_array('23', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="24" class="flat" <?php echo in_array('24', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>หน่วยนับ</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="25" class="flat" <?php echo in_array('25', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="26" class="flat" <?php echo in_array('26', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="27" class="flat" <?php echo in_array('27', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="28" class="flat" <?php echo in_array('28', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>หมวดหมู่</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="29" class="flat" <?php echo in_array('29', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="30" class="flat" <?php echo in_array('30', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="31" class="flat" <?php echo in_array('31', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="32" class="flat" <?php echo in_array('32', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>สินค้า</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="33" class="flat" <?php echo in_array('33', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="34" class="flat" <?php echo in_array('34', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="35" class="flat" <?php echo in_array('35', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="36" class="flat" <?php echo in_array('36', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      			</tbody>
                      		</table>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>role';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MX_Controller 
{
	public $title = 'สิทธิ';
	public $menu = array('menu' => 'user', 'sub' => 'role');
	
	function __construct()
	{
		parent::__construct();
		
		if(empty($_SESSION['binary_login']))
		{
			header('Location: '.base_url());
			exit();
		}
		
        $this->load->model('role/role_mod', 'role'); 
        $this->load->model('role/role_module_mod', 'role_module');
	}
	
	public function index()
	{
		$isSave = $this->input->get('isStatus');
		$popup_alert = '';
		if($isSave!='')
		{
			switch ($isSave)
			{
				case 'success_add': $popup_alert = popup_alert('success', 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_add': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้');
					break;
				case 'success_edit': $popup_alert = popup_alert('success', 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_edit': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_del': $popup_alert = popup_alert('success', 'ลบข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_del': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้');
					break;
			}
		}
		
		$data_content = array(
				'popup_alert' => $popup_alert,
				'role' => $this->role->get_role()
			);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_role/role.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('index', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		if($id=='')
		{
			$data_content = $this->role->model();
			$data_content['module'] = array();
		}
		else
		{
			$data_content = (array)$this->role->get_role($id);
			
			$list_module = (array)$this->role_module->get_by_role($id);
			
			$data_content['module'] = array();
			if(!empty($list_module))
			{
				foreach ($list_module as $module) 
				{
					$module = (array)$module;
					$data_content['module'][$module['module_id']] = $module['module_id'];
				}
			}
		}
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_role/form.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}

	public function save()
	{
		$data = $this->input->post();
		
		if($data['role_id']=='')
		{
			$data['role_id'] = $this->role->save($data);
			$isSave = is_numeric($data['role_id'])? 'success_add' : 'error_add';	
		}
		else
		{
			$isSave = $this->role->save($data, $data['role_id'])? 'success_edit' : 'error_edit';
			
			if($isSave=='success_del') $this->role_module->del_role($data['role_id']);
		}
		
		if(!empty($data['module_id']))
		{
			foreach ($data['module_id'] as $module_id)
			{
				$data_role_module['role_id'] = $data['role_id'];
				$data_role_module['module_id'] = $module_id;
				$this->role_module->save($data_role_module);
			}
		}
		
		redirect(base_url().'role?isStatus='.$isSave);
	}
	
	public function del($id)
	{
		$data['role_id'] = $id;
		$data['role_visible'] = '0';
		
		$isSave = $this->role->save($data, $data['role_id'])? 'success_del' : 'error_del';
		if($isSave=='success_del') $this->role_module->del_role($data['role_id']);
		
		redirect(base_url().'role?isStatus='.$isSave);
	}
	
	public function del_all()
	{
		$isSave = 'error_del';
	
		if(!$this->input->post('chk'))
		{
			foreach ($this->input->post('chk') as $value)
			{
				$data['role_id'] = $value;
				$data['role_visible'] = '0';
	
				$this->role->save($data, $data['role_id']);
			}
	
			$isSave = 'success_del';
		}
	
		redirect(base_url().'role?isStatus='.$isSave);
	}
}

<?php
class Role_module_mod extends MY_Model
{
	private $table = "role_module";
	private $key = "role_module_id";
	private $db_filed = array(
			"role_module_id" => "role_module_id",
			"role_id" => "role_id",
			"module_id" => "module_id",
			"role_module_createby" => "role_module_createby",
			"role_module_createdate" => "role_module_createdate"
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_role_module($role_module_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		if($role_module_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $role_module_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function get_by_role($role_id)
	{
		$cfg->where['role_id'] = $role_id;
		return  $this->get_data($this->table, '', $cfg) ;
	}
	
	function save($data=NULL, $role_id=NULL)
	{
		$role = (object) array();
		if(!$role_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $role->${'value'} = $data[$value] ;
			}
			
			unset($role->{$this->db_filed['role_module_id']});

			$role->{$this->db_filed['role_module_createdate']} = date(DATETIME_FORMAT_2DB);
			$role->{$this->db_filed['role_module_createby']} = 1;
			 
			$this->db->set($role)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $role->${'value'} = $data[$value] ;
			}
			
			return $this->db->where($this->key, $data['role_module_id'])
				->set($role)
				->update($this->table);
		}
	}
	
	function del_role($role_id)
	{
		return $this->delete($this->table, $this->db_filed['role_id'], $role_id);
	}
	
	function model()
	{
		$data['role_module_id'] = '';
		$data['role_id'] = '';
		$data['module_id'] = '';
		
		return $data;
	}
}
?>
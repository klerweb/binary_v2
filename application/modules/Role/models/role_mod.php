<?php
class Role_mod extends MY_Model
{
	private $table = "role";
	private $key = "role_id";
	private $db_filed = array(
			"role_id" => "role_id",
			"role_name" => "role_name",
			"role_visible" => "role_visible",
			"role_createby" => "role_createby",
			"role_createdate" => "role_createdate",
			"role_updateby" => "role_updateby",
			"role_updatedate" => "role_updatedate"
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_role($role_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['role_visible'] = '1';
		
		if($role_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $role_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function save($data=NULL, $role_id=NULL)
	{
		$role = (object) array();
		if(!$role_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $role->${'value'} = $data[$value] ;
			}
			
			unset($role->{$this->db_filed['role_id']});
	
			$role->{$this->db_filed['role_visible']} = '1';
			$role->{$this->db_filed['role_createdate']} = date(DATETIME_FORMAT_2DB);
			$role->{$this->db_filed['role_createby']} = 1;
			$role->{$this->db_filed['role_updatedate']} = date(DATETIME_FORMAT_2DB);
			$role->{$this->db_filed['role_updateby']} = 1;
			 
			$this->db->set($role)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $role->${'value'} = $data[$value] ;
			}
			
			$role->{$this->db_filed['role_updatedate']} = date(DATETIME_FORMAT_2DB);
			$role->{$this->db_filed['role_updateby']} = 1;
			
			return $this->db->where($this->key, $data['role_id'])
				->set($role)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['role_id'] = '';
		$data['role_name'] = '';
		$data['role_visible'] = '1';
		
		return $data;
	}
}
?>
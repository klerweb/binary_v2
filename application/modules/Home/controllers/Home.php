<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller 
{
	function __construct()
	{
		parent::__construct();		
	}
	
	public function index()
	{
		if(isset($_SESSION['binary_login']))
		{
			header('Location: '.base_url().'dashboard');
			exit();
		}
		
		$this->parser->parse('login', array());
	}
	
	public function check_login()
	{
		$this->load->model('employee/employee_mod', 'employee');
		
		$data = $this->input->post();
		$employee = $this->employee->get_login($data['username'], $data['password']);
		
		if(empty($employee))
		{
			echo 'ชื่อผู้ใช้งานและรหัสผ่านไม่ถูกต้อง';
		}
		else
		{
			$_SESSION['binary_login'] = true;
			$_SESSION['binary_user'] = $data['username'];
			$_SESSION['binary_user_id'] = $employee['employee_id'];
			$_SESSION['binary_name'] = $employee['employee_fname'].' '.$employee['employee_lname'];
			$_SESSION['binary_role_id'] = $employee['role_id'];
			$_SESSION['binary_role'] = $employee['role_name'];
			
			echo '200';
		}
	}
	
	public function logout()
	{
		unset($_SESSION['binary_login']);
		unset($_SESSION['binary_user']);
		unset($_SESSION['binary_user_id']);
		unset($_SESSION['binary_name']);
		unset($_SESSION['binary_role_id']);
		unset($_SESSION['binary_role']);
		
		header('Location: '.base_url());
		exit();
	}
	
	public function get_profile()
	{
	}
}

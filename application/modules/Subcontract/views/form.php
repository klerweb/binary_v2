<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="frmSubcontract" name="frmSubcontract" method="post" enctype="multipart/form-data" action="<?php echo base_url().'subcontract/save'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="subcontract_id" name="subcontract_id" value="<?php echo $subcontract_id; ?>" />
					<input type="hidden" id="title_id" name="title_id" value="<?php echo $title_id; ?>" />
					<input type="hidden" id="subcontract_fname" name="subcontract_fname" value="<?php echo $subcontract_fname; ?>" />
					<input type="hidden" id="subcontract_lname" name="subcontract_lname" value="<?php echo $subcontract_lname; ?>" />
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ผู้รับเหมา <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="subcontract_company" name="subcontract_company" class="form-control" value="<?php echo $subcontract_company; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">เบอร์โทรศัพท์ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="subcontract_tel" name="subcontract_tel" class="form-control" value="<?php echo $subcontract_tel; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">อีเมล์</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="email" id="subcontract_email" name="subcontract_email" class="form-control" value="<?php echo $subcontract_email; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ที่อยู่</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<textarea id="textarea" id="subcontract_address" name="subcontract_address"  name="textarea" class="form-control"><?php echo $subcontract_address; ?></textarea>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>subcontract';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
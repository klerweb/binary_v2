<?php
class Subcontract_mod extends MY_Model
{
	private $table = 'subcontract';
	private $key = 'subcontract_id';
	private $db_filed = array(
            "subcontract_id" => "subcontract_id",
            "subcontract_company" => "subcontract_company",
            "title_id" => "title_id",
            "subcontract_fname" => "subcontract_fname",
            "subcontract_lname" => "subcontract_lname",
            "subcontract_tel" => "subcontract_tel",
            "subcontract_email" => "subcontract_email",
            "subcontract_address" => "subcontract_address",
            "subcontract_visible" => "subcontract_visible",
            "subcontract_createby" => "subcontract_createby",
            "subcontract_createdate" => "subcontract_createdate",  
            "subcontract_updateby" => "subcontract_updateby",  
            "subcontract_updatedate" => "subcontract_updatedate"
            );
 
	public function get_dbfiled()
	{
		return  $this->db_filed;
	}
	
	function get_subcontract($subcontract_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['subcontract_visible'] = '1';
		
		if($subcontract_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $subcontract_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function save($data=NULL, $subcontract_id=NULL)
	{
		$subcontract = (object) array();
		if(!$subcontract_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $subcontract->${'value'} = $data[$value] ;
			}
			
			unset($subcontract->{$this->db_filed['subcontract_id']});
	
			$subcontract->{$this->db_filed['subcontract_visible']} = '1';
			$subcontract->{$this->db_filed['subcontract_createdate']} = date(DATETIME_FORMAT_2DB);
			$subcontract->{$this->db_filed['subcontract_createby']} = 1;
			$subcontract->{$this->db_filed['subcontract_updatedate']} = date(DATETIME_FORMAT_2DB);
			$subcontract->{$this->db_filed['subcontract_updateby']} = 1;
			 
			$this->db->set($subcontract)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $subcontract->${'value'} = $data[$value] ;
			}
			
			$subcontract->{$this->db_filed['subcontract_updatedate']} = date(DATETIME_FORMAT_2DB);
			$subcontract->{$this->db_filed['subcontract_updateby']} = 1;
			
			return $this->db->where($this->key, $data['subcontract_id'])
				->set($subcontract)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['subcontract_id'] = '';
		$data['title_id'] = '';
		$data['subcontract_fname'] = '';
		$data['subcontract_lname'] = '';
		$data['subcontract_company'] = '';
		$data['subcontract_tel'] = '';
		$data['subcontract_email'] = '';
		$data['subcontract_address'] = '';
		$data['subcontract_visible'] = '1';
	
		return $data;
	}
 }
?>

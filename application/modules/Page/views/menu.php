<!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
                            <h3>&nbsp;</h3>
                            <ul class="nav side-menu">
                                <li<?php if($menu=='dashboard') echo ' class="active"'; ?>><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-home"></i> หน้าหลัก</a></li>
                                <li<?php if($menu=='user') echo ' class="active"'; ?>><a><i class="fa fa-users"></i> ข้อมูลบุคคล <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu"<?php if($menu!='user') echo ' style="display: none"'; ?>>
                                    	<li<?php if($sub=='role') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>role">สิทธิ</a></li>
                                        <li<?php if($sub=='department') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>department">แผนก</a></li>
                                        <li<?php if($sub=='employee') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>employee">พนักงาน</a></li>
                                    </ul>
                                </li>
                                 <li<?php if($menu=='contact') echo ' class="active"'; ?>><a><i class="fa fa-star"></i> ผู้ติดต่อ <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu"<?php if($menu!='contact') echo ' style="display: none"'; ?>>
                                        <li<?php if($sub=='customer') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>customer">ลูกค้า</a></li>
                                        <li<?php if($sub=='supplier') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>supplier">ตัวแทนจำหน่าย</a></li>
                                        <li<?php if($sub=='subcontract') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>subcontract">ผู้รับเหมา</a></li>
                                    </ul>
                                </li>
                                <li<?php if($menu=='store') echo ' class="active"'; ?>><a><i class="fa fa-archive"></i> คลังสินค้า <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu"<?php if($menu!='store') echo ' style="display: none"'; ?>>
                                        <li<?php if($sub=='unit') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>unit">หน่วยนับ</a></li>
                                        <li<?php if($sub=='category') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>category">หมวดหมู่</a></li>
                                        <li<?php if($sub=='product') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>product">สินค้า</a></li>
                                    </ul>
                                </li>
                                <li<?php if($menu=='setting') echo ' class="active"'; ?>><a><i class="fa fa-cog"></i> ตั้งค่า <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu"<?php if($menu!='setting') echo ' style="display: none"'; ?>>
                                        <li<?php if($sub=='type') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>type">ประเภทบริการ</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>servicerequest"><i class="fa fa-paper-plane-o"></i> รับเรื่อง</a></li>
                                <li><a href="<?php echo base_url(); ?>service"><i class="fa fa-paper-plane"></i> บริการ</a></li>
                                <li><a href="#"><i class="fa fa-asterisk"></i> Cost Sheet</a></li>
                                <li<?php if($menu=='account') echo ' class="active"'; ?>><a><i class="fa fa-calculator"></i> บัญชี <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu"<?php if($menu!='account') echo ' style="display: none"'; ?>>
                                        <li<?php if($sub=='expenses') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>expenses">ค่าใช้จ่าย</a></li>
                                        <li<?php if($sub=='payment') echo ' class="current-page"'; ?>><a href="<?php echo base_url(); ?>payment">รายการเบิก</a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><i class="fa fa-calendar"></i> ตารางงาน</a></li>
                                <li><a href="#"><i class="fa fa-bar-chart"></i> รายงาน</a></li>
                                <li><a href="#" onclick="logout();"><i class="fa fa-unlock"></i> ออกจากระบบ</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
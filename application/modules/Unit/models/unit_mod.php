<?php
class Unit_mod extends MY_Model
{
	private $table = "unit";
	private $key = "unit_id";
	private $db_filed = array(
			"unit_id" => "unit_id",
			"unit_name" => "unit_name",
			"unit_visible" => "unit_visible",
			"unit_createby" => "unit_createby",
			"unit_createdate" => "unit_createdate",
			"unit_updateby" => "unit_updateby",
			"unit_updatedate" => "unit_updatedate"
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_unit($unit_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['unit_visible'] = '1';
		
		if($unit_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $unit_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function save($data=NULL, $unit_id=NULL)
	{
		$unit = (object) array();
		if(!$unit_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $unit->${'value'} = $data[$value] ;
			}
			
			unset($unit->{$this->db_filed['unit_id']});
	
			$unit->{$this->db_filed['unit_visible']} = '1';
			$unit->{$this->db_filed['unit_createdate']} = date(DATETIME_FORMAT_2DB);
			$unit->{$this->db_filed['unit_createby']} = 1;
			$unit->{$this->db_filed['unit_updatedate']} = date(DATETIME_FORMAT_2DB);
			$unit->{$this->db_filed['unit_updateby']} = 1;
			 
			$this->db->set($unit)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $unit->${'value'} = $data[$value] ;
			}
			
			$unit->{$this->db_filed['unit_updatedate']} = date(DATETIME_FORMAT_2DB);
			$unit->{$this->db_filed['unit_updateby']} = 1;
			
			return $this->db->where($this->key, $data['unit_id'])
				->set($unit)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['unit_id'] = '';
		$data['unit_name'] = '';
		$data['unit_visible'] = '1';
		
		return $data;
	}
}
?>
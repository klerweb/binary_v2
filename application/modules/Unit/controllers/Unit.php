<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends MX_Controller 
{
	public $title = 'หน่วยนับ';
	public $menu = array('menu' => 'store', 'sub' => 'unit');
	
	function __construct()
	{
		parent::__construct();
		
		if(empty($_SESSION['binary_login']))
		{
			header('Location: '.base_url());
			exit();
		}
		
        $this->load->model('unit/unit_mod', 'unit'); 
	}
	
	public function index()
	{
		$isSave = $this->input->get('isStatus');
		$popup_alert = '';
		if($isSave!='')
		{
			switch ($isSave)
			{
				case 'success_add': $popup_alert = popup_alert('success', 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_add': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้');
					break;
				case 'success_edit': $popup_alert = popup_alert('success', 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_edit': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_del': $popup_alert = popup_alert('success', 'ลบข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_del': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้');
					break;
			}
		}
		
		$data_content = array(
				'popup_alert' => $popup_alert,
				'unit' => $this->unit->get_unit()
			);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_unit/unit.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('index', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_content = $id==''? $this->unit->model() : (array)$this->unit->get_unit($id);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_unit/form.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}

	public function save()
	{
		$data = $this->input->post();
		
		if($data['unit_id']=='')
		{
			$data['unit_id'] = $this->unit->save($data);
			$isSave = is_numeric($data['unit_id'])? 'success_add' : 'error_add';
		}
		else
		{
			$isSave = $this->unit->save($data, $data['unit_id'])? 'success_edit' : 'error_edit';
		}
		
		redirect(base_url().'unit?isStatus='.$isSave);
	}
	
	public function del($id)
	{
		$data['unit_id'] = $id;
		$data['unit_visible'] = '0';
		
		$isSave = $this->unit->save($data, $data['unit_id'])? 'success_del' : 'error_del';
		
		redirect(base_url().'unit?isStatus='.$isSave);
	}
	
	public function del_all()
	{
		$isSave = 'error_del';
	
		if(!$this->input->post('chk'))
		{
			foreach ($this->input->post('chk') as $value)
			{
				$data['unit_id'] = $value;
				$data['unit_visible'] = '0';
	
				$this->unit->save($data, $data['unit_id']);
			}
	
			$isSave = 'success_del';
		}
	
		redirect(base_url().'unit?isStatus='.$isSave);
	}
}

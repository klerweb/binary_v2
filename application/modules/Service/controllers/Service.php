<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MX_Controller 
{
	public $title = 'บริการ';
    public $menu = array('menu' => 'service', 'sub' => '');   
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('servicerequest/service_mod', 'service');   
        $this->load->model('servicerequest/servicetype_mod', 'servicetype');
        $this->load->model('servicerequest/service_branch_mod', 'service_branch');     
        $this->load->model('servicerequest/quotation_mod', 'quotation'); 
        $this->load->model('servicerequest/purchase_mod', 'purchase'); 
        $this->load->model('servicerequest/job_mod', 'job'); 
        $this->load->model('servicerequest/expenses_mod', 'expenses');  
        $this->load->model('type/type_mod', 'type');         
        $this->load->model('employee/employee_mod', 'employee');   
        $this->load->model('customer/customer_mod', 'customer');   
        $this->load->model('customer/customer_address_mod', 'customer_address');  
        $this->load->model('customer/customer_branch_mod', 'customer_branch');   
        $this->load->model('supplier/supplier_mod', 'supplier');   
        $this->load->model('subcontract/subcontract_mod', 'subcontract');   
          
     
    }                              
	
	public function index()
    {
       $isSave  = $this->input->get('isStatus');
      if( $isSave !=""){
            $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้ <p>".$this->input->get('message')."</p>" ;
        }  
       
        $data_array = array(
            "service"=>$this->service->get_service(),
            'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "", 
        ) ;
        $data = array(
                'title' => $this->title,
                'title_sub' => '',   
                'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),  
                'js_other' => array('modules_servicerequest/servicerequest.js'),  
                'content' => $this->parser->parse('index', $data_array, TRUE),
        );
        
        $this->parser->parse('main', $data);   
    }
    
    public function description($id='',$isSave=''){
 
        if($isSave !="" || $this->input->get('isStatus')){
            if($this->input->get('isStatus')) $isSave = $this->input->get('isSave');
            $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้" ;
        }
        
        $data_array =  array(
                        'id' => $id,
                        'service_id' => $id,
                        'tab' => 'description',
                        'service'=> $id ? array($this->service->get_service($id))  : '',
                        'type'=>$this->type->get_type(),
                        'employee'=>$this->employee->get_employee(),
                        'customer'=>$this->customer->get_customer() ,    
                        'supplier'=>$this->supplier->get_supplier(), 
                        'subcontract'=>$this->subcontract->get_subcontract(),
                        'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "", 
                        
        );     
        $service_code2 = $id ? $data_array['service'][0]->service_code :   service_format2($this->service->serviceTmp()) ;
        $data_array['service_code'] = $service_code2;
        $data_array['data'] = $data_array; 
        $data = array(
                'title' => $this->title,
                'title_sub' => ':: '.$service_code2,                                         
                'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
                'content' => $this->parser->parse('description', $data_array, TRUE),
        );
         
        $this->parser->parse('main', $data);
    }
    
   public function save_description($id=''){
        $data = $this->input->post();
 
         list($startdate,$enddate) = explode(" - ",$data['service_startend']);  
         
         $data['service_date'] =setDateToDB($startdate); 
         $data['service_end'] = setDateToDB($enddate);                    
          
 
          if(!$id) $data['service_id'] = $this->getidServiceFromTmp($data['service_code']);                
          else $data['service_id'] = $id;
          
          $data['customer_id'] = $data['customer_id'][0];                
 
          //-------------- save service 
          if(!$id){
           $this->service->save($data);      
          }else{
              $service = $this->service->get_service($id);
           
           if($data['customer_id'] != $service->customer_id){
            
               // delete all old branch                                                              
           $branch = $this->service_branch->get_service_branchByservice($service->service_id);     
 
           foreach($branch as $key=>$val){
                //update brach
                $data_brach['service_branch_id'] = $val->service_branch_id;
                $data_brach['service_branch_visible'] = '0';
                $this->service_branch->save($data_brach,$val->service_branch_id)    ;
            }     
           } 
              $this->service->save($data,$id);      
          }
         
             
          //----------- save customer branch
          if(isset($data['customer_branch']) && @$data['customer_branch'][0] !=""){
                foreach($data['customer_branch'] as $key=>$val){
                $data_customer_branch['service_id'] = $data['service_id'];    
                $data_customer_branch['customer_id'] = $data['customer_id'][0];
                $data_customer_branch['customer_branch_id'] = $val;
                $data_customer_branch['service_branch_visible'] = '1' ;
                 
                $this->service_branch->save($data_customer_branch);
           }  
          }
            
 
          // ------------- save quotation
         if(isset($_SESSION['quotation']) &&  count($_SESSION['quotation'])>0){
             foreach($_SESSION['quotation'] as $key=>$val){
              if(isset($val['service_code']) && @in_array($key,$data['quotation_session']) ){
                    if($val['service_code'] == $data['service_code']){
                          $data_q = $val;
                          $data_q['service_id'] = $data['service_id'];
                          $data_q['quotation_date'] = setDateToDB($val['quotation_date']); 
                          
                    if($val['quotation_type'] == "out")  {
                         $data_q['quotation_source'] = ""; 
                         $data_q['customer_id'] = 0;
                         $data_q['supplier_id'] = 0;
                         $data_q['subcontract_id'] = 0;
                    }else{
  
                         $data_q['customer_id'] = $val['Qcustomer_id'];
                         $data_q['supplier_id'] = $val['Qsupplier_id'];  
                         $data_q['subcontract_id'] = $val['Qsubcontract_id'];   
                    }        
                  $this->quotation->save($data_q);
                  unset($_SESSION['quotation'][$key]); 
              }
          }

          }
         } 
           
             
          //----------- save purchase
          if(isset($_SESSION['purchase']) &&  count($_SESSION['purchase'])>0){ 
            foreach($_SESSION['purchase'] as $key=>$val){
               if($val['service_code'] == $data['service_code'] && @in_array($key,$data['purchase_session']) ){
                  $data_p = $val;
                  $data_p['service_id'] = $data['service_id'];
                  $data_p['purchase_date'] = setDateToDB($val['purchase_date']);
                   
                   if($val['purchase_type'] == "in")  {
                     $data_p['purchase_source'] = "";
                     $data_p['customer_id'] = 0;
                     $data_p['supplier_id'] = 0;
                     $data_p['subcontract_id'] = 0;
                }else{
                    $data_p['customer_id'] = $val['Pcustomer_id'] ;
                     $data_p['supplier_id'] = $data_p['Psupplier_id'];
                     $data_p['subcontract_id'] = $data_p['Psubcontract_id'];
                }
                 
                  $this->purchase->save($data_p);
                  unset($_SESSION['purchase'][$key]); 
               }
          }
          }
          
          //---------- save job
          if(isset($_SESSION['job']) &&  count($_SESSION['job'])>0){     
             foreach($_SESSION['job']  as $key=>$val){
                if($val['service_code'] == $data['service_code'] && @in_array($key,$data['job_session'])  ){
                $data_j = $val;
                $data_j['service_id'] = $data['service_id'];   
                $data_j['employee_id']  = $data_j['Jemployee_id'];
                $data_j['subcontract_id']  = $data_j['Jsubcontract_id'];
                
                if($data_j['job_type'] == "employee") $data['subcontract_id'] = 0;
                if($data_j['job_type'] == "subcontract") $data['employee_id'] = 0;
                
                list($startdate,$enddate) = explode(" - ",$val['job_startdate']);
         
                $data_j['job_startdate'] =setDateToDB($startdate); 
                $data_j['job_enddate'] =setDateToDB($enddate); 
                if($val['job_type'] == "employee") $data_j['subcontract_id'] = 0;
                if($val['job_type'] == "subcontract") $data_j['employee_id'] = 0;
                
                        
                
                $this->job->save($data_j) ;
                 unset($_SESSION['job'][$key]);
                }
          }
          }
          
          //----------- save expenses
          if(isset($_SESSION['expenses']) &&  count($_SESSION['expenses'])>0){
             foreach($_SESSION['expenses']  as $key=>$val){
                if($val['service_code'] == $data['service_code'] && @in_array($key,$data['expenses_session']) ){
                $data_e = $val;
                $data_e['service_id'] = $data['service_id'];   
                $data_e['expenses_date'] = setDateToDB($val['expenses_date']);    
                
                $this->expenses->save($data_e) ;
                  unset($_SESSION['expenses'][$key]);
                }
          }
          }
             
         
      redirect(base_url().'service?isStatus=success'); 
      
    }
    
    public function description_delete($id=''){
      if(!empty($id) ) {
            $data['service_id'] = $id;
            $data['service_visible'] = 0;
            $this->service->save($data,$id); 
            
            redirect(base_url().'service/?isStatus=success');
      }  
                
    }
    
    public function description_cancel($id=''){
       if(!empty($id) ) {
            $data['service_id'] = $id;
            $data['service_visible'] = '3';
            //debug($data); exit();
            $this->service->save($data,$id); 
            
            redirect(base_url().'service/?isStatus=success');
      }  
          
    }
    
    public function customerData($customer_id='',$customer_branch_id='',$service_id=''){
        $data_array = array(
            'customer_id'=>$customer_id,
            'customer_branch_id'=>$customer_branch_id,
            'service_id'=>$service_id,
            'customer'=>$this->customer->get_customer($customer_id),
            'customer_branch'=>$this->customer_branch->get_branchByCustomer($customer_id),
        );  
        if($this->input->get('ajax')) echo $this->load->view('customerdata',$data_array,true);  
        else return   $this->load->view('customerdata',$data_array,true);
    } 
    
    public function customerBranch($customer_branch_id=''){
        $data_array = array(
            'customer_branch_id'=>$customer_branch_id,
            'customer_branch'=>$this->customer_branch->get_customer_branch($customer_branch_id),
        );    
        echo $this->load->view('customerdata_branch',$data_array,true);   
    }
    
    public function customerAddress($customer_address_id=''){
         $data_array = array(
            'customer_address_id'=>$customer_address_id,
            'customer_address'=>$this->customer_address->get_customer_address($customer_address_id),
        );    
        echo $this->load->view('customerdata_address',$data_array,true);  
    }
    
    public function customer_option($customer_id=''){
        echo $customer_id."<br>";
        if($this->input->get('ajax')) echo $this->load->view('service/customer_option',array('customer_id'=>$customer_id),true) ;
        else return $this->load->view('service/customer_option',array('customer_id'=>$customer_id),true) ; 
    }
    
    public function addressByZipcode($zipcode=''){
        if($zipcode !=''){
            $address_data = $this->customer_address->get_zipcodeByZipcode($zipcode); 
            $data['address'] = $address_data;
              
            echo $this->load->view('service/customer_address_zipcode',$data,true);  
        }                     
    }
    
    public function save_popupaddress() {
        $data = $this->input->post();                 
        $data['zipcode_id']  = $this->customer_address->get_zipcodeByRefer($data['province_id'],$data['amphur_id'],$data['district_id']);
        $data['zipcode_id']  = $data['zipcode_id'][0]->zipcode_id; 
 
        $result_id = $this->customer_address->save($data); 
        
        
      $isSave = $result_id ? json_encode(array('code'=>'200','message'=>'success','id'=>$result_id,'address_name'=>$data['customer_address_name'])): json_encode(array('code'=>'100','message'=>'error','id'=>'','address_name'=>'')) ;   
      echo $isSave ;
    }
    
    public function quotation($service_id='',$qt_id=''){
      $isSave  = $this->input->get('isStatus');
      if( $isSave !=""){
            $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้ <p>".$this->input->get('message')."</p>" ;
        }
      $data_array =  array(
                        'id' => $service_id,
                        'service_id' => $service_id,
                        'qt_id' => $qt_id,
                        'tab' => 'quotation',
                        'quotation'=>$qt_id ? $this->quotation->get_quotation($service_id,$qt_id): "",
                        'quotations'=>$this->quotation->get_quotation($service_id) ,
                        'customer'=>$this->customer->get_customer() ,    
                        'supplier'=>$this->supplier->all(), 
                        'subcontract'=>$this->subcontract->get_subcontract(),
                        'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "", 
        );
        
 
        $data = array(
                'title' => $this->title,
                'title_sub' => '',
                'content' => $this->parser->parse('quotation', $data_array, TRUE),
        );
         
        $this->parser->parse('main', $data);  
    }
    
    public function quotation_form($service_id='',$qt_id=''){
            $isSave  = $this->input->get('isStatus');
      if( $isSave !=""){
            $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้ <p>".$this->input->get('message')."</p>" ;
        }
      $data_array =  array(
                        'id' => $service_id,
                        'service_id' => $service_id,
                        'qt_id' => $qt_id,
                        'tab' => 'quotation',
                        'quotation'=>$qt_id ? $this->quotation->get_quotation($service_id,$qt_id): "",
                        'quotations'=>$this->quotation->get_quotation($service_id) ,
                        'customer'=>$this->customer->get_customer() ,    
                        'supplier'=>$this->supplier->get_supplier(), 
                        'subcontract'=>$this->subcontract->get_subcontract(),
                        'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "", 
        );
        
        $this->load->view('servicerequest/quotation_form',$data_array);
        
    }
 
    
    public function save_quotation($service_id='',$qt_id=''){
        $data = $this->input->post();
        $data['quotation_date'] = setDateToDB($data['quotation_date']);
        
        $name_folder =  $this->input->post('quotation_type')=="in" ? "quotation_in" : "quotation_out";   
 
 // UPLOAD USER IMAGE
        $config['upload_path'] = APPPATH . '../uploads/'.$name_folder;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';

 
        if (!is_dir($config['upload_path']))
            mkdir($config['upload_path'], 0777, TRUE);

        $this->load->library('upload', $config);    
             
        if($_FILES['quotation_file']['name'] !=""){
               if ( ! $this->upload->do_upload('quotation_file'))
                {
                   redirect(base_url().'service/quotation/'.$service_id."/".$qt_id.'?isStatus=error&message='.strip_tags($this->upload->display_errors())); 
                }
                else
                {
                    $fileName = $this->upload->data();     
                    $allowed =  array('gif','png' ,'jpg');
                    $filename = $fileName['file_name'];
         
                    $data['quotation_path'] = 'uploads/'.$name_folder.'/'.$fileName['file_name'];    
                    
                }
                    
        } 
        if($qt_id==''){
              
           $result_id = $this->quotation->save($data); 
        }else{
            
            $data['quotation_id'] = $qt_id;
            $result_id = $qt_id;
            
             if($data['quotation_type'] == "out")  {
                 $data['quotation_source'] = ""; 
                 $data['customer_id'] = 0;
                 $data['supplier_id'] = 0;
                 $data['subcontract_id'] = 0;
            }else{
                $data['customer_id'] = $data['Qcustomer_id'];
                $data['supplier_id'] = $data['Qsupplier_id'];  
                $data['subcontract_id'] = $data['Qsubcontract_id'];   
            }
            
             
            $this->quotation->save($data,$qt_id); 
        }
        
      $isSave = $result_id ? "success":"error" ;    
      if($qt_id)  redirect(base_url().'service/description/'.$service_id."/".$result_id."?isStatus=".$isSave); 
      else       redirect(base_url().'service/description/'.$service_id."/?isStatus=".$isSave); 
      
    }
    
    public function quotation_delete($service_id='',$qt_id=''){
      if(!empty($service_id) && !empty($qt_id)) {
            $data['quotation_id'] = $qt_id;
            $data['quotation_visible'] = '0';
            $this->quotation->save($data,$qt_id); 
            
            redirect(base_url().'service/description/'.$service_id."?isStatus=success");
      }  
          
        
    }
    
    public function purchase($service_id='',$purchase_id=''){
  $isSave  = $this->input->get('isStatus');
      if( $isSave !=""){
            $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้ <p>".$this->input->get('message')."</p>" ;
        }
      $data_array =  array(
                        'id' => $service_id,
                        'service_id' => $service_id,
                        'purchase_id' => $purchase_id,
                        'tab' => 'purchase',
                        'purchase'=>$purchase_id ? $this->purchase->get_purchase($service_id,$purchase_id): "",
                        'purchases'=>$this->purchase->get_purchase($service_id) ,
                        'customer'=>$this->customer->get_customer() ,    
                        'supplier'=>$this->supplier->all(), 
                        'subcontract'=>$this->subcontract->get_subcontract(),
                        'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "", 
        );
        
 
        $data = array(
                'title' => $this->title,
                'title_sub' => '',
                'content' => $this->parser->parse('purchase', $data_array, TRUE),
        );
         
        $this->parser->parse('main', $data);  
    }
    
    public function purchase_form($service_id='',$purchase_id=''){
      $isSave  = $this->input->get('isStatus');
          if( $isSave !=""){
                $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้ <p>".$this->input->get('message')."</p>" ;
            }
          $data_array =  array(
                            'id' => $service_id,
                            'service_id' => $service_id,
                            'purchase_id' => $purchase_id,
                            'tab' => 'purchase',
                            'purchase'=>$purchase_id ? $this->purchase->get_purchase($service_id,$purchase_id): "",
                            'purchases'=>$this->purchase->get_purchase($service_id) ,
                            'customer'=>$this->customer->get_customer() ,    
                            'supplier'=>$this->supplier->get_supplier(), 
                            'subcontract'=>$this->subcontract->get_subcontract(),
                            'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "", 
            );
            
      $this->load->view('servicerequest/purchase_form',$data_array);
    }
    
    public function purchase_delete($service_id='',$purchase_id=''){
      if(!empty($service_id) && !empty($purchase_id)) {
            $data['purchase_id'] = $purchase_id;
            $data['purchase_visible'] = '0';
            $this->purchase->save($data,$purchase_id); 
            
            redirect(base_url().'service/description/'.$service_id."?isStatus=success");
      }  
          
        
    }
    
    public function save_purchase($service_id='',$purchase_id=''){
        $data = $this->input->post();
        $data['purchase_date'] = setDateToDB($data['purchase_date']);
        
         $name_folder =  $this->input->get('purchase_type')=="in" ? "po_in" : "po_out";
        
 // UPLOAD USER IMAGE
        $config['upload_path'] = APPPATH . '../uploads/'.$name_folder;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';

 
        if (!is_dir($config['upload_path']))
            mkdir($config['upload_path'], 0777, TRUE);

        $this->load->library('upload', $config);    
             
        if($_FILES['purchase_file']['name'] !=""){
               if ( ! $this->upload->do_upload('purchase_file'))
                {
                   redirect(base_url().'service/purchase/'.$service_id."/".$purchase_id.'?isStatus=error&message='.strip_tags($this->upload->display_errors())); 
                }
                else
                {
                    $fileName = $this->upload->data();     
                    $allowed =  array('gif','png' ,'jpg');
                    $filename = $fileName['file_name'];
         
                    $data['purchase_path'] = 'uploads/'.$name_folder.'/'.$fileName['file_name'];   
                     
                }
                    
        }
        
        
        if($purchase_id==''){
           $result_id = $this->purchase->save($data); 
        }else{
            if($data['purchase_type'] == "in")  {
                 $data['purchase_source'] = "";
                 $data['customer_id'] = 0;
                 $data['supplier_id'] = 0;
                 $data['subcontract_id'] = 0;
            }else{
                $data['customer_id'] = $data['Pcustomer_id'];
                $data['supplier_id'] = $data['Psupplier_id'];  
                $data['subcontract_id'] = $data['Psubcontract_id'];   
            }
            
            
            $data['purchase_id'] = $purchase_id;
            $result_id = $purchase_id;
            $this->purchase->save($data,$purchase_id); 
 
        }
        
      $isSave = $result_id ? "success":"error" ;
      if($purchase_id) redirect(base_url().'service/description/'.$service_id."/".$result_id."?isStatus=".$isSave); 
      else redirect(base_url().'service/description/'.$service_id."/?isStatus=".$isSave); 
      
    }
    
    public function job($service_id='',$job_id=0){
      $isSave  = $this->input->get('isStatus');
      if( $isSave !=""){
            $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้ <p>".$this->input->get('message')."</p>" ;
        }
        
      $data_array =  array(
                        'id' => $service_id,
                        'service_id' => $service_id,
                        'job_id' => $job_id,
                        'tab' => 'job',
                        'job'=>$job_id ? $this->job->get_job($service_id,$job_id) : "",
                        'jobs'=>$this->job->get_job($service_id),
                        'employee'=>$this->employee->get_employee(),
                        'subcontract'=>$this->subcontract->get_subcontract(),
                        'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "", 
        );
        $data = array(
                'title' => $this->title,
                'title_sub' => '',
                'content' => $this->parser->parse('job', $data_array, TRUE),
        );
         
        $this->parser->parse('main', $data);  
    }
    
    public function save_job($service_id='',$job_id=''){
        $data = $this->input->post();
        list($startdate,$enddate) = explode(" - ",$data['job_startdate']);
         
        $data['job_startdate'] =setDateToDB($startdate); 
        $data['job_enddate'] =setDateToDB($enddate); 
        
 // UPLOAD USER IMAGE
        $config['upload_path'] = APPPATH . '../uploads/job';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';

 
        if (!is_dir($config['upload_path']))
            mkdir($config['upload_path'], 0777, TRUE);

        $this->load->library('upload', $config);    
             
        if($_FILES['job_file']['name'] !=""){
               if ( ! $this->upload->do_upload('job_file'))
                {
                   redirect(base_url().'service/job/'.$service_id."/".$job_id.'?isStatus=error&message='.strip_tags($this->upload->display_errors())); 
                }
                else
                {
                    $fileName = $this->upload->data();     
                    $allowed =  array('gif','png' ,'jpg');
                    $filename = $fileName['file_name'];
         
                    $data['job_path'] = 'uploads/job/'.$fileName['file_name'];   
                     
                }
                    
        }
        
        $data['subcontract_id']  = $data['Jsubcontract_id'] ;
        $data['employee_id']  = $data['Jsubcontract_id'] ;
        
        if($data['job_type'] == "employee") $data['subcontract_id'] = 0;
        if($data['job_type'] == "subcontract") $data['employee_id'] = 0;
        
        if($job_id==''){
           $result_id = $this->job->save($data); 
        }else{
 
            $data['job_id'] = $job_id;
            $result_id = $job_id;
            $this->job->save($data,$job_id); 
        }
        
      $isSave = $result_id ? "success":"error" ;   
       
      if($job_id) redirect(base_url().'service/job/'.$service_id."/".$result_id."?isStatus=".$isSave); 
      else  redirect(base_url().'service/job/'.$service_id."?isStatus=".$isSave);  
      
    }
    
    public function job_delete($service_id='',$job_id=''){
      if(!empty($service_id) && !empty($job_id)) {
            $data['job_id'] = $job_id;
            $data['job_visible'] = '0';
            $this->job->save($data,$job_id); 
            
            redirect(base_url().'service/description/'.$service_id."?isStatus=success");
      }  
          
        
    }
    
    public function expenses($service_id='',$expenses_id=''){
      $isSave  = $this->input->get('isStatus');
      if( $isSave !=""){
            $message_alert = $isSave == "success" ? "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"  : "ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้ <p>".$this->input->get('message')."</p>" ;
        }  
      $data_array =  array(
                        'id' => $service_id,
                        'service_id' => $service_id,
                        'expenses_id' => $expenses_id,
                        'expense'=>$expenses_id ? $this->expenses->get_expenses($service_id,$expenses_id) : "",
                        'expenses'=>$this->expenses->get_expenses($service_id),
                        'tab' => 'expenses', 
                        'popup_alert'=>$isSave ? popup_alert($isSave,$message_alert) : "",  
        );
        $data = array(
                'title' => $this->title,
                'title_sub' => '',
                'content' => $this->parser->parse('expenses', $data_array, TRUE),
        );
         
        $this->parser->parse('main', $data);  
    }
    
    public function save_expenses($service_id='',$expenses_id=''){
        $data = $this->input->post();
        $data['expenses_date'] = setDateToDB($data['expenses_date']);      
        
 // UPLOAD USER IMAGE
        $config['upload_path'] = APPPATH . '../uploads/expenses';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';

 
        if (!is_dir($config['upload_path']))
            mkdir($config['upload_path'], 0777, TRUE);

        $this->load->library('upload', $config);    
             
        if($_FILES['expenses_file']['name'] !=""){
               if ( ! $this->upload->do_upload('expenses_file'))
                {
                   redirect(base_url().'service/expenses/'.$service_id."/".$expenses_id.'?isStatus=error&message='.strip_tags($this->upload->display_errors())); 
                }
                else
                {
                    $fileName = $this->upload->data();     
                    $allowed =  array('gif','png' ,'jpg');
                    $filename = $fileName['file_name'];
         
                    $data['expenses_path'] = 'uploads/expenses/'.$fileName['file_name'];   
                     
                }
                    
        }
 
        if($expenses_id==''){
           $result_id = $this->expenses->save($data); 
        }else{
 
            $data['expenses_id'] = $expenses_id;
            $result_id = $expenses_id;
            $this->expenses->save($data,$expenses_id); 
        }
        
      $isSave = $result_id ? "success":"error" ;   
      if($expenses_id) redirect(base_url().'service/expenses/'.$service_id."/".$result_id."?isStatus=".$isSave); 
      else   redirect(base_url().'service/expenses/'.$service_id."?isStatus=".$isSave);   
      
    }
    
    public function expenses_delete($service_id='',$expenses_id=''){
      if(!empty($service_id) && !empty($expenses_id)) {
            $data['expenses_id'] = $expenses_id;
            $data['expenses_visible'] = '0';
            $this->expenses->save($data,$expenses_id); 
            
            redirect(base_url().'service/description/'.$service_id."?isStatus=success");
      }       
    }
    
    public function unset_session($name=''){
        if(!$name){
           unset($_SESSION); 
        }else{
            unset($_SESSION[$name]);    
        }
        
    }
    
    public function show_session($name=''){
        if(!$name){
           debug($_SESSION); 
        }else{
          debug($_SESSION[$name]);  
        }
        
    }
    
     
    public function array_session($name=''){
       $array_name= array(
            'quotation'=>'quotation',
            'purchase'=>'purchase',
            'job'=>'job',
            'expenses'=>'expenses',
        );
        
        return $array_name[$name] ;
        
    }
    
    public function save_session($name='',$key=''){
       $_sessname = $this->array_session($name);
       if($key!=""){
           $_SESSION[$name][$key]['file'] = $_FILES ; 
            $name_folder = $name;
            
          if($name=="quotation"){
             $name_folder =  $this->input->get('type')=="in" ? "quotation_in" : "quotation_out";   
          }else if($name=="purchase"){
             $name_folder =  $this->input->get('type')=="in" ? "po_in" : "po_out";
          } 
                    // UPLOAD FILE
                $path = APPPATH . '../uploads/'.$name_folder;
                $new_name =date('ymdHis')."_".$_FILES[0]['name'];
                $new_path_name =    $path."/".$new_name;
        
               if(move_uploaded_file($_FILES[0]['tmp_name'],$new_path_name )){
                   $_SESSION[$name][$key][$name.'_path']  ='uploads/'.$name_folder."/".$new_name;   
                     echo json_encode(array('code'=>200,'path'=>base_url()."".$_SESSION[$name][$key][$name.'_path']));
               }else{
                   echo json_encode(array('code'=>100));
               }        
                    
 
        
       }else{
            $data = $_POST;
            $data['file'] = $_FILES;
            $_SESSION[$_sessname][] = $data ;      
            
            $array_keys = array_keys($_SESSION[$_sessname]);
            $keys= end($array_keys);
             
            echo $keys;
       }  
    }
    
    
    public function delete_session($name='',$key=''){
         $_sessname = $this->array_session($name); 
         
         unset($_SESSION[$_sessname][$key]); 
    }
 
    public function getidServiceFromTmp($code=''){
         $_code = $code ;
        $id = $this->service->getidServiceFromTmp($_code);
        
        return $id ; 
        
    }
    
    public function delete_quotation_session($key){
        unset($_SESSION['quotation'][$key]);
         debug($_SESSION['quotation']);
    }
    
    public function delete_service_brach($service_branch_id='',$service_id=''){
        if(($service_id) && ($service_branch_id)) {
            $data['service_branch_id'] = $service_branch_id;
            $data['service_branch_visible'] = '0';
            $this->service_branch->save($data,$service_branch_id); 
            
            redirect(base_url().'service/description/'.$service_id."?isStatus=success");
    }
    }
    
      
}

<?php
 $this->load->model('quotation_mod', 'quotation');  
  $this->load->model('supplier/supplier_mod', 'supplier');  
 $this->load->model('subcontract/subcontract_mod', 'subcontract');

 
if(isset($quotation) && $quotation !="")  {$quotation= $quotation[0];      }
$url_save = $id =="" ? base_url()."service/save_quotation" : base_url()."service/save_quotation/".@$service_id."/".@$qt_id;
  
 ?>
                  <form id="add_quotation" class="form-horizontal form-label-left"   method="post" enctype="multipart/form-data"  action ="<?php echo $url_save?>">      
                   <div class="modal-body" >
                    
                                    <input type="hidden" id="service_id" name="service_id" value="<?php echo @$service_id?>"> 
                                    <div class="form-group" id="myModalWithDatePicker">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่ <span class=" ">*</span></label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="quotation_date" name="quotation_date"  readonly="readonly"  class="date-picker form-control col-md-7 col-xs-12"  value="<?php echo @$quotation->quotation_date?>" >
                                      </div>
                                    </div>
                                     <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ประเภท <span class=" ">*</span> </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="radio" class="quotation_type" name="quotation_type" id="quotation_type" value="in"   <?php echo @$quotation->quotation_type=="in"? "checked":""?>    /> เข้า:
                                          <input type="radio" class="quotation_type" name="quotation_type" id="quotation_type" value="out"  <?php echo @$quotation->quotation_type=="out"? "checked":""?> /> Out
                                      </div>
                                    </div>
                                    <?php
                                     $style_display = (isset($quotation->quotation_type) && $quotation->quotation_type == "in") ? "" : "display:none";     
                                    ?>
                                     <div class="form-group" id="div_customer" style="<?php echo $style_display?>" >     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">รับข้อมูลจาก</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="radio" class="quotation_source" name="quotation_source"  value="customer"     <?php echo @$quotation->quotation_source=="customer"? "checked":""?>  /> ลูกค้า:
                                          <input type="radio" class="quotation_source" name="quotation_source"  value="supplier"   <?php echo @$quotation->quotation_source=="supplier"? "checked":""?> /> ตัวแทนจำหน่าย
                                          <input type="radio" class="quotation_source" name="quotation_source"  value="subcontract"  <?php echo @$quotation->quotation_source=="subcontract"? "checked":""?> /> ผู้รับเหมา
                                       </div>
                                       <?php
                                         $display_customer = (isset($quotation->customer_id) && $quotation->customer_id !=0) ? "" : "display:none";     
                                        ?>   
                                          <div id="areaQ_customer" class="area_opt col-md-6 col-sm-6 col-xs-12" style="<?php echo $display_customer?>">
                                                 <select  class="select2_single form-control people_option"  tabindex="-1" name="Qcustomer_id" id="Qcustomer_id" style="width:50%;">        
                                                            <option value="">เลือกข้อมูลลูกค้า</option>    
                                                              <?php 
                                                                   if(isset($customer)){
                                                                       foreach($customer as $key=>$val){
                                                                           $selected ="";
                                                                            if(isset($quotation->customer_id) ){
                                                                                     $selected =$quotation->customer_id == $val->customer_id ? "selected" : "";
                                                                            }   
                                                                              if($val->customer_visible ==STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->customer_id.'" '.$selected.'>'.$val->customer_company.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?>  
                                                </select> 
                                                <!--<button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_customer"><i class="fa fa-plus"></i> Add</button> -->
                                          </div>
                                          <?php       
                                               $display_supplier  = @$quotation->supplier_id !=0 && isset($quotation->supplier_id) ? "" : "display:none"  ; 
                                          ?>
                                          <div id="areaQ_supplier" class="area_opt col-md-6 col-sm-6 col-xs-12" style="<?php echo $display_supplier?>">
                                                 <select  class="select2_single form-control people_option"  tabindex="-1" name="Qsupplier_id" id="Qsupplier_id" style="width:50%;">
                                                 <option value="">เลือก ตัวแทนจำหน่าย</option>        
                                                        <?php 
                                                                     
                                                                   if(isset($supplier)){
                                                                       foreach($supplier as $key=>$val){
                                                                            
                                                                           $selected ="";
                                                                            if(isset($quotation->supplier_id) ){
                                                                                     $selected = $quotation->supplier_id == $val->supplier_id ? "selected" : "";
                                                                            }   
                                                                              if($val->supplier_visible == STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->supplier_id.'" '.$selected.'>'.$val->supplier_fname.'&nbsp;'.$val->supplier_lname.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?> 
                                                </select>
                                                 <!--<button type="button" class="btn btn-dark btn-xs" data-toggle="modal" data-target=".add_supplier"><i class="fa fa-plus"></i> Add</button>        -->
                                          </div>
                                           <?php       
                                               $display_subcontract  = @$quotation->subcontract_id !=0 && isset($quotation->subcontract_id) ? "" : "display:none"  ; 
                                          ?>
                                          <div id="areaQ_subcontract" class="area_opt col-md-6 col-sm-6 col-xs-12" style="<?php echo $display_subcontract?>">
                                                 <select  class="select2_single form-control people_option"  tabindex="-1" name="Qsubcontract_id" id="Qsubcontract_id" style="width:50%;">        
                                                 <option value="">เลือก ผู้รับเหมา</option>
                                                         <?php 
                                                                   if(isset($subcontract)){
                                                                       foreach($subcontract as $key=>$val){
                                                                           $selected ="";
                                                                             if(isset($subcontract_id) ){
                                                                                     $selected =$subcontract_id == $val->supplier ? "selected" : "";
                                                                            }    
                                                                              if($val->subcontract_visible ==STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->subcontract_id.'" '.$selected.'>'.$val->subcontract_fname.'&nbsp;'.$val->subcontract_lname.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?> 
                                                </select>
                                                <!--<button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_subcontract"><i class="fa fa-plus"></i> Add</button> -->
                                          </div>
                                      
                                    </div>
                                    <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">จำนวนเงิน <span class=" ">*</span> </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                         <input  id="quotation_amount"  type="number"   step="100"   data-parsley-type="digits" name="quotation_amount"   class="form-control col-md-7 col-xs-12"  value="<?php echo @$quotation->quotation_amount ?>" > 
                                      </div>
                                    </div>
                                     <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ไฟล์</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">      
                                         <input type="file" name="quotation_file" id="quotation_file" accept="image/*,application/pdf">
                                           <?php if(@$quotation->quotation_path !="") { echo "<a href='".base_url(). $quotation->quotation_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?>
                                        
                                      </div> 
                                    </div>
                                              
                       
                      <input type="hidden" id="q_path" value="">  
              
                   </div>
                    <div class="modal-footer">
                    <span id="btn_qsave"><button type="button"  class="btn btn-primary" id="save_quotation">บันทึก</button></span>
                    <span id="btn_qsubmit" style="display: none;"><button type="submit"  class="btn btn-primary" id="save_quotation">บันทึก</button></span>
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button> 
                  </div>

                </div>    
                </form>   

 
<script>
               $('.quotation_type').change(function() {
                  var val = $(this).val();
                  if(val== 'in') 
                      $('#div_customer').show();
                   else 
                    $('#div_customer').hide();
               });
               $('.quotation_source').change(function() {
                 
                        var val = $(this).val();     
                       $('.people_option').val('');
                       $('.area_opt').hide(); 
                       $('#areaQ_'+val).show();
                      
               });
               $('#save_quotation').click(function(){
 
                            var file2 = new FormData();
                            var countfile = 0;
                                jQuery.each(jQuery('#quotation_file')[0].files, function(i, file) {
                                    file2.append(i, file);
                                    countfile=1;
                                });       
                          $.ajax({                            
                            url:"<?php echo base_url()."service/save_session/quotation"?>",
                            data: $('#add_quotation').serialize()+ '&service_code=' + $('#service_code').val(), 
                            method: "POST",       
                            success:function(result){ 
                                
                                $('.add_quotation').modal('toggle');   
                                    var text_type = ($('.quotation_type:checked').val() == "in") ? "เข้า" : "ออก" ;
                                    var type_input = ($('.quotation_source:checked').val());
                                    var text_source = "";
                                    var name = "";
                                    
                                    if($('.quotation_type:checked').val() == "out")   type_input = "" ;  
                                    if(type_input=="customer"){
                                            text_source = "ลูกค้า";
                                            name = $('#Qcustomer_id option:selected').text();
                                    }else if(type_input=="supplier"){
                                            text_source = "ผู้แทนจำหน่าย";
                                            name = $('#Qsupplier_id option:selected').text();
                                    }else if(type_input=="subcontract"){
                                           text_source = "ผู้รับเหมา";
                                           name = $('#Qsubcontract_id option:selected').text();
                                    }    
 
                                    var qrows =  'Qrows_'+result ;
                                    var html_file = '';
                                    var q_date =  $('#quotation_date').val();
                                    var q_amount =  $('#quotation_amount').val();     
                                    
                                    
                                if(countfile!=0){
                                  $.ajax({
                                        url:"<?php echo base_url()."service/save_session/quotation/"?>"+result+"?type="+$('.quotation_type:checked').val(),
                                        data: file2,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        type: 'POST',
                                        success: function(data){
                                             var q_data = jQuery.parseJSON(data);  
                                             if(q_data.code==200){                 
                                                 var img = '<a href="'+q_data.path+'"  target="_blank"><i class="fa fa-file"></i></a>';
                                                      $('#tb_quotaion tr:last').after('<tr id="'+qrows+'"><td>'+q_date+'</td><td>'+text_type+'</td><td>'+text_source+'</td><td>'+name+'</td><td>'+q_amount+'</td><td>'+img+'</td><td><a href="javascript:delete_quotation(\''+qrows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                      '<input type="hidden" name="quotation_session[]" value="'+result+'">'+'</td></tr>'); 
                                                 
                                             }else{         
                                                  $('#tb_quotaion tr:last').after('<tr id="'+qrows+'"><td>'+q_date+'</td><td></td><td>'+text_source+'</td><td>'+name+'</td><td>'+q_amount+'</td><td>'+img+'</td><td><a href="javascript:delete_quotation(\''+qrows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                   '<input type="hidden" name="quotation_session[]" value="'+result+'">'+'</td></tr>'); 
                                             }
                                        }
                                    });       
                                }
                                
                               else $('#tb_quotaion tr:last').after('<tr id="'+qrows+'"><td>'+$('#quotation_date').val()+'</td><td>'+text_type+'</td><td>'+text_source+'</td><td>'+name+'</td><td>'+$('#quotation_amount').val()+'</td><td></td><td><a href="javascript:delete_quotation(\''+qrows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                '<input type="hidden" name="quotation_session[]" value="'+result+'">'+'</td></tr>');   
                                                                                  
                                    $('#quotation_date').val('');       
                                    $('#quotation_amount').val('');
                                    $('#quotation_file').val('');
                                    $('#div_customer').hide();
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');  
                               
                            }
                        });     
                        
              }) ;
                function delete_quotation(div_id,id){         

                        $.ajax({                            
                            url:"<?php echo base_url()."service/delete_quotation_session/"?>"+(id-1),        
                            method: "POST",   
                            success:function(result){ 
                                $('#'+div_id).remove();    
                            }
                        }); 
               }
                
            </script>  
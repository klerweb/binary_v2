           <?php         
           $this->load->model('customer/customer_address_mod','customer_address');    
 
                 if($customer_address_id !=0){
           ?>
                <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">ที่อยู่</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <?php echo $customer_address->customer_address_desc?>
                  </div>
                </div>
                <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">จังหวัด</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                          <?php echo $this->customer_address->get_province($customer_address->province_id,'province_name')?>
                    </div>
                  </div>
                  <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">อำเภอ</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                          <?php echo $this->customer_address->get_amphur($customer_address->amphur_id,'amphur_name')?>
                    </div>
                  </div>
                  <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">ตำบล</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php echo $this->customer_address->get_district($customer_address->district_id,'district_name')?>
                    </div>                                                              
                  </div>
                  <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">รหัสไปรษณีย์</label>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <?php  echo $this->customer_address->get_zipcode($customer_address->zipcode_id,'zipcode')?>   
                  </div>
                </div>
                <?php }?>

 <script>
 $('#customer_addressOpt').change(function(){
           if($(this).val() !=""){     
                $.ajax({
                            url:"<?php echo base_url()."service/customerAddress/"?>"+$(this).val(),
                            success:function(result){       
                                $('#area_addressCustomer').html(result);   
                            }
                        });
           }
    });
 </script>
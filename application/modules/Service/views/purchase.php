 <?php
 $this->load->model('purchase_mod', 'purchase');
 $this->load->model('supplier/supplier_mod', 'supplier');  
 $this->load->model('subcontract/subcontract_mod', 'subcontract');
 ?>
<div class="row"  id="area_form"> 
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
                  <h2>ใบสั่งซื้อ</h2>&nbsp;&nbsp; <button type="button" class="btn btn-dark btn-xs btn_action open_add_purchase" ><i class="fa fa-plus"></i>  เพิ่ม</button> 
                  
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>  
                  </ul>
                  <div class="clearfix"></div>
            </div>
         <div class="x_content">
                  <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tb_purchase">     
                        <tr>     
                              <th>วันที่</th>
                              <th width="5%">ประเภท</th>
                              <th>รับข้อมูลจาก</th>
                              <th>ชื่อ - นามสกุล</th>
                              <th>จำนวนเงิน</th>
                              <th width="3%">ไฟล์</th>
                              <th width="20%">จัดการ</th>
                            </tr>
                             <?php
                                if(isset($service_id)){
                                    $purchase = $this->purchase->get_purchase($service_id);
                                    if(isset($purchase) && @$purchase !=""){
                                         
                                         foreach($purchase as $key=>$val){
                                            if($val->purchase_visible !=0)
                                            {
                                             ?>
                                              <tr>
                                                <td><?php echo getShowDateFormat($val->purchase_date)?></td>
                                                <td><?php echo $val->purchase_type == "out" ? "ออก" : "เข้า"?></td>
                                                <td><?php if($val->purchase_source) echo $this->purchase->show_sourch($val->purchase_source)?></td>
                                                <td>
                                                    <?php
                                                        if($val->customer_id !=0){echo $this->customer->get_customer($val->customer_id,'customer_company') ; }
                                                        if($val->supplier_id !=0){
                                                            $supplier_data = $this->supplier->get_supplier($val->supplier_id);
                                                            echo $supplier_data->supplier_fname."&nbsp;".$supplier_data->supplier_lname;     
                                                        }
                                                        if($val->subcontract_id !=0){echo $this->subcontract->get_subcontract($val->subcontract_id,'subcontract_fname')."&nbsp;".$this->subcontract->get_subcontract($val->subcontract_id,'subcontract_lname') ; }
                                                          
                                                    ?>   
                                                  </td>
                                                  <td><?php echo $val->purchase_amount?></td>
                                                  <td><?php if($val->purchase_path) { echo "<a href='".base_url(). $val->purchase_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?></td>
                                                  <td>
                                                     <a  class="btn btn-info btn-xs modal-purchase" data-id="<?php echo $val->purchase_id?>"  class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข </a>
                                                    <a href="<?php echo base_url()."service/purchase_delete/".$service_id."/".$val->purchase_id?>"  onclick="return confirm('คุณต้องการลบข้อมูล ?')" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"></i> ลบ </a>
                                                  </td>
                                              </tr>
                                             <?php
                                            }
                                          }
                                    }   
                                }
                             
                             ?>
                                
                        </table> 
                                                                                                                                                                                     
                    </div>
                  </div>
                </div>
</div>
            
            
                         
           <?php 
           // echo $this->load->view('service/popup_customer',array(),true);
            //echo $this->load->view('service/popup_supplier',array(),true);
            //echo $this->load->view('service/popup_subcontract',array(),true);
            ?>
                <script type="text/javascript">
                    $('.modal-purchase').click(function(){
                            var id = $(this).attr('data-id');
                            $('#purchase_form').html('<div align="center"><p>Loading ........</p></div> ');
                            $('.add_purchase').modal('show');
                            $.ajax({
                            url:"<?php echo base_url()."service/purchase_form/".$service_id."/"?>"+id,
                            method: "POST",
                            success:function(result){
                                $('#purchase_form').html(result);
                                $('#btn_psave').hide();
                                $('#btn_psubmit').show();
                                 
                                $('.add_purchase').modal('show');        
                            }      
                            });        
                        });

               $('.open_add_purchase').click(function(){
                      $('#purchase_date').val("");
                      $('#purchase_amount').val("");  
                      $("input[name=purchase_type][value=out]").prop('checked', true);
                      $('#Pdiv_customer').hide();  
                      $('#btn_psave').show();
                      $('#btn_psubmit').hide();
                      $('.add_purchase').modal('show');        
               });
               $('.purchase_type').change(function() {
                  var val = $(this).val();
                  if(val== 'out') 
                      $('#Pdiv_customer').show();
                   else 
                    $('#Pdiv_customer').hide();
               });
               $('.purchase_source').change(function() {
                    
                        var val = $(this).val();       
                       $('.people_option').val('');
                       $('.Parea_opt').hide(); 
                       $('#Parea_'+val).show();
 
                      
               });
               $('#save_customer').click(function(){   
                   var fname = $('#customer_fname').val();
                   var lname = $('#customer_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."customer/save_popup"?>",
                            data: $('#add_customer').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                    $('.add_customer').modal('toggle');   
                                             $('#customer_id').append($("<option></option>").attr("value",result_data.id).text($('#customer_fname').val()+" "+$('#customer_lname').val())); 
                                             $('#customer_id').val(result_data.id);
                                             $('#customer_id').trigger('change');     
                                             $(".add_customer input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;
              
              $('#save_supplier').click(function(){   
                   var fname = $('#supplier_fname').val();
                   var lname = $('#supplier_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."supplier/save_popup"?>",
                            data: $('#add_supplier').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                    $('.add_supplier').modal('toggle');   
                                             $('#supplier_id').append($("<option></option>").attr("value",result_data.id).text($('#supplier_fname').val()+" "+$('#supplier_lname').val())); 
                                             $('#supplier_id').val(result_data.id);
                                             $('#supplier_id').trigger('change');     
                                             $(".add_supplier input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;
              
              $('#save_subcontract').click(function(){   
                   var fname = $('#subcontract_fname').val();
                   var lname = $('#subcontract_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."subcontract/save_popup"?>",
                            data: $('#add_subcontract').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                        $('.add_subcontract').modal('toggle');   
                                             $('#subcontract_id').append($("<option></option>").attr("value",result_data.id).text($('#subcontract_fname').val()+" "+$('#subcontract_lname').val())); 
                                             $('#subcontract_id').val(result_data.id);
                                             $('#subcontract_id').trigger('change');     
                                             $(".add_subcontract input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;
              
              $('#save_purchase').click(function(){
                           
                          $.ajax({                            
                            url:"<?php echo base_url()."service/save_session/purchase"?>",
                            data: $('#add_purchase').serialize()+ '&service_code=' + $('#service_code').val(),
                            method: "POST",
                            success:function(result){ 
                               $('.add_purchase').modal('toggle');        
                               
                                    var text_type = ($('.purchase_type:checked').val() == "in") ? "เข้า" : "ออก" ;
                                    var type_input = ($('.purchase_source:checked').val());
                                    var text_source = "";
                                    var name = "";
                                    
                                    if(type_input=="customer"){
                                           var text_source = "ลูกค้า";
                                           var name = $('#Pcustomer_id option:selected').text();
                                    }else if(type_input=="supplier"){
                                           var text_source = "ผู้แทนจำหน่าย";
                                           var name = $('#Psupplier_id option:selected').text();
                                    }else if(type_input=="subcontract"){ 
                                          var text_source = "ผู้รับเหมา";
                                          var name = $('#Psubcontract_id option:selected').text();
                                    }       
                                    var prows =  'Prows_'+result ;
                                             $('#tb_purchase tr:last').after('<tr id="'+prows+'"><td>'+$('#purchase_date').val()+'</td><td>'+text_type+'</td><td>'+text_source+'</td><td>'+name+'</td><td>'+$('#purchase_amount').val()+'</td><td></td><td><button  href="javascript:delete_purchase(\''+prows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </button></td></tr>'); 
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');  
                               
                            }
                        });     
                        
              }) ;
                function delete_purchase(div_id,id){         

                        $.ajax({                            
                            url:"<?php echo base_url()."service/delete_session/purchase/"?>"+(id-1),        
                            method: "POST",   
                            success:function(result){ 
                                $('#'+div_id).remove();    
                            }
                        }); 
               }
              
              <?php
              if($this->input->get('view') && $purchase_id==""){?>
                $('#area_form').hide();   
                $(".btn_action").hide();
                $("input[type=search]").prop('disabled', false);
               <?php } ?> 
              <?php   if($this->input->get('view') && $purchase_id !=""){?>
                $("input").prop('disabled', true);
                $("textarea").prop('disabled', true);
                $("select").prop('disabled', true);
                $(".btn").prop('disabled', true);     
                $(".btn_action").hide(); 
                $("input[type=search]").prop('disabled', false);  
                <?php }?>
               
        </script>
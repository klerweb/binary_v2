     <?php
       
       $this->load->model('employee/employee_mod', 'employee');      
       $this->load->model('subcontract/subcontract_mod', 'subcontract'); 
       $this->load->model('job_mod', 'job'); 
            
            
       if(isset($job) && @$job_id )  { 
           $job= $job[0];
           $startdateJob = getShowDateFormat2(@$job->job_startdate) ." - ".getShowDateFormat2(@$job->job_enddate) ;          
       }
          
     ?>
<div class="row" id="area_form">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
                  <h2>งาน</h2>&nbsp;&nbsp;<button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_job"><i class="fa fa-plus"></i>  เพิ่ม</button> 
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>  
                  </ul>
                  <div class="clearfix"></div>
            </div>
         <div class="x_content  btn_action">
                       <table   class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tb_job">
 
                        <tr>
                             <th>ชื่อ</th>
                              <th>วันที่เริ่มงาน - วันที่สิ้นสุดงาน</th>
                              <th>ประเภท</th>
                              <th>ชื่อ - นามสกุล</th>
                              <th>สถานะ</th>  
                              <th>เอกสาร</th>  
                              <th>รายละเอียด</th>  
                              <th width="15%">จัดการ</th>
                            </tr>
                             <?php
                            if(isset($service_id) && $service_id !=""){
                                $jobs = $this->job->get_job($service_id);
                                  if(isset($jobs) && $jobs !=""){     
                                        foreach($jobs as $key=>$val){
                                      if($val->job_visible != STATUS_DISACTIVE){
                                             ?> <tr> 
                                                  <td><?php echo $val->job_name?></td>
                                                  <td><?php echo getShowDateFormat($val->job_startdate)?> - <?php echo getShowDateFormat($val->job_enddate)?></td>
                                                  <td><?php echo $this->job->show_sourch($val->job_type)?></td>      
                                                  <td>
                                                   <?php if($val->employee_id !=0){echo $this->employee->get_employee($val->employee_id,'employee_fname')."&nbsp;".$this->employee->get_employee($val->employee_id,'employee_lname') ; } ?>
                                                   <?php if($val->subcontract_id !=0){echo $this->subcontract->get_subcontract($val->subcontract_id,'subcontract_fname')."&nbsp;".$this->subcontract->get_subcontract($val->subcontract_id,'subcontract_lname') ; }?>
                                                   </td>      
                                                  <td><?php echo $this->job->get_jobStatus($val->job_visible)?></td>      
                                                  <td><?php if($val->job_path) { echo "<a href='".base_url(). $val->job_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?></td>
                                                  <td><?php echo $val->job_desc?></td>
                                                  <td>          
                                                     <?php if($val->job_visible==1) {?>
                                                    <a href="<?php echo base_url()."servicerequest/job_delete/".$service_id."/".$val->job_id?>"  onclick="return confirm('คุณต้องการลบข้อมูล ?')" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"></i> ลบ </a>
                                                    <?php }?>
                                                  </td>
                                                </tr>
                                             <?php
                                      }
                                }
                                  }
                            }  
                        
                          
                          ?>  
                        </table>
 
                        </div>                                                                                                                                                                     
                    </div>
                  </div>
                </div>
 
               <?php  //echo $this->load->view('servicerequest/popup_employee',array(),true);  ?>
                <?php // echo $this->load->view('servicerequest/popup_subcontract',array(),true);  ?>
                <script type="text/javascript">
          
 
               
               $('#save_subcontract').click(function(){   
                   var fname = $('#subcontract_fname').val();
                   var lname = $('#subcontract_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."subcontract/save_popup"?>",
                            data: $('#add_subcontract').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                        $('.add_subcontract').modal('toggle');   
                                             $('#subcontract_id').append($("<option></option>").attr("value",result_data.id).text($('#subcontract_fname').val()+" "+$('#subcontract_lname').val())); 
                                             $('#subcontract_id').val(result_data.id);
                                             $('#subcontract_id').trigger('change');     
                                             $(".add_subcontract input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ; 
              
             $('#save_employee').click(function(){   
                   var fname = $('#employee_fname').val();
                   var lname = $('#employee_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."employee/save_popup"?>",
                            data: $('#add_employee').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                        $('.add_employee').modal('toggle');   
                                             $('#employee_id').append($("<option></option>").attr("value",result_data.id).text($('#employee_fname').val()+" "+$('#employee_lname').val())); 
                                             $('#employee_id').val(result_data.id);
                                             $('#employee_id').trigger('change');     
                                             $(".add_employee input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;   
             

             <?php
              if($this->input->get('view') && $job_id==""){?>
                $('#area_form').hide();   
                $(".btn_action").hide();
                $("input[type=search]").prop('disabled', false);
               <?php } ?> 
              <?php   if($this->input->get('view') && $job_id !=""){?>
                $("input").prop('disabled', true);
                $("textarea").prop('disabled', true);
                $("select").prop('disabled', true);
                $(".btn").prop('disabled', true);     
                $(".btn_action").hide(); 
                $("input[type=search]").prop('disabled', false);  
                <?php }?>          
        </script>

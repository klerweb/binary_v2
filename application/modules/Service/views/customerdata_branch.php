           <?php         
           $this->load->model('customer/customer_branch_mod','customer_branch');    
 
                 if($customer_branch_id !=0){
           ?>
                <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">ผู้ติดต่อ</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <?php echo $customer_branch->customer_branch_contact?>
                  </div>
                </div>
                <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">เบอร์โทรศัพท์</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                          <?php echo $customer_branch->customer_branch_tel ?>
                    </div>
                  </div>
       
                  <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12">แผนที่</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                          <?php   echo '<a class="btn btn-success btn-xs" target="_blank" href="https://www.google.co.th/maps/place/'.urlencode($this->customer->get_customer($customer_branch->customer_id,'customer_company')).'/@'.$customer_branch->customer_branch_latitude.','.$customer_branch->customer_branch_longitude.',17z"><i class="fa fa-map-marker"></i> Google Map</a>';      ?>
                    </div>
                  </div> 
                  <div class="form-group">     
                  <label class="control-label col-md-5 col-sm-5 col-xs-12"></label>
                  <div class="col-md-4 col-sm-4 col-xs-12">   
                            <button type="button" class="btn btn-dark btn_action btn-xs" id="add_branch"> <i class="fa fa-plus"></i> เพิ่ม</button>
                    </div>
                  </div> 
                   
                <?php }?>
               

 <script>
 $('#customer_addressOpt').change(function(){
           if($(this).val() !=""){     
                $.ajax({
                            url:"<?php echo base_url()."service/customerBranch/"?>"+$(this).val(),
                            success:function(result){       
                                $('#area_addressCustomer').html(result);   
                            }
                        });
           }
    });
 $('#add_branch').click(function(){
         var customer_choose = $('#customerOpt').val();
         var customer_branch_choose = $('#customer_branch').val();
     
        var hasCusDup = false;
        var hasCusBranchDup = false;
          $('.customer_id_text').each(function (index) {
            var $inputsWithSameValue = $('input[value=' + $(this).val() + ']');
            var $inputsWithSameValue2 = $('input[value=' + $('.customer_branch_text eq:'+index).val() + ']');
            hasCusDup = $inputsWithSameValue.length > 1 ;              
            hasCusBranchDup = $inputsWithSameValue.length > 1 ;              
        });
           var inputs_customer  =   $('.customer_id_text');
           var inputs_branch  =   $('.customer_branch_text');
 
           var check_custmer  = false ;
           var check_branch  = false ;
            $.each(inputs_customer,function(k,v){
                var getVal  =   $(v).val();                    
                if($('#cutomerOpt').val() == getVal){
                    check_custmer = true ; 
                }     
            });
            $.each(inputs_branch,function(k,v){
                var getVal  =   $(v).val();               
                if($('#customer_branch').val() == getVal){
                    check_branch = true ; 
                }     
            });
            if($('#customer_branch').val() == ""){
                alert('ขออภัยค่ะ !! ไม่สามารถเพิ่มข้อมูลได้  กรุณาเลือกสาขาค่ะ');
            }else{
               if(!check_custmer || !check_branch){
                    var div_id = 'rows_'+$('#cutomerOpt').val()+'_'+$('#customer_branch').val() ;
                 
                    $('#tb_branch tr:last').after('<tr id="'+div_id+'"><td><input type="hidden" name="customer_id[]" class="customer_id_text" value="'+$('#cutomerOpt option:selected').val()+'"><input type="hidden" name="customer_branch[]" class="customer_branch_text" value="'+$('#customer_branch option:selected').val()+'">'+$('#cutomerOpt option:selected').text()+' สาขา '+$('#customer_branch option:selected').text()+'</td><td> <button  href="javascript:delete_branch(\''+div_id+'\')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </button></td></tr>');            
                }else{
                    alert('ขออภัยค่ะ !!  ท่านกรอก ข้อมูลซ้ำโปรดตรวจสอบข้อมูลอีกครั้งค่ะ');
                } 
            }       
            
            
         $('.delete_branch').click(function(){
                
                   var r = confirm("คุณต้องการลบข้อมูล ?");
                    if (r == true) {
                        $(this).parent().parent().remove();
                    } 
            });
    }) ;
   function delete_branch(div_id){         
        $('#'+div_id).remove();   
   }
 </script>
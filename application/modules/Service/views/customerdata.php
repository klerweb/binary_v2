                          <?php
                           $this->load->model('customer_mod', 'customer');      
                           $this->load->model('customer_branch_mod', 'customer_branch');      
                           $this->load->model('service_branch_mod', 'service_branch');      
                           $option2 ="<option value=''>สาขา</option>";
                           foreach($customer_branch as $key=>$val){
                               $option2 .="<option value='".$val->customer_branch_id."' >".$val->customer_branch_name."</option>";
                           }
                           ?>
                           
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">สาขา</label>
                              <div class="col-md-6 col-sm-6 col-xs-12 ">    
                                       <select class="select2_single form-control"  tabindex="-1" name="customer_branch" id="customer_branch" >
                                       <?php echo $option2?>
                                       </select>
                              </div>
                           </div>                                              
                            <div id="area_addressCustomer">
                            <?php 
                              /* if(isset($customer_address_id) && $customer_address_id !="")
                                echo Modules::run('service/customerBranch',$customer_branch_id);*/  
                            ?>
                            </div>          
                            <div id="area_multi_branch">
                            <table id="tb_branch" align="center" width="75%">
                               <?php
                                if(isset($service_id)){
                                     // get -multi branch
                                     $service_branch = $this->service_branch->get_service_branchByservice($service_id);
                                      if(isset($service_branch) && @$service_branch){
                                              
                                          foreach($service_branch as $key=>$val){
                                                 
                                             echo '<tr>';
                                             echo '<td>'.$this->customer->get_customer($val->customer_id,'customer_company').' สาขา '.$this->customer_branch->get_customer_branch($val->customer_branch_id,'customer_branch_name').'</td>';
                                             echo '<td><a href="'.base_url().'service/delete_service_brach/'.$val->service_branch_id.'/'.$service_id.'" class="btn btn-danger btn-xs" onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')" > ลบ </a>';
                                             echo '<input type="hidden"  value="'.$val->customer_branch_id.'" class="customer_branch_text">';
                                             echo '<input type="hidden"  value="'.$val->customer_id.'" class="customer_id_text">';
                                             echo '</td>';
                                             echo '</tr>';  
                                          }
                                      }
                                            
                                }
                               ?>
                               <tr>
                               <td style="border:0px"></td>
                               <td style="border:0px" width="5%"></td>
                               </tr>
                            </table>
                        </div> 
                           
             <script src="<?php echo base_url()?>assets/js/select/select2.full.js"></script>                                 
<script> 
    $(document).ready(function() {
                    $(".select2_single").select2({}); 
    });     
                                                                  
     $('#customer_branch').change(function(){    
           if($(this).val() !=""){     
                $.ajax({
                            url:"<?php echo base_url()."service/customerBranch/"?>"+$(this).val(),
                            success:function(result){       
                                $('#area_addressCustomer').html(result);  
                            }
                        });
           }
    });   
        $('#zipcode_id').change(function(){
              $.ajax({
                            url:"<?php echo base_url()."service/addressByZipcode/"?>"+$(this).val(),
                            success:function(result){       
                                $('#arer_popup_address').html(result);  
                            }
                        });
     
        });   
        $('#save_address').click(function(){
                if($('#address_name').val() ==""){
                   alert('โปรดกรอกชื่อที่อยู่ด้วยค่ะ');
                   $('#address_name').focus();
               }else{
                    
                      $.ajax({
                            url:"<?php echo base_url()."service/save_popupaddress"?>",
                            data:$('#add_address_popup').serialize()+ '&customer_id=' + $('#cutomerOpt').val(),      
                            method: "POST",
                            success:function(result){
                               
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                    
                                       alert('บันทึกเรียบร้อย')  ;  
                                       $('#customer_addressOpt').append($("<option></option>").attr("value",result_data.id).text(result_data.address_name)); 
                                       $('#customer_addressOpt').val(result_data.id);
                                       $('#customer_addressOpt').trigger('change');
                                       $("#add_address_popup input").val(""); 
                                       $("#add_address_popup textarea").val(""); 
                                       $("#arer_popup_address").html(""); 
                                       $("#zipcode_id").val(""); 
                                       $("#zipcode_id").trigger('change');
                                       $('.add_address').modal('toggle');    
                                       
                                    }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });      
               }         
                
        });    
</script>
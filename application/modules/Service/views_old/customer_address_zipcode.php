           <?php
            $this->load->model('customer/customer_address_mod','customer_address');   
              
            $province_id = $address[0]->province_id;
            $amphur_id = $address[0]->amphur_id;
 
 
           ?>                                 
                                            <div class="form-group">     
                                              <label class="control-label col-md-5 col-sm-5 col-xs-12">จังหวัด</label>
                                              <div class="col-md-7 col-sm-7 col-xs-12" id="pop_province">  
                                                 <input type="hidden" name="province_id" id="province_id" value="<?php echo $province_id?>"> 
                                                 
                                                 <?php echo $this->customer_address->get_province($province_id,'province_name')?>      
                                                </div>
                                              </div>
                                            <div class="form-group">     
                                              <label class="control-label col-md-5 col-sm-5 col-xs-12">อำเภอ</label>
                                              <div class="col-md-4 col-sm-4 col-xs-12"  id="pop_amphur">
                                              <input type="hidden" name="amphur_id" id="amphur_id"  value="<?php echo $amphur_id?>">
                                              <?php echo $this->customer_address->get_amphur($amphur_id,'amphur_name')?>    
                                              </div>
                                              </div>
                                              
                                            <div class="form-group">     
                                              <label class="control-label col-md-5 col-sm-5 col-xs-12">ตำบล</label>
                                              <div class="col-md-4 col-sm-4 col-xs-12" id="pop_distinct">
                                                <select name="district_id" id="district_id" class="select2_single form-control" > 
                                                 
                                                      <?php 
                                                           if(isset($address)){
                                                               foreach($address as $key=>$val){               
                                                                        echo '<option value="'.$val->district_id.'">'. $this->customer_address->get_district($val->district_id,'district_name').'</option>';    
                                                               }      
                                                           }   
                                                      ?>  
                                                       </select>                                       
                                                </select>
                                              </div>                                                  
                                              </div>
<!-------------- popup subcontract ------------------->           

            <div class="modal fade add_subcontract" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">?</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มข้อมูลผู้รับเหมา</h4>
                  </div>
                   <div class="modal-body">
                     <form id="add_subcontract" data-parsley-validate  class="form-horizontal form-label-left" action="" method="post">   
 
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อบริษัท</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="subcontract_company" name="subcontract_company"   class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อ <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="subcontract_fname" name="subcontract_fname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">นามสกุล <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="subcontract_lname" name="subcontract_lname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">เบอร์โทร</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="subcontract_tel" name="subcontract_tel" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">อีเมล์</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="subcontract_email" name="subcontract_email" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ที่อยู่</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">      
                            <textarea class="form-control" rows="3" name="subcontract_address" id="subcontract_address" required="required"></textarea> 
                          </div>
                        </div>
                  </div>
                    <div class="modal-footer">
                    <button type="button"  class="btn btn-primary" id="save_subcontract">บันทึก</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button> 
                  </div>

                </div>    
                </form>
              </div>
            </div>
           
       <!-------------- popup subcontract ------------------->
     <?php
      // include("tab.php"); 
      $this->load->model('employee/employee_mod', 'employee');      
       $this->load->model('subcontract/subcontract_mod', 'subcontract'); 
       $this->load->model('servicerequest/job_mod', 'job');
       
       $url_save ="";
       $startend ="";
        
       if(isset($service) && $service !="")  {    $service= $service[0];     }
        
           
       $url_save = $id =="" ? base_url()."service/save_description" : base_url()."service/save_description/".$id;
       $startend = $id ? getShowDateFormat2(@$service->service_date)." - ".getShowDateFormat2(@$service->service_end) : ""; 
      // $service_code2 = $id ? $service->service_code :   service_format2($this->service->serviceTmp()) ;
          
 
     ?> 
     <style>
     #tb_branch td{ 
         border:1px solid #ddd; 
         padding:7px 32px;
         
     } 
     .daterangepicker{z-index:1151 !important;}
 
     </style>
     <div class="popup_alert"><?php echo @$popup_alert?></div>
 
     <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $url_save?>" method="post">  
     <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                   <div class="x_title">
                              <h2>รายละเอียด</h2>
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>  
                              </ul>
                              <div class="clearfix"></div>
                           </div>
                    <div class="x_content">
                       
                        <div class="form-group">  
 
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">รหัส</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="service_code" name="service_code" value="<?php echo $service_code?>" class="form-control col-md-7 col-xs-12" readonly="readonly">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่เริ่มต้น - วันที่สิ้นสุด <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="service_startend" name="service_startend"  value="<?php echo $startend?>"  required="required"   readonly="readonly" class="date-pickerTwin form-control col-md-7 col-xs-12"   type="text">
                          </div>
                        </div>
 
                         <div class="form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12">ระยะเวลาการทำงาน <span class="required">*</span></label>
                             <div class="col-md-3 col-sm-3 col-xs-12 ">
                                 <div class="input-group">
                                  <input type="number" step=".30" min="1"  class="form-control"  id="service_time" name="service_time"   required="required"  value="<?php echo @$service->service_time?>">
                                  <span class="input-group-btn"><button type="button" class="btn btn-default" style="cursor:auto;">ชั่วโมง</button></span>   
                                </div>
                            </div>
                        </div>      
                            
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" rows="3" name="service_desc" id="service_desc" ><?php echo @$service->service_desc?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ข้อมูลสรุป </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" rows="3" name="service_summary" id="service_summary"><?php echo @$service->service_summary?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ประเภทบริการ</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php    
                            if(isset($type)){
                                ?>
                                <select  class="select2_single form-control"  tabindex="-1" name="type_id" id="service_type_id"> 
                                <?php   
                                foreach($type as $key=>$val){
                                    $selected ="";
                                    if(isset($service->service_type_id) ){
                                            $selected =$service->type_id == $val->type_id ? "selected" : "";
                                    } 
                                    echo '<option value="'. $val->type_id.'" '.$selected.'>'. $val->type_name.'</option>';
                                }
                                ?>
                                </select>
                                <?php   
                            }
                          ?>   
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">สถานะ</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                              
                                          <input type="radio" name="service_visible"  value="3"     <?php echo @$service->service_visible=="3"? "checked":"checked"?>  />บริการ
                                          <input type="radio" name="service_visible"  value="5"   <?php echo @$service->service_visible=="5"? "checked":""?> /> ยกเลิก
                                          <input type="radio" name="service_visible"  value="6"   <?php echo @$service->service_visible=="6"? "checked":""?> /> ดำเนินการสำเร็จ
                              </div>
                        </div>
                       
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">พนักงานรับเรื่อง</label>
                          <div class="col-md-6 col-sm-6 col-xs-12 ">    
                                  <select  class="select2_single form-control"  tabindex="-1" name="employee_id" value="employee_id">      
                                  <?php 
                                       if(isset($employee)){
                                           foreach($employee as $key=>$val){
                                                $selected ="";
                                                if(isset($service->employee_id) ){
                                                        $selected =$service->employee_id == $val->employee_id ? "selected" : "";
                                                }                                    
                                                  if($val->employee_visible ==STATUS_ACTIVE){
                                                    echo '<option value="'.$val->employee_id.'"  '.$selected.'>'.$val->employee_fname.'&nbsp;'.$val->employee_lname.'</option>';  
                                                 }  
                                           }      
                                       }   
                                  ?>  
                                   </select>
                          </div>
                        </div>                     
                    </div> 
              </div>
         </div>
     </div>
     <!-- -------------------- Custermer ----------------------- -->   
          <div class="row">
             <div class="col-md-12 col-sm-12 col-xs-12">
                   <div class="x_panel">
                         <div class="x_title">
                              <h2>ข้อมูลลูกค้า</h2> &nbsp;&nbsp; <!--<button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_customer"><i class="fa fa-plus"></i>  เพิ่ม</button> -->
 
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>  
                              </ul>
                              <div class="clearfix"></div>
                           </div>
                       <div class="x_content">
                         <div id="area_customer_option"><?php echo $this->load->view('service/customer_option',array('customer_id'=>@$service->customer_id))?></div>
                            
                        <div id="area_customer">
                        <?php 
                            if(isset($service->customer_id)){
                                  echo Modules::run('service/customerData',$service->customer_id,'',$service_id);  
                          }
                          ?>
                        </div>
                          <div id="area_multi_branch_db">
                            <table id="tb_branch" align="center" width="75%">
                               <tr>
                               <td style="border:0px"></td>
                               <td style="border:0px" width="5%"></td>
                               </tr>
                            </table>
                        </div>  
                        </div>
                    </div>
             </div>
        </div>
                
        <!-- -------------------- Custermer ----------------------- --> 
          
        <!-- -------------------- Quation -------------------------- -->
          <?php echo $this->load->view('quotation',$data)?>
          
          
        <!-- --------------------Quation --------------------------- -->
        
          <!-- -------------------- Purchase -------------------------- -->
          <?php echo $this->load->view('purchase',$data)?>
        <!-- --------------------Purchase --------------------------- -->
        
        <!-- -------------------- Job -------------------------- -->
          <?php echo $this->load->view('job',$data)?>
        <!-- --------------------Job --------------------------- -->
        
        
         <!-- ------------------- Expensen ------------------------- -->
          <?php echo $this->load->view('expenses',$data)?>
        <!-- --------------------Expensen --------------------------- -->
        
        
        <div class="row">
             <div class="col-md-12 col-sm-12 col-xs-12">
                   <div class="x_panel">
                       <div class="x_content">  
                       <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            <button type="submit" class="btn btn-success btn_action">บันทึก</button>
                            <button type="button" class="btn btn-primary btn_action" onclick="window.location.href='<?php echo base_url()?>service'">ยกเลิก</button>
                            
                          </div>
                        </div>
                        </div>
                    </div>
             </div>
        </div>  
        </form>  
                          
       <!-- ------------ popup customer ----------------- -->           

            <div class="modal fade add_customer" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มข้อมูลลูกค้า</h4>
                  </div>
                   <form id="add_customer" data-parsley-validate  class="form-horizontal form-label-left" action="" method="post">   
 
                   <div class="modal-body">      
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อบริษัท</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="customer_company" name="customer_company"   class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อ <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="customer_fname" name="customer_fname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">นามสกุล <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="customer_lname" name="customer_lname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">เบอร์โทร</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="customer_tel" name="customer_tel" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">อีเมล์</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="customer_email" name="customer_email" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                  </div>
                    <div class="modal-footer">
                    <button type="button"  class="btn btn-primary btn_action" id="save_customer">บันทึก</button>
                    <button type="button" class="btn btn-default btn_action" data-dismiss="modal">ยกเลิก</button> 
                  </div>
                   </form>
                </div>    
                
              </div>
            </div>
           
       
         <script>
         $(document).ready(function(){
             <?php if($popup_alert !=""){ ?>
                    setTimeout(function(){ $('.popup_alert').hide();}, 5000);
                 
             <?php } ?>  
              $('#save_customer').click(function(){   
                   var fname = $('#customer_fname').val();
                   var lname = $('#customer_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."customer/save_popup"?>",
                            data: $('#add_customer').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                          
                                       $.ajax({
                                        url:"<?php  echo base_url()."service/customer_option"?>?ajax=1",
                                        success:function(resultHtml){
                                             $('#area_customer_option').html(resultHtml);  
                                             $('.add_customer').modal('toggle');   
                                             $(".add_customer input").val("");
                                               
                                             $('#cutomerOpt').val(result_data.id);
                                             $('#cutomerOpt').trigger('change');
                                             
                                             alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                        }   
                                    });
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;
                   
         }) ;  
         
          <?php
          if($this->input->get('view')){
          ?>
            $("input").prop('disabled', true);
            $("textarea").prop('disabled', true);
            $("select").prop('disabled', true);
            $(".btn").prop('disabled', true);
            $("a.btn").hide();
            $(".btn_action").hide();
            $(".btn_action").hide();
          <?php }?>
         
         </script>
 
           <!-- ------------ popup customer ---------------- --->  
          <!-- -------------------------- popup add address ------------- -->         
           <div class="modal fade add_address" tabindex="-1" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title" id="myModalLabel">เพิ่มที่อยู่</h4>
                                      </div>
                                      
                                      
                                     <!-- body --> 
                                      <div class="modal-body">
                                      <form id="add_address_popup"   class="form-horizontal form-label-left" action="" method="post">
                                           <div class="form-group">     
                                              <label class="control-label col-md-5 col-sm-5 col-xs-12">ชื่อที่อยู่  <span class="required">*</span></label>
                                              <div class="col-md-4 col-sm-4 col-xs-12">
                                                <input type="text" id="customer_address_name" name="customer_address_name" class="form-control col-md-7 col-xs-12">    
                                              </div>
                                            </div>
                                           <div class="form-group">     
                                              <label class="control-label col-md-5 col-sm-5 col-xs-12">รายละเอียดที่อยู่</label>
                                              <div class="col-md-4 col-sm-4 col-xs-12">
                                                 <textarea class="form-control" rows="3" name="customer_address_desc" id="customer_address_desc"></textarea> 
                                              </div>
                                            </div>
                                            <div class="form-group">     
                                              <label class="control-label col-md-5 col-sm-5 col-xs-12">รหัสไปรษณีย์</label>
                                             <div class="col-md-7 col-sm-7 col-xs-12">          
                                               <select class="select2_single form-control"  tabindex="-1" style="width:50%" name="zipcode" id="zipcode_id">
                                                <option value="">กรุณาเลือกรหัสไปรษณีย์</option>
                                                  <?php  
                                                          $zipcode = $this->customer_address->get_zipcodeGroup();   
                                                          if(isset($zipcode)){
                                                               foreach($zipcode as $key=>$val){          
                                                                      echo '<option value="'.$val->zipcode.'">'.$val->zipcode.'</option>';   
                                                               }      
                                                           }      
                                                  ?>  
                                                 </select>    
                                                </div>
                                            </div> 
                                            <div id="arer_popup_address"></div>
                                            <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="save_address">บันทึก</button> 
                                        <button type="button" class="btn btn-default close_popaddress" data-dismiss="modal">ยกเลิก</button>
                                      </div>
                                      </form>    
                                      </div> 
                                  </div>
                            </div>
                            </div>     
           <script>
        $('#zipcode_id').change(function(){
              $.ajax({
                            url:"<?php echo base_url()."service/addressByZipcode/"?>"+$(this).val(),
                            success:function(result){       
                                $('#arer_popup_address').html(result);  
                            }
                        });
     
        });   
        $('#save_address').click(function(){
                if($('#address_name').val() ==""){
                   alert('โปรดกรอกชื่อที่อยู่ด้วยค่ะ');
                   $('#address_name').focus();
               }else{
                    
                      $.ajax({
                            url:"<?php echo base_url()."service/save_popupaddress"?>",
                            data:$('#add_address_popup').serialize()+ '&customer_id=' + $('#cutomerOpt').val(),      
                            method: "POST",
                            success:function(result){
                               
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                    
                                       alert('บันทึกเรียบร้อย')  ;  
                                       $('#customer_addressOpt').append($("<option></option>").attr("value",result_data.id).text(result_data.address_name)); 
                                       $('#customer_addressOpt').val(result_data.id);
                                       $('#customer_addressOpt').trigger('change');
                                       $("#add_address_popup input").val(""); 
                                       $("#add_address_popup textarea").val(""); 
                                       $("#arer_popup_address").html(""); 
                                       $("#zipcode_id").val(""); 
                                       $("#zipcode_id").trigger('change');     
                                       $('.close_popaddress').trigger('click') ;
                                       
                                    }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });      
               }         
                
        });               
        </script> 
           <!-- -------------------------- popup add address ------------- --> 
                 
            <!-- --------------------------popup quotation---------------------------- -->
            <div class="modal fade add_quotation" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มใบสั่งซื้อ</h4>
                  </div>
                  <div id="quotation_form">
                     <?php echo $this->load->view('quotation_form');?>
                  </div>
                </div>    
                
              </div>
            </div>    
                  
            <!-- --------------------------popup quotation---------------------------- -->
            
            <!-- --------------------------popup purchase---------------------------- -->
              <div class="modal fade add_purchase" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มใบสั่งซื้อ</h4>
                  </div> 
                  <div id="purchase_form"> 
                    <?php echo $this->load->view('purchase_form');?>
                </div> 
              </div>
            </div>

            <!-- --------------------------popup purchase---------------------------- -->
            
            <!-- ------------------ Popup Job ---------------------- -->
               <div class="modal fade add_job" tabindex="0" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">ใบตารางงาน</h4>
                  </div>
                  <form id="add_job"   class="form-horizontal form-label-left"   method="post"  enctype="multipart/form-data">
                   <div class="modal-body">
                        
                                    <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อ  <span class="required">*</span></label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="job_name" name="job_name" class="form-control col-md-7 col-xs-12" value="<?php echo @$job->job_name?>" required="required">
                                        <input type="hidden" id="service_id" name="service_id"  value="<?php echo @$service_id?>" >
                                      </div>
                                    </div>
                                    <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่เริ่มงาน และสิ้นสุด  <span class="required">*</span></label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="job_startdate" name="job_startdate"  readonly="readonly" class="date-pickerTwin form-control col-md-7 col-xs-12" value="<?php echo @$startdateJob?>"  required="required" >
                                      </div>
                                    </div>
                                     
                                     <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ประเภท</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">                                       
                                          <input type="radio" class="job_type" name="job_type"   value="employee" <?php echo @$job->job_type=="employee"? "checked":""?>  required="required" /> พนักงาน:
                                          <input type="radio" class="job_type" name="job_type"   value="subcontract" <?php echo @$job->job_type=="subcontract"? "checked":""?> /> ผู้รับเหมา
                                      </div>
                                    </div>  
                                    <?php             
                                       if(@$job_id){   
                                           if(@$job->job_type == "subcontract"){
                                               $style_emp = "display:none";
                                               $style_subcontact = "";
                                           }else{
                                              $style_emp = "";
                                              $style_subcontact = "display:none"; 
                                           }
                                       }else{
                                           $style_emp ="display:none";
                                           $style_subcontact ="display:none";
                                       }
                                       
                                    ?>
                                    <div id="Jarea_employee" class="col-md-offset-3 col-md-9 col-sm-9 col-xs-12 area_option" style="<?php echo $style_emp?>">
                                      <div class="form-group">  
                                        <select  class="select2_single form-control people_option"  tabindex="-1" name="Jemployee_id" id="Jemployee_id" style="width:50%;" >        
                                                            <option value="">เลือกพนักงาน</option>    
                                                              <?php 
                                                                   if(isset($employee)){
                                                                       foreach($employee as $key=>$val){
                                                                           $selected ="";
                                                                            if(isset($job->employee_id) ){
                                                                                     $selected =$job->employee_id == $val->employee_id ? "selected" : "";
                                                                            }   
                                                                              if($val->employee_visible ==STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->employee_id.'" '.$selected.'>'.$val->employee_fname.'&nbsp;'.$val->employee_lname.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?>  
                                                </select>
                                              <!--  <button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_employee"><i class="fa fa-plus"></i> Add</button>-->
                                        </div>
                                    </div>
                                     <div id="Jarea_subcontract"  class="col-md-offset-3 col-md-9 col-sm-9 col-xs-12 area_option" style="<?php echo $style_subcontact?>">
                                        <div class="form-group">  
                                        <select  class="select2_single form-control people_option"  tabindex="-1" name="Jsubcontract_id" id="Jsubcontract_id" style="width:50%;">        
                                                            <option value="">เลือกข้อมูลผู้รับเหมา </option>    
                                                              <?php 
                                                                   if(isset($subcontract)){
                                                                       foreach($subcontract as $key=>$val){
                                                                           $selected ="";
                                                                            if(isset($job->subcontract_id) ){
                                                                                     $selected =$job->subcontract_id == $val->subcontract_id ? "selected" : "";
                                                                            }   
                                                                              if($val->subcontract_visible ==STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->subcontract_id.'" '.$selected.'>'.$val->subcontract_fname.'&nbsp;'.$val->subcontract_lname.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?>  
                                                </select>
                                                <!--<button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_subcontract"><i class="fa fa-plus"></i> Add</button>-->
                                        </div>
                                    </div>  
                                     <div>
                                      <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">สถานะ</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">                                       
                                            <select  class="select2_single form-control"  tabindex="-1" name="job_visible" id="job_visible" style="width:50%;" required="required">          
                                                              <?php 
                                                                   $array_status = $this->job->get_jobStatus();
                                                                   if(isset($array_status) && @$array_status !=""){
                                                                         foreach($array_status as $key=>$val){                                               
                                                                           $selected ="";
                                                                               $selected = @$job->job_visible == $key ? "selected" : "";
                                                                            
                                                                               echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';   
                                                                       }
                                                                   }
                                                                             
                                                                      
                                                              ?>  
                                                </select>  
                                      </div>
                                    </div>
                                    </div>
                                   <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด</label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control" rows="3" name="job_desc" id="job_desc" ><?php echo @$job->job_desc?></textarea>
                                  </div>
                                </div>
                                <div class="form-group ">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ไฟล์</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">      
                                         <input type="file" name="job_file"  id="job_file"  accept="image/*,application/pdf">
                                         <?php if(@$job->job_path !="") { echo "<a href='".base_url(). $job->job_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?>
                                        
                                      </div>
                                    </div>
 
                   </div>
                    <div class="modal-footer">
                    <button type="button"  class="btn btn-primary" id="save_job">บันทึก</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" >ยกเลิก</button> 
                  </div>
                   </form> 
                </div>    
                
              </div>
            </div>
            
            <script type="text/javascript">
          
               $('.job_type').click(function(){    
                   $('.people_option').val('');
                   $('.area_option').hide();
                   $('#Jarea_'+$(this).val()).show();
               }); 
                $('#save_job').click(function(){
                          var file2 = new FormData();
                            var countfile = 0;
                                jQuery.each(jQuery('#job_file')[0].files, function(i, file) {
                                    file2.append(i, file);
                                    countfile=1;
                                });
                                 
                          $.ajax({                            
                            url:"<?php echo base_url()."service/save_session/job"?>",
                            data: $('#add_job').serialize()+ '&service_code=' + $('#service_code').val(),
                            method: "POST",
                            success:function(result){ 
                               $('.add_job').modal('toggle');        
                               
                                    var text_type = ($('.job_type:checked').val() == "employee") ? "พนักงาน" : "ผู้รับเหมา" ;
                                    var type_input = ($('.job_source:checked').val());
                                    var visible_input = ($('.job_visible option:selected').text());
                                    
                                    if($('.job_type:checked').val() == "employee"){
                                           var text_source = "ลูกค้า";
                                           var name = $('#Jemployee_id  option:selected').text();
                                    }else{
                                          var text_source = "ผู้รับเหมา";
                                          var name = $('#Jsubcontract_id option:selected').text();
                                    }       
                                    var jrows =  'Jrows_'+result ;
                                    // upload file 
                                   
                                   
                                    var job_name =  $('#job_name').val();
                                    var job_startdate =  $('#job_startdate').val();    
                                    var job_desc =  $('#job_desc').val();    
                                    var job_file =  $('#job_file').val();    
                                    var job_visible =  $('#job_visible option:selected').text();    
                                    
                                     
                                 if(countfile!=0){
                                  $.ajax({
                                        url:"<?php echo base_url()."service/save_session/job/"?>"+result,
                                        data: file2,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        type: 'POST',
                                        success: function(data){
                                             var q_data = jQuery.parseJSON(data);     
                                             if(q_data.code==200){
     
                                                 var img = '<a href="'+q_data.path+'"  target="_blank"><i class="fa fa-file"></i></a>';
                                                      $('#tb_job tr:last').after('<tr id="'+jrows+'"><td>'+job_name+'</td><td>'+job_startdate+'</td><td>'+text_type+'</td><td>'+name+'</td><td>'+job_visible+'</td><td>'+img+'</td><td>'+job_desc+'</td><td><a  href="javascript:delete_job(\''+jrows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                        '<input type="hidden" name="job_session[]" value="'+result+'">'+'</td></tr>');   
                                                 
                                             }else{
                                                 $('#tb_job tr:last').after('<tr id="'+jrows+'"><td>'+job_name+'</td><td>'+job_startdate+'</td><td>'+text_type+'</td><td>'+name+'</td><td>'+job_visible+'</td><td>'+img+'</td><td>'+job_desc+'</td><td><a  href="javascript:delete_job(\''+jrows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                 '<input type="hidden" name="job_session[]" value="'+result+'">'+'</td></tr>'); 
                                             }
                                        }
                                    });       
                                }
                               else  $('#tb_job tr:last').after('<tr id="'+jrows+'"><td>'+job_name+'</td><td>'+job_startdate+'</td><td>'+text_type+'</td><td>'+name+'</td><td>'+job_visible+'</td><td></td><td>'+job_desc+'</td><td><a  href="javascript:delete_job(\''+jrows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                               '<input type="hidden" name="job_session[]" value="'+result+'">'+'</td></tr>');  
                               
                                      $('#job_name').val('');
                                      $('#job_startdate').val('');    
                                       $('#job_desc').val('');    
                                     $('#job_file').val('');   
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');  
                               
                            }
                        });     
                        
              }) ;
                function delete_job(div_id,id){         

                        $.ajax({                            
                            url:"<?php echo base_url()."service/delete_session/job/"?>"+(id-1),        
                            method: "POST",   
                            success:function(result){ 
                                $('#'+div_id).remove();    
                            }
                        }); 
               }
               
               </script>
            <!-- ------------------ Popup Job ---------------------- -->
            
             <!-- ------------------ Popup  expenses---------------------- -->
               <div class="modal fade add_expenses" tabindex="0" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มค่าใช้จ่าย</h4>
                  </div>
                 
                   <div class="modal-body">
                       <form id="add_expenses"   class="form-horizontal form-label-left"    method="post" enctype="multipart/form-data">
                                         
                                        <div class="form-group">     
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อค่าใช้จ่าย</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="expenses_name" name="expenses_name" class="form-control col-md-7 col-xs-12"  value="<?php echo @$expense->expenses_name?>" >
                                            
                                          </div>
                                        </div>
                                        <div class="form-group">     
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="expenses_date" name="expenses_date"  readonly="readonly"  class="date-picker form-control col-md-7 col-xs-12" style="width: 30%;"  value="<?php echo @getShowDateFormat($expense->expenses_date)?>" >
                                            <input type="hidden" id="service_id" name="service_id"  value="<?php echo @$service_id?>"    >
                                          </div>
                                        </div> 
                                        <div class="form-group">     
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">จำนวนเงิน</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number"   step="100"  id="expenses_amount" name="expenses_amount" value="<?php echo @$expense->expenses_amount?>"    class="form-control col-md-7 col-xs-12"   >
                                          </div>
                                        </div>
                                      <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">สถานะ</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">                                       
                                            <select  class="select2_single form-control"  tabindex="-1" name="expenses_visible" id="expenses_visible" style="width:50%;" required="required">          
                                                              <?php 
                                                                   $array_status = $this->expenses->get_expensesStatus();
                                                                       foreach($array_status as $key=>$val){                                               
                                                                           $selected ="";
                                                                               $selected = @$expenses->expenses_visible == $key ? "selected" : "";
                                                                            
                                                                               echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';   
                                                                       }      
                                                                      
                                                              ?>  
                                                </select>  
                                      </div>
                                    </div>
                                         <div class="form-group">     
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ไฟล์</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">      
                                             <input type="file" name="expenses_file" id="expenses_file" accept="image/*,application/pdf">
                                              <?php if(@$expense->expenses_path !="") { echo "<a href='".base_url(). $expense->expenses_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?>
                                          </div>
                                        </div>       
                   </div>
                    <div class="modal-footer">
                    <button type="button"  class="btn btn-primary" id="save_expenses">บันทึก</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button> 
                  </div>
                   </form>                            
                </div>    
                
              </div>
            </div>
            
            <script type="text/javascript">
                   $('#save_expenses').click(function(){
                            var file2 = new FormData();
                            var countfile = 0;
                                jQuery.each(jQuery('#expenses_file')[0].files, function(i, file) {
                                    file2.append(i, file);
                                    countfile=1;
                                });
                          $.ajax({                            
                            url:"<?php echo base_url()."service/save_session/expenses"?>",
                            data: $('#add_expenses').serialize()+ '&service_code=' + $('#service_code').val(),
                            method: "POST",
                            success:function(result){ 
                               $('.add_expenses').modal('toggle');        
                                 var prows =  'erows_'+result ;    
                                 var expenses_name =$('#expenses_name').val();
                                 var expenses_date =$('#expenses_date').val();
                                 var expenses_amount =$('#expenses_amount').val();
                                 var expenses_visible = $('#expenses_visible option:selected').text() ;
                                 
                                 if(countfile!=0){
                                  $.ajax({
                                        url:"<?php echo base_url()."service/save_session/expenses/"?>"+result,
                                        data: file2,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        type: 'POST',
                                        success: function(data){
                                             var q_data = jQuery.parseJSON(data);     
                                             if(q_data.code==200){
     
                                                 var img = '<a href="'+q_data.path+'"  target="_blank"><i class="fa fa-file"></i></a>';
                                                     $('#tb_expenses tr:last').after('<tr id="'+prows+'"><td>'+expenses_name+'</td><td>'+expenses_date+'</td><td>'+expenses_amount+'</td><td>'+expenses_visible+'</td><td>'+img+'</td><td><a  href="javascript:delete_expenses(\''+prows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                     '<input type="hidden" name="expenses_session[]" value="'+result+'">'+'</td></tr>');       
                                                 
                                             }else{
                                                 $('#tb_expenses tr:last').after('<tr id="'+prows+'"><td>'+expenses_name+'</td><td>'+expenses_date+'</td><td>'+expenses_amount+'</td><td>'+expenses_visible+'</td><td></td><td><a  href="javascript:delete_expenses(\''+prows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                 '<input type="hidden" name="expenses_session[]" value="'+result+'">'+'</td></tr>');
                                             }
                                        }
                                    });       
                                }
                               else   $('#tb_expenses tr:last').after('<tr id="'+prows+'"><td>'+expenses_name+'</td><td>'+expenses_date+'</td><td>'+expenses_amount+'</td><td>'+expenses_visible+'</td><td></td><td><a  href="javascript:delete_expenses(\''+prows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                               '<input type="hidden" name="expenses_session[]" value="'+result+'">'+'</td></tr>');
                                    
                                     $('#expenses_name').val('');
                                     $('#expenses_date').val('');
                                     $('#expenses_amount').val(''); 
                                       $('#expenses_file').val('');
                                    
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');  
                               
                            }
                        });     
                        
              }) ;
                function delete_expenses(div_id,id){         

                        $.ajax({                            
                            url:"<?php echo base_url()."service/delete_session/expensen/"?>"+(id-1),        
                            method: "POST",   
                            success:function(result){ 
                                $('#'+div_id).remove();    
                            }
                        }); 
               }
                     
                     </script>
            <!-- ---------------------------- Popup expenses ---------------------------->
            
             
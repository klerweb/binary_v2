<?php
   $this->load->model('service_mod', 'service');  
   $this->load->model('servicetype_mod', 'servicetype');  
   $this->load->model('customer/customer_mod', 'customer');   
 
?>
<div class="row"></div>
<div class="popup_alert "><?php echo @$popup_alert?></div>  
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
            <form action="" method="post">
                <button type="button" class="btn btn-dark btn-sm" onclick="window.location.href='<?php echo base_url(); ?>service/description';"><i class="fa fa-plus"></i> เพิ่ม</button>
                <!--<button type="submit" class="btn btn-warning btn-sm" onclick="window.location.href='<?php //echo base_url(); ?>service/description';"><i class="fa fa-ban"></i> ยกเลิก</button>-->
                <!--<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i> ลบ</button>   -->
                <hr>
               
                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>   
                   
                             <th >รหัส</th>
                              <th>วันที่</th>  
                              <th>ลูกค้า</th>
                              <th>ประเภทบริการ</th>
                              <th>สถานะ</th>
                              <th width="15%">จัดการ</th>
                            </tr>
                          </thead>
                          <tbody>                                             
 
                            <?php
                          if(isset($service)){
                              foreach($service as $key=>$val){
                                  if(in_array($val->service_visible,array(3,5,6)))
                                  {
                                  ?>
                                  
                               <tr>  
                              <td><?php echo $val->service_code ?></td>
                              <td><?php echo  getShowDateFormat($val->service_date) ?></td>  
                              <td><?php if($val->customer_id !=0) echo $this->customer->get_customer($val->customer_id,'customer_company') ?></td>
                              <td><?php if($val->type_id !=0) echo $this->servicetype->get_servicetype($val->type_id,'type_name')?></td>
                              <td><?php  echo $this->service->get_serviceStatus($val->service_visible) ?></td>
                              <td>   
                                <?php
                                     if($val->service_visible ==5 || $val->service_visible == 6){
                                         ?>
                                         <a  href="<?php echo base_url()."service/description/".$val->service_id."?view=1"?>"  class="btn btn-success btn-xs" ><i class="fa fa-search"></i> ดูข้อมูล </a> 
                                         <?php
                                     }
                                ?>
                                <?php if($val->service_visible == 3){?><a href="<?php echo base_url()."service/description/".$val->service_id?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข </a>  <?php } ?>
                                <?php 
                                   $sumcount =  $this->service->isDelete($val->service_id) ;
                                   if(isset($sumcount->sumCounts) && $sumcount->sumCounts !=0 && $val->service_visible ==3){
                                      ?>
                                      <a  href="<?php echo base_url()."service/description_cancel/".$val->service_id?>"  onclick="return confirm('คุณต้องการยกเลิกข้อมูล ?')" class="btn btn-warning btn-xs" ><i class="fa fa-ban"></i> ยกเลิก </a> 
                                      <?php
                                   } else if($val->service_visible != 5 && $val->service_visible !=6){
                                       ?>
                                       <a  href="<?php echo base_url()."service/description_delete/".$val->service_id?>"  onclick="return confirm('คุณต้องการลบข้อมูล ?')" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"></i> ลบ </a>
                                       <?php
                                   }
                                ?>
                                
                              </td>
                            </tr>
                                  <?php
                                  }
                              }
                          }
                          ?>
                          </tbody>
                        </table>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                
                <script type="text/javascript">
            $('#datatable-responsive').DataTable({
                responsive: true,
                aoColumnDefs: [ { bSortable: false, aTargets: [ 0 ] } ],
                order: [[ 1, "desc" ]]
                       } );
        </script>
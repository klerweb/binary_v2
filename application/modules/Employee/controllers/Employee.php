<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends MX_Controller 
{
	public $title = 'พนักงาน';
	public $menu = array('menu' => 'user', 'sub' => 'employee');
	
	function __construct()
	{
		parent::__construct();
		
		if(empty($_SESSION['binary_login']))
		{
			header('Location: '.base_url());
			exit();
		}
		
        $this->load->model('employee/employee_mod', 'employee'); 
        $this->load->model('employee/employee_module_mod', 'employee_module');
        $this->load->model('title/title_mod', 'titles');
        $this->load->model('department/department_mod', 'department');
        $this->load->model('role/role_mod', 'role');
	}
	
	public function index()
	{
		$isSave = $this->input->get('isStatus');
		$popup_alert = '';
		if($isSave!='')
		{
			switch ($isSave)
			{
				case 'success_add': $popup_alert = popup_alert('success', 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_add': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้');
					break;
				case 'error_dup': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้เนื่องจากชื่อผู้ใช้งานซ้ำ');
					break;
				case 'success_edit': $popup_alert = popup_alert('success', 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_edit': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_pass': $popup_alert = popup_alert('success', 'เปลี่ยนรหัสเรียบร้อยแล้วค่ะ');
					break;
				case 'error_pass': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_del': $popup_alert = popup_alert('success', 'ลบข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_del': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้');
					break;
			}
		}
		
		$data_content = array(
				'popup_alert' => $popup_alert,
				'employee' => $this->employee->all()
			);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_employee/employee.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('index', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{	
		if($id=='')
		{
			$data_content = $this->employee->model();
			$data_content['module'] = array();
			
			$title_sub = '';
		}
		else
		{
			$data_content = (array)$this->employee->get_employee($id);
			
			$title_sub = ' :: '.$data_content['employee_username'];
			
			$list_module = (array)$this->employee_module->get_by_employee($id);
				
			$data_content['module'] = array();
			if(!empty($list_module))
			{
				foreach ($list_module as $module)
				{
					$module = (array)$module;
					$data_content['module'][$module['module_id']] = $module['module_id'];
				}
			}
		}
		
		$data_content['title'] = $this->titles->get_title();
		$data_content['department'] = $this->department->get_department();
		$data_content['role'] = $this->role->get_role();
		
		$data = array(
				'title' => $this->title,
				'title_sub' => $title_sub,
				'js_other' => array('modules_employee/form.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}

	public function save()
	{
		$data = $this->input->post();
		$data['employee_startdate'] = setDateToDB($data['employee_startdate']);
		
		if($data['employee_id']=='')
		{
			$check_username = $this->employee->get_user($data['employee_username']);
			
			if(!empty($check_username))
			{
				$isSave = 'error_dup';
			}
			else
			{
				$data['employee_password'] = md5($data['employee_password']);
					
				$data['employee_id'] = $this->employee->save($data);
				$isSave = is_numeric($data['employee_id'])? 'success_add' : 'error_add';
			}
		}
		else
		{
			$isSave = $this->employee->save($data, $data['employee_id'])? 'success_edit' : 'error_edit';
			
			if($isSave=='success_edit') $this->employee_module->del_employee($data['employee_id']);
		}
		
		if(!empty($data['module_id']) && ($isSave=='success_add' || $isSave=='success_edit'))
		{
			foreach ($data['module_id'] as $module_id)
			{
				$data_employee_module['employee_id'] = $data['employee_id'];
				$data_employee_module['module_id'] = $module_id;
				$this->employee_module->save($data_employee_module);
			}
		}
		
		redirect(base_url().'employee?isStatus='.$isSave);
	}
	
	public function pass($id)
	{
		$data_content['employee_id'] = $id;
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_employee/pass.js'),
				'content' => $this->parser->parse('pass', $data_content, TRUE),
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save_pass()
	{
		$data = $this->input->post();
		
		$data['employee_password'] = md5($data['employee_password']);
		$isSave = $this->employee->save($data, $data['employee_id'])? 'success_pass' : 'error_pass';
	
		redirect(base_url().'employee?isStatus='.$isSave);
	}
	
	public function del($id)
	{
		$data['employee_id'] = $id;
		$data['employee_visible'] = '0';
		
		$isSave = $this->employee->save($data, $data['employee_id'])? 'success_del' : 'error_del';
		
		redirect(base_url().'employee?isStatus='.$isSave);
	}
	
	public function del_all()
	{
		$isSave = 'error_del';
		
		if(!$this->input->post('chk'))
		{
			foreach ($this->input->post('chk') as $value)
			{
				$data['employee_id'] = $value;
				$data['employee_visible'] = '0';	
				
				$this->employee->save($data, $data['employee_id']);
			}
			
			$isSave = 'success_del';
		}
		
		redirect(base_url().'employee?isStatus='.$isSave);
	}
	
     public function save_popup(){
        $data = $this->input->post();
 
           $result_id = $this->employee->save($data);   
           if($result_id) echo json_encode(array('code'=>"200","description"=>"success","id"=>$result_id));
           else echo json_encode(array('code'=>"100","description"=>"error"));    
    }
}

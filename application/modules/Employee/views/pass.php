<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="frmPass" name="frmPass" method="post" enctype="multipart/form-data" action="<?php echo base_url().'employee/save_pass'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="employee_id" name="employee_id" value="<?php echo $employee_id; ?>" />
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">รหัสผ่าน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="password" id="employee_password" name="employee_password" class="form-control" value="" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ยืนยันรหัสผ่าน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="password" id="employee_password_confirm" name="employee_password_confirm" class="form-control" value="" required />
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>employee';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="frmEmployee" name="frmEmployee" method="post" enctype="multipart/form-data" action="<?php echo base_url().'employee/save'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="employee_id" name="employee_id" value="<?php echo $employee_id; ?>" />
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">คำนำหน้า <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="title_id" name="title_id" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($title))
										{
											foreach ($title as $list_title) 
											{
												$selected = $list_title->title_id==$title_id? ' selected' : '';
												echo '<option value="'.$list_title->title_id.'"'.$selected.'>'.$list_title->title_name.'</option>';
											}
										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="employee_fname" name="employee_fname" class="form-control" value="<?php echo $employee_fname; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">นามสกุล <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="employee_lname" name="employee_lname" class="form-control" value="<?php echo $employee_lname; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่เริ่มงาน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="employee_startdate" name="employee_startdate" class="form-control" value="<?php echo getShowDateFormat($employee_startdate); ?>" required readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">แผนก <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="department_id" name="department_id" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($department))
										{
											foreach ($department as $list_department) 
											{
												$selected = $list_department->department_id==$department_id? ' selected' : '';
												echo '<option value="'.$list_department->department_id.'"'.$selected.'>'.$list_department->department_name.'</option>';
											}
										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ตำแหน่ง <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="employee_position" name="employee_position" class="form-control" value="<?php echo $employee_position; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">เบอร์โทรศัพท์</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="employee_tel" name="employee_tel" class="form-control" value="<?php echo $employee_tel; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">อีเมล์</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="email" id="employee_email" name="employee_email" class="form-control" value="<?php echo $employee_email; ?>" />
						</div>
					</div>
					<?php if($employee_id=='') { ?>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อผู้ใช้งาน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="employee_username" name="employee_username" class="form-control" value="<?php echo $employee_username; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">รหัสผ่าน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="password" id="employee_password" name="employee_password" class="form-control" value="" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ยืนยันรหัสผ่าน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="password" id="employee_password_confirm" name="employee_password_confirm" class="form-control" data-parsley-equalto="#employee_password" value="" required />
						</div>
					</div>
					<?php } ?>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">สิทธิ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="role_id" name="role_id" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($role))
										{
											foreach ($role as $list_role) 
											{
												$selected = $list_role->role_id==$role_id? ' selected' : '';
												echo '<option value="'.$list_role->role_id.'"'.$selected.'>'.$list_role->role_name.'</option>';
											}
										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ฟังก์ชั่น</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>รายการ</th>
										<th>แสดง</th>
										<th>เพิ่ม</th>
										<th>แก้ไข</th>
										<th>ลบ</th>
									</tr>
								</thead>
								<tbody>
									<tr>
				                        <td>สิทธิ</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="1" class="flat" <?php echo in_array('1', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="2" class="flat" <?php echo in_array('2', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="3" class="flat" <?php echo in_array('3', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="4" class="flat" <?php echo in_array('4', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>แผนก</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="5" class="flat" <?php echo in_array('5', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="6" class="flat" <?php echo in_array('6', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="7" class="flat" <?php echo in_array('7', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="8" class="flat" <?php echo in_array('8', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>พนักงาน</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="9" class="flat" <?php echo in_array('9', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="10" class="flat" <?php echo in_array('10', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="11" class="flat" <?php echo in_array('11', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="12" class="flat" <?php echo in_array('12', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>ลูกค้า</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="13" class="flat" <?php echo in_array('13', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="14" class="flat" <?php echo in_array('14', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="15" class="flat" <?php echo in_array('15', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="16" class="flat" <?php echo in_array('16', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>ตัวแทนจำหน่าย</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="17" class="flat" <?php echo in_array('17', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="18" class="flat" <?php echo in_array('18', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="19" class="flat" <?php echo in_array('19', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="20" class="flat" <?php echo in_array('20', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>ผู้รับเหมา</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="21" class="flat" <?php echo in_array('21', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="22" class="flat" <?php echo in_array('22', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="23" class="flat" <?php echo in_array('23', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="24" class="flat" <?php echo in_array('24', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>หน่วยนับ</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="25" class="flat" <?php echo in_array('25', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="26" class="flat" <?php echo in_array('26', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="27" class="flat" <?php echo in_array('27', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="28" class="flat" <?php echo in_array('28', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>หมวดหมู่</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="29" class="flat" <?php echo in_array('29', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="30" class="flat" <?php echo in_array('30', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="31" class="flat" <?php echo in_array('31', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="32" class="flat" <?php echo in_array('32', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      				<tr>
				                        <td>สินค้า</td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="33" class="flat" <?php echo in_array('33', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="34" class="flat" <?php echo in_array('34', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="35" class="flat" <?php echo in_array('35', $module)? 'checked ' : ''; ?>/></td>
				                        <td align="center"><input type="checkbox"  id="module_id[]" name="module_id[]" value="36" class="flat" <?php echo in_array('36', $module)? 'checked ' : ''; ?>/></td>
                      				</tr>
                      			</tbody>
                      		</table>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>employee';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
class Employee_mod extends MY_Model
{        
       private $table = 'employee';
       private $key = 'employee_id';
       private $db_filed = array(
       		'employee_id' => 'employee_id',
       		'title_id' => 'title_id',
       		'employee_fname' => 'employee_fname',
       		'employee_lname' => 'employee_lname',
       		'employee_startdate' => 'employee_startdate',
       		'department_id' => 'department_id',
       		'employee_position' => 'employee_position',
       		'employee_tel' => 'employee_tel',
       		'employee_email' => 'employee_email',
       		'role_id' => 'role_id',
       		'employee_username' => 'employee_username',
       		'employee_password' => 'employee_password',
       		'employee_visible' => 'employee_visible',
       		'employee_createby' => 'employee_createby',
       		'employee_createdate' => 'employee_createdate',
       		'employee_updateby' => 'employee_updateby',
       		'employee_updatedate' => 'employee_updatedate'
       	);

	function get_dbfiled()
	{
		return  $this->db_filed;
	}
	
	function get_employee($employee_id=NULL, $filed=NULL)
	{
		$cfg->where['employee_visible'] = '1';
		
		if($employee_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $employee_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
					return  $data ;
				}
            }
            else 
            {
            	return $this->get_data($this->table, '', $cfg);
            }
        }
        
    public function all()
    {
        	$this->db->select("*");
        	$this->db->from($this->table);
        	$this->db->join("title", "title.title_id = employee.title_id", "LEFT");
        	$this->db->join("department", "department.department_id = employee.department_id", "LEFT");
        	$this->db->join("role", "role.role_id = employee.role_id", "LEFT");
        	$this->db->where("employee.employee_visible =", "1");
        
        	$this->db->order_by("employee.employee_id", "desc");
        
        	$query = $this->db->get();
        
        	return $query->num_rows()!=0? $query->result_array() : array();
        }
        
	function save($data=NULL, $employee_id=NULL)
	{
		$employee = (object) array();
		if(!$employee_id)
		{
			if(@$data['employee_id']) $employee->employee_id = $data['employee_id']  ;
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $employee->${'value'} = $data[$value] ;
			}
	
			$employee->{$this->db_filed['employee_visible']} = '1';
			$employee->{$this->db_filed['employee_createdate']} = date(DATETIME_FORMAT_2DB);
			$employee->{$this->db_filed['employee_createby']} = 1;
			$employee->{$this->db_filed['employee_updatedate']} = date(DATETIME_FORMAT_2DB);
			$employee->{$this->db_filed['employee_updateby']} = 1;
			 
			$this->db->set($employee)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $employee->${'value'} = $data[$value] ;
			}
			
			$employee->{$this->db_filed['employee_updatedate']} = date(DATETIME_FORMAT_2DB);
			$employee->{$this->db_filed['employee_updateby']} = 1;
			
			return $this->db->where($this->key, $data['employee_id'])
				->set($employee)
				->update($this->table);
		}
	} 
        
     function model()
     {
        	$data['employee_id'] = '';
        	$data['title_id'] = '';
        	$data['employee_fname'] = '';
        	$data['employee_lname'] = '';
        	$data['employee_startdate'] = date('d-m-Y');
        	$data['department_id'] = '';
        	$data['employee_position'] = '';
        	$data['employee_tel'] = '';
        	$data['employee_email'] = '';
        	$data['role_id'] = '';
        	$data['employee_username'] = '';
        	$data['employee_visible'] = '1';
        
        	return $data;
        }   
 }
?>

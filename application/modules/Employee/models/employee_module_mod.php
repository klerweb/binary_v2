<?php
class Employee_module_mod extends MY_Model
{
	private $table = "employee_module";
	private $key = "employee_module_id";
	private $db_filed = array(
			"employee_module_id" => "employee_module_id",
			"employee_id" => "employee_id",
			"module_id" => "module_id",
			"employee_module_createby" => "employee_module_createby",
			"employee_module_createdate" => "employee_module_createdate"
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_employee_module($employee_module_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		if($employee_module_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $employee_module_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function get_by_employee($employee_id)
	{
		$cfg->where['employee_id'] = $employee_id;
		return  $this->get_data($this->table, '', $cfg) ;
	}
	
	function save($data=NULL, $employee_id=NULL)
	{
		$employee = (object) array();
		if(!$employee_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $employee->${'value'} = $data[$value] ;
			}
			
			unset($employee->{$this->db_filed['employee_module_id']});

			$employee->{$this->db_filed['employee_module_createdate']} = date(DATETIME_FORMAT_2DB);
			$employee->{$this->db_filed['employee_module_createby']} = 1;
			 
			$this->db->set($employee)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $employee->${'value'} = $data[$value] ;
			}
			
			return $this->db->where($this->key, $data['employee_module_id'])
				->set($employee)
				->update($this->table);
		}
	}
	
	function del_employee($employee_id)
	{
		return $this->delete($this->table, $this->db_filed['employee_id'], $employee_id);
	}
	
	function model()
	{
		$data['employee_module_id'] = '';
		$data['employee_id'] = '';
		$data['module_id'] = '';
		
		return $data;
	}
}
?>
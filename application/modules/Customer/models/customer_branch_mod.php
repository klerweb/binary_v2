<?php
class Customer_branch_mod extends MY_Model
{
	private $table = 'customer_branch';
	private $key = 'customer_branch_id';
	private $db_filed = array(
			"customer_branch_id" => "customer_branch_id",
			"customer_id" => "customer_id",
			"customer_branch_name" => "customer_branch_name",
			"customer_branch_contact" => "customer_branch_contact",
			"customer_branch_tel" => "customer_branch_tel",
			"customer_branch_email" => "customer_branch_email",
			"customer_branch_address" => "customer_branch_address",
			"customer_branch_latitude" => "customer_branch_latitude",
			"customer_branch_longitude" => "customer_branch_longitude",
            "customer_branch_visible" => "customer_branch_visible",
            "customer_branch_createby" => "customer_branch_createby",
            "customer_branch_createdate" => "customer_branch_createdate",
            "customer_branch_updateby" => "customer_branch_updateby",
            "customer_branch_updatedate" => "customer_branch_updatedate"
		);
                     
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_customer_branch($customer_branch_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['customer_branch_visible'] = '1';
		
		if($customer_branch_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $customer_branch_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	public function get_branchByCustomer($customer_id='')
	{
		$cfg->where['customer_id'] = $customer_id;
		$cfg->where['customer_branch_visible'] = '1';
	
		return $this->get_data($this->table, $this->key, $cfg);      
	} 
	
	function save($data=NULL, $customer_branch_id=NULL)
	{
		$customer_branch = (object) array();
		if(!$customer_branch_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $customer_branch->${'value'} = $data[$value] ;
			}
			
			unset($customer_branch->{$this->db_filed['customer_branch_id']});
	
			$customer_branch->{$this->db_filed['customer_branch_visible']} = '1';
			$customer_branch->{$this->db_filed['customer_branch_createdate']} = date(DATETIME_FORMAT_2DB);
			$customer_branch->{$this->db_filed['customer_branch_createby']} = 1;
			$customer_branch->{$this->db_filed['customer_branch_updatedate']} = date(DATETIME_FORMAT_2DB);
			$customer_branch->{$this->db_filed['customer_branch_updateby']} = 1;
			 
			$this->db->set($customer_branch)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $customer_branch->${'value'} = $data[$value] ;
			}
			
			$customer_branch->{$this->db_filed['customer_branch_updatedate']} = date(DATETIME_FORMAT_2DB);
			$customer_branch->{$this->db_filed['customer_branch_updateby']} = 1;
			
			return $this->db->where($this->key, $data['customer_branch_id'])
				->set($customer_branch)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['customer_branch_id'] = '';
		$data['customer_id'] = '';
		$data['customer_branch_name'] = '';
		$data['customer_branch_contact'] = '';
		$data['customer_branch_tel'] = '';
		$data['customer_branch_email'] = '';
		$data['customer_branch_address'] = '';
		$data['customer_branch_latitude'] = '';
		$data['customer_branch_longitude'] = '';
		$data['customer_branch_visible'] = '1';
		
		return $data;
	}
 }
?>
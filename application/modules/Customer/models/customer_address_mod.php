<?php
class Customer_address_mod extends MY_Model
{
	private $table='customer_address';
        private $key='customer_address_id';
        private $db_filed=array(
            "customer_address_id"=>"customer_address_id",
            "customer_id"=>"customer_id",
            "customer_address_name"=>"customer_address_name",
            "customer_address_default"=>"customer_address_default",
            "customer_address_desc"=>"customer_address_desc",
            "district_id"=>"district_id",
            "amphur_id"=>"amphur_id",
            "province_id"=>"province_id",
            "zipcode_id"=>"zipcode_id",
            "customer_address_visible"=>"customer_address_visible",
            "customer_address_createby"=>"customer_address_createby",
            "customer_address_createdate"=>"customer_address_createdate",
            "customer_address_updateby"=>"customer_address_updateby",
            "customer_address_updatedate"=>"customer_address_updatedate"
            );          
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
       function get_customer_address($cus_address_id=NULL,$filed=NULL){      
           
            if($cus_address_id){ 
                $data =  $this->get_data($this->table,$this->key,'',$cus_address_id);   
                if($filed){ return $data->{$this->db_filed[$filed]};  }
                return  $data ; 
            }
            
            else return $this->get_data($this->table) ;
        } 
        
        function get_customer_addressByCustomer($customer_id=NULL,$filed=NULL){
               $cfg = "";
               if(empty($customer_id)) return false ;
                $cfg->where[$this->db_filed['customer_id']] = $customer_id;
                $data  = $this->get_data($this->table,'',$cfg) ;
                return $data ;   
        } 
        
        public function get_province($province_id=NULL,$filed=''){
             
            $provinceTB = 'province';
            if($province_id){ 
                $data =  $this->get_data($provinceTB,'province_id','',$province_id);   
                if($filed){ return $data->{$filed};  }
                return  $data ; 
            }                         
            
            else return $this->get_data($provinceTB) ;
        }
        
        public function get_amphur($amphur_id=NULL,$filed=''){
            $amphurTB = 'amphur';
            if($amphur_id){ 
                $data =  $this->get_data($amphurTB,'amphur_id','',$amphur_id);   
                if($filed){ return $data->{$filed};  }
                return  $data ; 
            }  
        }
        
        public function get_amphurByProvince($province_id=NULL,$filed=''){
            $amphurTB = 'amphur';
         
               $cfg = "";
               if(empty($province_id)) return false ;
                $cfg->where['province_id'] = $province_id;
                $data  = $this->get_data($amphurTB,'',$cfg) ;
                return $data ;     
        }
        
        public function get_district($district_id=NULL,$filed=''){
            $districtTB = 'district';
            if($district_id){ 
                $data =  $this->get_data($districtTB,'district_id','',$district_id);   
                if($filed){ return $data->{$filed};  }
                return  $data ; 
            }  
        }
        
        public function get_districtByAmphur($amphur_id=NULL,$filed=''){
            $districtTB = 'district';  
         
               $cfg = "";
               if(empty($province_id)) return false ;
                $cfg->where['amphur_id'] = $amphur_id;
                $data  = $this->get_data($districtTB,'',$cfg) ;
                return $data ;     
        }
        
        public function get_zipcode($zipcode_id=NULL,$filed=''){
           $zipcodeTB = 'zipcode';
            if($zipcode_id){ 
                $data =  $this->get_data($zipcodeTB,'zipcode_id','',$zipcode_id);   
                if($filed){ return $data->{$filed};  }
                return  $data ; 
            }else return $this->get_data($zipcodeTB) ;   
                                                             
        }
        
        public function get_zipcodeByRefer($province_id=NULL,$amphur_id=NULL,$district_id=NULL,$filed=''){
            $zipcodeTB = 'zipcode';  
         
               $cfg = "";
               if(empty($province_id)) return false ;
                $cfg->where['province_id'] = $province_id;
                $cfg->where['amphur_id'] = $amphur_id;
                $cfg->where['district_id'] = $district_id;
                $data  = $this->get_data($zipcodeTB,'',$cfg) ;  
                return  $data ;  
            }  
            
        public function get_zipcodeByzipcode($zipcode=NULL,$filed=''){
           $zipcodeTB = 'zipcode';
             $cfg = "";
               if(empty($zipcode)) return false ;
                $cfg->where['zipcode'] = $zipcode;     
                $data  = $this->get_data($zipcodeTB,'',$cfg) ;
                return $data ;                                                     
        }
            
        public function get_zipcodeGroup(){
           $zipcodeTB = 'zipcode';  
             
               return $this->db->select('*')
               ->group_by('zipcode') 
               ->order_by('zipcode', 'asc')
               ->get($zipcodeTB)->result();                                                               
        }
        
       function save($data=NULL,$customer_address_id=NULL){
       $customer_address = (object) array();   
       
       if(!$customer_address_id){
                            
             if(@$data['customer_address_id']) $customer_address->customer_address_id = $data['customer_address_id']  ;
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $customer_address->${'value'} = $data[$value] ;
             } 
              
             $customer_address->{$this->db_filed['customer_address_visible']} = ''.STATUS_ACTIVE.'';
             $customer_address->{$this->db_filed['customer_address_createdate']} = date(DATETIME_FORMAT_2DB);
             $customer_address->{$this->db_filed['customer_address_createby']} = 1;     
         
              $this->db->set($customer_address)->insert($this->table);
 
            return $this->db->insert_id(); 
            
       }
       else{
           
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $customer_address->${'value'} = $data[$value] ;
             }   
            $customer_address->{$this->db_filed['customer_address_updatedate']} = date(DATETIME_FORMAT_2DB);
            $customer_address->{$this->db_filed['customer_address_updateby']} =  1;
               
             return $this->db->where($this->key, $data['customer_address_id'])
            ->set($customer_address)
            ->update($this->table);     
       }                      
       
    } 
         
 }
?>

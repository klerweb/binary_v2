<?php
class Customer_mod extends MY_Model
{
	private $table = 'customer';
	private $key = 'customer_id';
	private $db_filed = array(
			"customer_id" => "customer_id",
			"customer_company" => "customer_company",
			"customer_fname" => "customer_fname",
			"customer_lname" => "customer_lname",
            "customer_visible" => "customer_visible",
            "customer_createby" => "customer_createby",
            "customer_createdate" => "customer_createdate",
            "customer_updateby" => "customer_updateby",
            "customer_updatedate" => "customer_updatedate"
		);
                     
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_customer($customer_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['customer_visible'] = '1';
		
		if($customer_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $customer_id);
			
			if($filed && $data)
			{     
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function save($data=NULL, $customer_id=NULL)
	{
		$customer = (object) array();
		if(!$customer_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $customer->${'value'} = $data[$value] ;
			}
			
			unset($customer->{$this->db_filed['customer_id']});
	
			$customer->{$this->db_filed['customer_visible']} = '1';
			$customer->{$this->db_filed['customer_createdate']} = date(DATETIME_FORMAT_2DB);
			$customer->{$this->db_filed['customer_createby']} = 1;
			$customer->{$this->db_filed['customer_updatedate']} = date(DATETIME_FORMAT_2DB);
			$customer->{$this->db_filed['customer_updateby']} = 1;
			 
			$this->db->set($customer)->insert($this->table);
			
			$id = $this->db->insert_id();
			
			if(!empty($_SESSION['customer_branch']))
			{
				$this->load->model('customer/customer_branch_mod', 'customer_branch');
				
				foreach ($_SESSION['customer_branch'] as $branch)
				{
					unset($branch['customer_branch_id']);
					
					$branch['customer_id'] = $id;
					$this->customer_branch->save($branch);
				}
			}
			
			return $id;
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $customer->${'value'} = $data[$value] ;
			}
			
			$customer->{$this->db_filed['customer_updatedate']} = date(DATETIME_FORMAT_2DB);
			$customer->{$this->db_filed['customer_updateby']} = 1;
			
			return $this->db->where($this->key, $data['customer_id'])
				->set($customer)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['customer_id'] = '';
		$data['customer_company'] = '';
		$data['customer_visible'] = '1';
		
		return $data;
	}
 }
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MX_Controller 
{
	public $title = 'ลูกค้า';
	public $menu = array('menu' => 'contact', 'sub' => 'customer');
	
	function __construct()
	{
		parent::__construct();
		
		if(empty($_SESSION['binary_login']))
		{
			header('Location: '.base_url());
			exit();
		}
		
        $this->load->model('customer/customer_mod', 'customer'); 
        $this->load->model('customer/customer_branch_mod', 'customer_branch');
	}
	
	public function index()
	{
		$isSave = $this->input->get('isStatus');
		$popup_alert = '';
		if($isSave!='')
		{
			switch ($isSave)
			{
				case 'success_add': $popup_alert = popup_alert('success', 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_add': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้');
					break;
				case 'success_edit': $popup_alert = popup_alert('success', 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_edit': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_del': $popup_alert = popup_alert('success', 'ลบข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_del': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้');
					break;
			}
		}
		
		$data_content = array(
				'popup_alert' => $popup_alert,
				'customer' => $this->customer->get_customer()
			);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_customer/customer.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('index', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function branch($id='')
	{
		if($id=='')
		{
			$data_content['customer_branch'] = $_SESSION['customer_branch'];
		}
		else
		{
			$data_content['customer_branch'] = $this->customer_branch->get_branchByCustomer($id);
		}
		
		echo $this->parser->parse('branch', $data_content, TRUE);
	}
	
	public function form($id='')
	{
		if($id=='')
		{
			$data_content = $this->customer->model();
			
			unset($_SESSION['customer_branch']);
			
			$_SESSION['customer_branch'] = array();
			$data_content['customer_branch'] = $_SESSION['customer_branch'];
		}
		else
		{
			$data_content = (array)$this->customer->get_customer($id);
			$data_content['customer_branch'] = $this->customer_branch->get_branchByCustomer($id);
		}
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_customer/form.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}

	public function save()
	{
		$data = $this->input->post();
		
		if($data['customer_id']=='')
		{
			$data['customer_id'] = $this->customer->save($data);
			$isSave = is_numeric($data['customer_id'])? 'success_add' : 'error_add';
		}
		else
		{
			$isSave = $this->customer->save($data, $data['customer_id'])? 'success_edit' : 'error_edit';
		}
		
		redirect(base_url().'customer?isStatus='.$isSave);
	}
	
	public function del($id)
	{
		$data['customer_id'] = $id;
		$data['customer_visible'] = '0';
		
		$isSave = $this->customer->save($data, $data['customer_id'])? 'success_del' : 'error_del';
		
		redirect(base_url().'customer?isStatus='.$isSave);
	}
	
	public function del_all()
	{
		$isSave = 'error_del';
	
		if(!$this->input->post('chk'))
		{
			foreach ($this->input->post('chk') as $value)
			{
				$data['customer_id'] = $value;
				$data['customer_visible'] = '0';
	
				$this->customer->save($data, $data['customer_id']);
			}
	
			$isSave = 'success_del';
		}
	
		redirect(base_url().'customer?isStatus='.$isSave);
	}
	
	public function save_branch()
	{
		$data = $this->input->post();
		
		if($data['customer_id']=='')
		{
			if($data['customer_branch_id']=='')
			{
				$_SESSION['customer_branch'][] = $data;
				
				echo 'success_add';
			}
			else
			{
				$_SESSION['customer_branch'][$data['customer_branch_id']] = $data;
				
				echo 'success_edit';
			}
		}
		else
		{
			if($data['customer_branch_id']=='')
			{
				$data['customer_branch_id'] = $this->customer_branch->save($data);
				echo is_numeric($data['customer_branch_id'])? 'success_add' : 'error_add';
			}
			else
			{
				echo $this->customer_branch->save($data, $data['customer_branch_id'])? 'success_edit' : 'error_edit';
			}	
		}
	}
	
	public function del_branch()
	{
		$data = $this->input->post();
		
		if($data['type']=='add')
		{
			unset($_SESSION['customer_branch'][$data['id']]);
			
			echo 'success_del';
		}
		else
		{
			$data['customer_branch_id'] = $data['id'];
			$data['customer_branch_visible'] = '0';
			
			echo $this->customer_branch->save($data, $data['customer_branch_id'])? 'success_del' : 'error_del';
		}
	}
    
    public function save_popup(){
        $data = $this->input->post();
 
           $result_id = $this->customer->save($data);   
           if($result_id) echo json_encode(array('code'=>"200","description"=>"success","id"=>$result_id));
           else echo json_encode(array('code'=>"100","description"=>"error"));    
    }
}

<table id="tblBranch" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ลำดับ</th>
			<th>สาขา</th>
			<th>จัดการ</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			if(!empty($customer_branch))
			{
				$i_branch = 1;
				foreach ($customer_branch as $key => $branch)
				{
					$branch = (array)$branch;
					
					if($branch['customer_id']!='')
					{
						$key = $branch['customer_branch_id'];
						$type = 'edit';
					}
					else
					{
						$branch['customer_branch_id'] = $key;
						$type = 'add';
					}
		?>
		<tr>
			<td><?php echo $i_branch++; ?></td>
			<td>
				<b><?php echo $branch['customer_branch_name']; ?></b><br/>
				ผู้ติดต่อ: <?php echo $branch['customer_branch_contact']; ?><br/>
				เบอร์โทรศัพท์: <?php echo $branch['customer_branch_tel']; ?><br/>
				อีเมล์: <?php echo $branch['customer_branch_email']; ?><br/>
				ที่อยู่: <?php echo $branch['customer_branch_address']; ?><br/>
				พิกัด: <?php echo $branch['customer_branch_latitude'].' , '.$branch['customer_branch_longitude']; ?> <?php if(!empty($branch['customer_branch_latitude']) && !empty($branch['customer_branch_longitude'])) echo '<a href="http://www.google.com/maps/place/'.$branch['customer_branch_latitude'].','.$branch['customer_branch_longitude'].',8z" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a>'; ?>
				<input type="hidden" id="branch_id_<?php echo $key; ?>" name="branch_id_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_id']; ?>" />
				<input type="hidden" id="branch_name_<?php echo $key; ?>" name="branch_name_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_name']; ?>" />
				<input type="hidden" id="branch_tel_<?php echo $key; ?>" name="branch_tel_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_tel']; ?>" />
				<input type="hidden" id="branch_email_<?php echo $key; ?>" name="branch_email_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_email']; ?>" />
				<input type="hidden" id="branch_address_<?php echo $key; ?>" name="branch_address_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_address']; ?>" />
				<input type="hidden" id="branch_latitude_<?php echo $key; ?>" name="branch_latitude_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_latitude']; ?>" />
				<input type="hidden" id="branch_longitude_<?php echo $key; ?>" name="branch_longitude_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_longitude']; ?>" />
			</td>
			<td>
				<a href="#" onclick="edit_branch('<?php echo $key; ?>');" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข</a>
				<a href="#" onclick="del_branch('<?php echo $type; ?>', '<?php echo $key; ?>');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ</a>
			</td>
		</tr>
		<?php
				}
			}
		?>
	</tbody>
</table>
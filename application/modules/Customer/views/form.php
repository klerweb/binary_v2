<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="frmCustomer" name="frmCustomer" method="post" enctype="multipart/form-data" action="<?php echo base_url().'customer/save'; ?>" >
					<div class="form-horizontal form-label-left" >
						<input type="hidden" id="customer_id" name="customer_id" value="<?php echo $customer_id; ?>" />
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">ลูกค้า <span class="red">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" id="customer_company" name="customer_company" class="form-control" value="<?php echo $customer_company; ?>" required />
							</div>
						</div>
						<hr/>
						<span style="font-size: 18px; font-weight: 400;">สาขา</span>
						<a href="#" class="btn btn-dark btn-xs" data-toggle="modal" data-target="#modal-branch"><i class="fa fa-plus"></i> เพิ่ม</a>
						<div id="divBranch" style="margin-top:10px;">
							<table id="tblBranch" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ลำดับ</th>
										<th>สาขา</th>
										<th>จัดการ</th>
									</tr>
								</thead>
								<tbody>
								<?php 
									if(!empty($customer_branch))
									{
										$i_branch = 1;
										foreach ($customer_branch as $key => $branch)
										{
											$branch = (array)$branch;
					
											if($branch['customer_id']!='')
											{
												$key = $branch['customer_branch_id'];
												$type = 'edit';
											}
											else
											{
												$branch['customer_branch_id'] = $key;
												$type = 'add';
											}
								?>
									<tr>
										<td><?php echo $i_branch++; ?></td>
										<td>
											<b><?php echo $branch['customer_branch_name']; ?></b><br/>
											ผู้ติดต่อ: <?php echo $branch['customer_branch_contact']; ?><br/>
											เบอร์โทรศัพท์: <?php echo $branch['customer_branch_tel']; ?><br/>
											อีเมล์: <?php echo $branch['customer_branch_email']; ?><br/>
											ที่อยู่: <?php echo $branch['customer_branch_address']; ?><br/>
											พิกัด: <?php echo $branch['customer_branch_latitude'].' , '.$branch['customer_branch_longitude']; ?> <?php if(!empty($branch['customer_branch_latitude']) && !empty($branch['customer_branch_longitude'])) echo '<a href="http://www.google.com/maps/place/'.$branch['customer_branch_latitude'].','.$branch['customer_branch_longitude'].',8z" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a>'; ?>
											<input type="hidden" id="branch_id_<?php echo $key; ?>" name="branch_id_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_id']; ?>" />
											<input type="hidden" id="branch_name_<?php echo $key; ?>" name="branch_name_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_name']; ?>" />
											<input type="hidden" id="branch_tel_<?php echo $key; ?>" name="branch_tel_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_tel']; ?>" />
											<input type="hidden" id="branch_email_<?php echo $key; ?>" name="branch_email_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_email']; ?>" />
											<input type="hidden" id="branch_address_<?php echo $key; ?>" name="branch_address_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_address']; ?>" />
											<input type="hidden" id="branch_latitude_<?php echo $key; ?>" name="branch_latitude_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_latitude']; ?>" />
											<input type="hidden" id="branch_longitude_<?php echo $key; ?>" name="branch_longitude_<?php echo $key; ?>" value="<?php echo $branch['customer_branch_longitude']; ?>" />
										</td>
										<td>   
			                                <a href="#" onclick="edit_branch('<?php echo $key; ?>');" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข</a>  
			                                <a href="#" onclick="del_branch('<?php echo $type; ?>', '<?php echo $key; ?>');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ</a>
			                              </td>
									</tr>
								<?php
										}
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>customer';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="modal-branch" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">สาขา</h4>
			</div>
			<div class="modal-body">
				<form id="frmBranch" name="frmBranch" method="post" enctype="multipart/form-data" action="<?php echo base_url().'customer/save_branch'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="customer_id" name="customer_id" value="<?php echo $customer_id; ?>" />
					<input type="hidden" id="customer_branch_id" name="customer_branch_id" value="" />
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">สาขา <span class="red">*</span></label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="customer_branch_name" name="customer_branch_name" class="form-control" value="" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">ผู้ติดต่อ</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="customer_branch_contact" name="customer_branch_contact" class="form-control" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">เบอร์โทรศัพท์ <span class="red">*</span></label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="customer_branch_tel" name="customer_branch_tel" class="form-control" value="" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">อีเมล์</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="email" id="customer_branch_email" name="customer_branch_email" class="form-control" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">ที่อยู่</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<textarea id="textarea" id="customer_branch_address" name="customer_branch_address"  name="textarea" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">พิกัด</label>
						<div class="col-md-5 col-sm-5 col-xs-12">
							<input type="text" id="customer_branch_latitude" name="customer_branch_latitude" class="form-control" value="" />
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
							<input type="text" id="customer_branch_longitude" name="customer_branch_longitude" class="form-control" value="" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnBranch" class="btn btn-success">บันทึก</button>
				<button type="button" id="btnBranchCancel" class="btn btn-primary">ยกเลิก</button>
			</div>
		</div>
	</div>
</div>
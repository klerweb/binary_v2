<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends MX_Controller 
{
	public $title = 'ตัวแทนจำหน่าย';
	public $menu = array('menu' => 'contact', 'sub' => 'supplier');
	
	function __construct()
	{
		parent::__construct();
		
		if(empty($_SESSION['binary_login']))
		{
			header('Location: '.base_url());
			exit();
		}
		
        $this->load->model('supplier/supplier_mod', 'supplier');      
	}
	
	public function index()
	{
		$isSave = $this->input->get('isStatus');
		$popup_alert = '';
		if($isSave!='')
		{
			switch ($isSave)
			{
				case 'success_add': $popup_alert = popup_alert('success', 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
				break;
				case 'error_add': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้');
				break;
				case 'success_edit': $popup_alert = popup_alert('success', 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ');
				break;
				case 'error_edit': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
				break;
				case 'success_del': $popup_alert = popup_alert('success', 'ลบข้อมูลเรียบร้อยแล้วค่ะ');
				break;
				case 'error_del': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้');
				break;
			}
		}
		
		$data_content = array(
				'popup_alert' => $popup_alert,
				'supplier' => $this->supplier->get_supplier()
			);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_supplier/supplier.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('index', $data_content, TRUE),
			);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_content = $id==''? $this->supplier->model() : (array)$this->supplier->get_supplier($id);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_supplier/form.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}

	public function save()
	{
		$data = $this->input->post();
		
		if($data['supplier_id']=='')
		{
			$data['supplier_id'] = $this->supplier->save($data);
			$isSave = is_numeric($data['supplier_id'])? 'success_add' : 'error_add';
		}
		else
		{
			$isSave = $this->supplier->save($data, $data['supplier_id'])? 'success_edit' : 'error_edit';
		}
		
		redirect(base_url().'supplier?isStatus='.$isSave);
	}
	
	public function del($id)
	{
		$data['supplier_id'] = $id;
		$data['supplier_visible'] = '0';
		
		$isSave = $this->supplier->save($data, $data['supplier_id'])? 'success_del' : 'error_del';
		
		redirect(base_url().'supplier?isStatus='.$isSave);
	}
	
	public function del_all()
	{
		$isSave = 'error_del';
	
		if(!$this->input->post('chk'))
		{
			foreach ($this->input->post('chk') as $value)
			{
				$data['supplier_id'] = $value;
				$data['supplier_visible'] = '0';
	
				$this->supplier->save($data, $data['supplier_id']);
			}
	
			$isSave = 'success_del';
		}
	
		redirect(base_url().'supplier?isStatus='.$isSave);
	}
    
    public function save_popup(){
        $data = $this->input->post();
 
           $result_id = $this->supplier->save($data);   
           if($result_id) echo json_encode(array('code'=>"200","description"=>"success","id"=>$result_id));
           else echo json_encode(array('code'=>"100","description"=>"error"));    
    }
}

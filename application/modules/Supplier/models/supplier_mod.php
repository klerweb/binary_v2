<?php
class Supplier_mod extends MY_Model
{
	private $table = 'supplier';
	private $key = 'supplier_id';
	private $db_filed = array(
            "supplier_id" => "supplier_id",
            "supplier_company" => "supplier_company",
            "title_id" => "title_id",
            "supplier_fname" => "supplier_fname",
            "supplier_lname" => "supplier_lname",
            "supplier_tel" => "supplier_tel",
            "supplier_email" => "supplier_email",
            "supplier_address" => "supplier_address",
            "supplier_visible" => "supplier_visible",
            "supplier_createby" => "supplier_createby",
            "supplier_createdate" => "supplier_createdate",  
            "supplier_updateby" => "supplier_updateby",  
            "supplier_updatedate" => "supplier_updatedate"
            );
 
	public function get_dbfiled()
	{
		return  $this->db_filed;
	}
	
	function get_supplier($supplier_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['supplier_visible'] = '1';
		
		if($supplier_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $supplier_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function save($data=NULL, $supplier_id=NULL)
	{
		$supplier = (object) array();
		if(!$supplier_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $supplier->${'value'} = $data[$value] ;
			}
			
			unset($supplier->{$this->db_filed['supplier_id']});
	
			$supplier->{$this->db_filed['supplier_visible']} = '1';
			$supplier->{$this->db_filed['supplier_createdate']} = date(DATETIME_FORMAT_2DB);
			$supplier->{$this->db_filed['supplier_createby']} = 1;
			$supplier->{$this->db_filed['supplier_updatedate']} = date(DATETIME_FORMAT_2DB);
			$supplier->{$this->db_filed['supplier_updateby']} = 1;
			 
			$this->db->set($supplier)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $supplier->${'value'} = $data[$value] ;
			}
			
			$supplier->{$this->db_filed['supplier_updatedate']} = date(DATETIME_FORMAT_2DB);
			$supplier->{$this->db_filed['supplier_updateby']} = 1;
			
			return $this->db->where($this->key, $data['supplier_id'])
				->set($supplier)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['supplier_id'] = '';
		$data['supplier_company'] = '';
		$data['title_id'] = '';
		$data['supplier_fname'] = '';
		$data['supplier_lname'] = '';
		$data['supplier_tel'] = '';
		$data['supplier_email'] = '';
		$data['supplier_address'] = '';
		$data['supplier_visible'] = '1';
	
		return $data;
	}
 }
?>

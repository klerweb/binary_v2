<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_customer">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="frmServiceRequest" name="frmServiceRequest" method="post" enctype="multipart/form-data" data-parsley-validate action="<?php echo base_url().'service_service/save'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="service_id" name="service_id" value="<?php echo $service_id; ?>" />
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่รับงาน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="service_date" name="service_date" class="date-picker form-control" value="<?php echo getShowDateFormat($service_date); ?>" required readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">พนักงานที่รับเรื่อง</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="employee_id" name="employee_id" class="form-control">
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($employee))
										{
											foreach ($employee as $list_employee) 
											{
												$selected = $list_employee->employee_id==$employee_id? ' selected' : '';
												echo '<option value="'.$list_employee->employee_id.'"'.$selected.'>'.$list_employee->employee_fname.' '.$list_employee->employee_lname.'</option>';
											}
										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ประเภทบริการ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="type_id" name="type_id" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($type))
										{
											foreach ($type as $list_type) 
											{
												$selected = $list_type->type_id==$type_id? ' selected' : '';
												echo '<option value="'.$list_type->type_id.'"'.$selected.'>'.$list_type->type_name.'</option>';
											}
										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียดงาน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<textarea id="textarea" id="service_desc" name="service_desc"  name="textarea" class="form-control" rows="4" required><?php echo $service_desc; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่สิ้นสุดงาน <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="service_end" name="service_end" class="date-picker form-control" value="<?php echo getShowDateFormat($service_end); ?>" required readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">สรุปผลการดำเนินงาน</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<textarea id="textarea" id="service_summary" name="service_summary"  name="textarea" class="form-control" rows="4"><?php echo $service_summary; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">สถานะ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="service_visible" name="service_visible" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<option value="1"<?php if($service_visible=='1') echo ' selected'; ?>>รับเรื่อง</option>
								<option value="3"<?php if($service_visible=='3') echo ' selected'; ?>>บริการ</option>
								<option value="2"<?php if($service_visible=='2') echo ' selected'; ?>>ยกเลิก</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ลูกค้า <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="customer_id" name="customer_id" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($customer))
										{
											foreach ($customer as $list_customer) 
											{
												$selected = $list_customer->customer_id==$customer_id? ' selected' : '';
												echo '<option value="'.$list_customer->customer_id.'"'.$selected.'>'.$list_customer->customer_company.'</option>';
											}
										}
								?>
								</select>
							</div>
						</div>
						<hr/>
						<span style="font-size: 18px; font-weight: 400;">สาขาที่ปฏิบัติงาน</span>
						<a href="#" class="btn btn-dark btn-xs" data-toggle="modal" data-target="#modal-branch"><i class="fa fa-plus"></i> เพิ่ม</a>
						<div id="divBranch" style="margin-top:10px;">
							<table id="tblBranch" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ลำดับ</th>
										<th>สาขา</th>
										<th>จัดการ</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
						<hr/>
						<span style="font-size: 18px; font-weight: 400;">ตารางาน</span>
						<a href="#" class="btn btn-dark btn-xs" data-toggle="modal" data-target="#modal-branch"><i class="fa fa-plus"></i> เพิ่ม</a>
						<div id="divBranch" style="margin-top:10px;">
							<table id="tblBranch" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ลำดับ</th>
										<th>วันที่และเวลา</th>
										<th>ประเภท</th>
										<th>ผู้รับผิดชอบ</th>
										<th>สถานะ</th>
										<th>เอกสาร</th>
										<th>จัดการ</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
						<hr/>
						<span style="font-size: 18px; font-weight: 400;">ใบเสนอราคา</span>
						<a href="#" class="btn btn-dark btn-xs" data-toggle="modal" data-target="#modal-branch"><i class="fa fa-plus"></i> เพิ่ม</a>
						<div id="divBranch" style="margin-top:10px;">
							<table id="tblBranch" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ลำดับ</th>
										<th>ประเภท</th>
										<th>บริษัท</th>
										<th>จำนวนเงิน</th>
										<th>เอกสาร</th>
										<th>จัดการ</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
						<hr/>
						<span style="font-size: 18px; font-weight: 400;">ใบสั่งซื้อ</span>
						<a href="#" class="btn btn-dark btn-xs" data-toggle="modal" data-target="#modal-branch"><i class="fa fa-plus"></i> เพิ่ม</a>
						<div id="divBranch" style="margin-top:10px;">
							<table id="tblBranch" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ลำดับ</th>
										<th>ประเภท</th>
										<th>บริษัท</th>
										<th>จำนวนเงิน</th>
										<th>เอกสาร</th>
										<th>จัดการ</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
						<hr/>
						<span style="font-size: 18px; font-weight: 400;">ใบสั่งซื้อ</span>
						<a href="#" class="btn btn-dark btn-xs" data-toggle="modal" data-target="#modal-branch"><i class="fa fa-plus"></i> เพิ่ม</a>
						<div id="divBranch" style="margin-top:10px;">
							<table id="tblBranch" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ลำดับ</th>
										<th>ค่าใช้จ่าย</th>
										<th>จำนวนเงิน</th>
										<th>จัดการ</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>service_request';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="row"></div>
<div class="popup_alert"><?php echo $popup_alert; ?></div>    
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_content">
				<form id="frmServiceRequest" name="frmServiceRequest" method="post" action="<?php echo base_url(); ?>service_request/del_all" onsubmit="return confirm('คุณต้องการลบข้อมูล?');">
					<button type="button" class="btn btn-dark btn-sm" onclick="window.location.href='<?php echo base_url(); ?>service_request/form';"><i class="fa fa-plus"></i> เพิ่ม</button>
					<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i> ลบ</button>
					<hr/>
					<table id="tblServiceRequest" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="padding-left:0px; padding-right:0px;"><center><input type="checkbox" id="chkAll" name="chkAll" class="flat" /></center></th>
								<th>ลำดับ</th>
								<th>รหัส</th>
								<th>วันที่รับเรื่อง</th>
								<th>บริษัท</th>
								<th>ประเภทงาน</th>
								<th>สถานะ</th>
								<th>จัดการ</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							if(!empty($service))
							{ 
								$i_service = 1;
								foreach ($service as $list_service)
								{
									$button = '';
									switch ($list_service['service_visible'])
									{
										case '1': $list_service['service_visible'] = 'รับเรื่อง';
												$button = '<a href="'.base_url().'service_request/form/'.$list_service['service_id'].'" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข</a>
	                                				<a href="'.base_url().'service_request/del/'.$list_service['service_id'].'" onclick="return confirm(\'คุณต้องการลบข้อมูล?\');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ</a>';
											break;
										case '2': $list_service['service_visible'] = 'ยกเลิก';
											break;
										case '3': $list_service['service_visible'] = 'บริการ';
											break;
									}
						?>
							<tr>
								<td align="center"><input type="checkbox" name="chk[]" class="flat chk" value="<?php echo $list_service['service_id']; ?>" /></td>
								<td align="center"><?php echo $i_service++; ?></td>
								<td align="center"><?php echo $list_service['service_year'].'-'.service_format($list_service['service_id']); ?></td>
								<td align="center"><?php echo $list_service['service_date']; ?></td>
								<td><?php echo $list_service['customer_company']; ?></td>
								<td><?php echo $list_service['service_type_name']; ?></td>
								<td><?php echo $list_service['service_visible']; ?></td>
								<td><?php echo $button; ?></td>
							</tr>
						<?php 
								}
							} 
						?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
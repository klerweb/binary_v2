<?php
class Service_mod extends MY_Model
{
	private $table = "service";
	private $key = "service_id";
	private $db_filed = array(
			"service_id" => "service_id",
			"service_code" => "service_code",
			"service_date" => "service_date",
			"service_desc" => "service_desc",
			"employee_id" => "employee_id",
			"customer_id" => "customer_id",
			"type_id" => "type_id",
			"service_end" => "service_end",
			"service_summary" => "service_summary",
			"service_createby" => "service_createby",
			"service_createdate" => "service_createdate",
			"service_updateby" => "service_updateby",
			"service_updatedate" => "service_updatedate"
	);
	
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_service($service_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['service_visible'] = '1';
		
		if($service_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $service_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	public function all()
	{
		$this->db->select("*, EXTRACT(YEAR FROM binary_service.service_createdate) AS service_year");
		$this->db->from($this->table);
		$this->db->join("customer", "customer.customer_id = service.customer_id", "LEFT");
		$this->db->join("service_type", "service_type.service_type_id = service.service_type_id", "LEFT");
		$this->db->where_in("service.service_visible", array("1", "2", "3"));
	
		$this->db->order_by("service.service_id", "desc");
	
		$query = $this->db->get();
	
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	function save($data=NULL, $service_id=NULL)
	{
		$service = (object) array();
		if(!$service_id)
		{
			if(@$data['service_id']) $service->service_id = $data['service_id']  ;
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $service->${'value'} = $data[$value] ;
			}
	
			$service->{$this->db_filed['service_visible']} = '1';
			$service->{$this->db_filed['service_createdate']} = date(DATETIME_FORMAT_2DB);
			$service->{$this->db_filed['service_createby']} = 1;
			$service->{$this->db_filed['service_updatedate']} = date(DATETIME_FORMAT_2DB);
			$service->{$this->db_filed['service_updateby']} = 1;
			 
			$this->db->set($service)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $service->${'value'} = $data[$value] ;
			}
			
			$service->{$this->db_filed['service_updatedate']} = date(DATETIME_FORMAT_2DB);
			$service->{$this->db_filed['service_updateby']} = 1;
			
			return $this->db->where($this->key, $data['service_id'])
				->set($service)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['service_id'] = '';
		$data['service_code'] = '';
		$data['service_date'] = date('Y-m-d');
		$data['service_desc'] = '';
		$data['employee_id'] = '';
		$data['customer_id'] = '';
		$data['type_id'] = '';
		$data['service_end'] = '';
		$data['service_summary'] = '';
		$data['service_visible'] = '1';
		
		return $data;
	}
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_request extends MX_Controller 
{
	public $title = 'รับเรื่อง';
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('employee/employee_mod', 'employee'); 
        $this->load->model('customer/customer_mod', 'customer');
        $this->load->model('type/type_mod', 'type');
        $this->load->model('service_request/service_mod', 'service');
	}
	
	public function index()
	{
		$isSave = $this->input->get('isStatus');
		$popup_alert = '';
		if($isSave!='')
		{
			switch ($isSave)
			{
				case 'success_add': $popup_alert = popup_alert('success', 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_add': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้');
					break;
				case 'success_edit': $popup_alert = popup_alert('success', 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_edit': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_del': $popup_alert = popup_alert('success', 'ลบข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_del': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้');
					break;
			}
		}
		
		$data_content = array(
				'popup_alert' => $popup_alert,
				'service' => $this->service->all()
			);
		
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_service_request/service_request.js'),
				'content' => $this->parser->parse('index', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{	
		if($id=='')
		{
			$data_content = $this->service->model();
			$data_content['module'] = array();
			
			$service_code = '';
		}
		else
		{
			$data_content = (array)$this->service->get_service($id);
			
			list($service_year, $service_month, $service_day) = explode('-', $data_content['service_createdate']);
			
			$service_code = ' :: '.$service_year.'-'.service_format($data_content['service_id']);
		}
		
		$data_content['employee'] = $this->employee->get_employee();
		$data_content['customer'] = $this->customer->get_customer();
		$data_content['type'] = $this->type->get_type();
		
		$data = array(
				'title' => $this->title,
				'title_sub' => $service_code,
				'js_other' => array('modules_service_request/form.js'),
				'content' => $this->parser->parse('form', $data_content, TRUE),
		);
		
		$this->parser->parse('main', $data);
	}

	public function save()
	{
		$data = $this->input->post();
		
		if($data['service_id']=='')
		{
			$data['service_id'] = $this->service->save($data);
			$isSave = is_numeric($data['service_id'])? 'success_add' : 'error_add';
		}
		else
		{
			$isSave = $this->service->save($data, $data['service_id'])? 'success_edit' : 'error_edit';
		}
		
		redirect(base_url().'service?isStatus='.$isSave);
	}
	
	public function del($id)
	{
		$data['service_id'] = $id;
		$data['service_visible'] = '0';
		
		$isSave = $this->service->save($data, $data['service_id'])? 'success_del' : 'error_del';
		
		redirect(base_url().'service?isStatus='.$isSave);
	}
	
	public function del_all()
	{
		$isSave = 'error_del';
	
		if(!empty($this->input->post('chk')))
		{
			foreach ($this->input->post('chk') as $value)
			{
				$data['service_id'] = $value;
				$data['service_visible'] = '0';
	
				$this->service->save($data, $data['service_id']);
			}
	
			$isSave = 'success_del';
		}
	
		redirect(base_url().'service?isStatus='.$isSave);
	}
}
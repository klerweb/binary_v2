<?php
    class Service_mod extends MY_Model{
        
        private $table='service';
        private $key='service_id';
        private $db_filed=array(
            "service_id"=>"service_id",
            "service_code"=>"service_code",
            /*"service_name"=>"service_name",*/
            "service_end"=>"service_end",
            "service_time"=>"service_time",
            "service_date"=>"service_date",
            "service_desc"=>"service_desc",
            "service_summary"=>"service_summary",
            "type_id"=>"type_id",
            "employee_id"=>"employee_id",
            "customer_id"=>"customer_id",
            "customer_address_id"=>"customer_address_id",
            "service_visible"=>"service_visible",
            "service_createdate"=>"service_createdate",
            "service_createby"=>"service_createby",
            "service_updatedate"=>"service_updatedate",
            "service_updateby"=>"service_updateby",
            );
 
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
       function get_serviceStatus($service_visible=''){                                                                                               
           $array_status=array(1=>'รับเรื่อง',2=>'ยกเลิก',3=>'ส่งต่อ',4=>'ลบจากบริการ',5=>'ยกเลิก',6=>'ดำเนินการเรียบร้อย');
           if($service_visible && isset($array_status[$service_visible])) return   $array_status[$service_visible] ;
           else return $array_status ; 
       }
                                                                                                         
       function get_service($service_id=NULL,$filed=NULL){      
     
            if($service_id){ 
                $data =  $this->get_data($this->table,$this->key,'',$service_id);   
                if($filed){ return $data->{$this->db_filed[$filed]};  }
                return  $data ; 
            }
            
            else return $this->get_data($this->table) ;
        }
        
       function save($data=NULL,$service_id=NULL){
       $service = (object) array();   
       if(!$service_id){
                            
             if(@$data['service_id']) $service->service_id = $data['service_id']  ;
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $service->${'value'} = $data[$value] ;
             } 
              
             /*$service->{$this->db_filed['service_visible']} = ''.STATUS_ACTIVE.'';   */
             $service->{$this->db_filed['service_createdate']} = date("Y-m-d H:i:s");
             $service->{$this->db_filed['service_createby']} = 1;     
         
          
              $this->db->set($service)->insert($this->table);
 
            return $this->db->insert_id(); 
            
       }else{
              
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $service->${'value'} = $data[$value] ;
             }   
            $service->{$this->db_filed['service_updatedate']} = date("Y-m-d H:i:s");
            $service->{$this->db_filed['service_updateby']} =  1;
 
             return $this->db->where($this->key, $data['service_id'])
            ->set($service)
            ->update($this->table);   
            
             
              return 1;
       }                      
       
    }   
    
       function isDelete($service_id=NULL){
                $sql = 'SELECT  sum(counts) as  sumCounts from (
                        SELECT count(*) as counts FROM binary_quotation where   binary_quotation.service_id ='.$service_id.' group by service_id  UNION 
                        SELECT count(*) as counts FROM binary_purchase where   binary_purchase.service_id ='.$service_id.' group by service_id  UNION
                        SELECT count(*) as counts FROM binary_expenses where   binary_expenses.service_id ='.$service_id.' group by service_id ) tables  
                     ';    
          return current($this->db->query($sql)->result());  
          
          
       } 
       
       function getMaxId(){
           $maxid = $this->db->query('SELECT MAX(service_id) AS "maxid" FROM binary_service' )->row()->maxid;   
           return $maxid ; 
       }
       
       function serviceTmp(){
         $maxid = $this->db->query('SELECT MAX(service_tmp_id) AS "maxid" FROM binary_service_tmp' )->row()->maxid;   
         $maxid = $maxid+1;  
           $service_tmp = (object) array(); 
           $service_tmp->service_tmp_code = service_format2($maxid);         
         
           $this->db->set($service_tmp)->insert('service_tmp');
              
              return $maxid ; 
       }
       
       function getidServiceFromTmp($code=''){
           $id = $this->db->query('SELECT service_tmp_id    FROM binary_service_tmp where service_tmp_code = "'.$code.'" ' )->row();  
 
           return $id->service_tmp_id ;  
       }
       
        
 }
?>

<?php
    class Service_branch_mod extends MY_Model{
        
        private $table='service_branch';
        private $key='service_branch_id';
        private $db_filed=array(
            "service_branch_id"=>"service_branch_id",
            "service_id"=>"service_id",
            "customer_id"=>"customer_id",
            "customer_branch_id"=>"customer_branch_id",
            "service_branch_visible"=>"service_branch_visible",
            "service_branch_createby"=>"service_branch_createby",
            "service_branch_createdate"=>"service_branch_createdate",
            "service_branch_updateby"=>"service_branch_updateby",
            "service_branch_updatedate"=>"service_branch_updatedate",   
            );
                   
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
       function get_serviceStatus($service_visible=''){                                                                                               
           $array_status=array(1=>'รับเรื่อง',2=>'บริการ',3=>'ยกเลิก',4=>'ลบจากบริการ',5=>'เสร็จสมบรูณ์',6=>'ยกเลิก');
           if($service_visible && isset($array_status[$service_visible])) return   $array_status[$service_visible] ;
           else return $array_status ; 
       }
                                                                                                         
       function get_service_branch($service_branch_id=NULL,$filed=NULL){      
     
            if($service_branch_id){ 
                $data =  $this->get_data($this->table,$this->key,'',$service_branch_id);   
                if($filed){ return $data->{$this->db_filed[$filed]};  }
                return  $data ; 
            }
            
            else return $this->get_data($this->table) ;
        }    
        
       function get_service_branchByservice($service_id=NULL){
            $cfg = "";
               if(!($service_id)) return false ;
                $cfg->where['service_id'] = $service_id;
                $cfg->where['service_branch_visible'] = '1';
               if($service_id) $cfg->where['service_id'] = $service_id;
                $data  = $this->get_data($this->table,'',$cfg) ;     
                  
                return $data ;      
       }
        
       function save($data=NULL,$service_branch_id=NULL){
       $service_branch = (object) array();   
       if(!$service_branch_id){
                            
             if(@$data['service_branch_id']) $service_branch->service_branch_id = $data['service_branch_id']  ;
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $service_branch->${'value'} = $data[$value] ;
             } 
              
 
             $service_branch->{$this->db_filed['service_branch_createdate']} = date("Y-m-d H:i:s");
             $service_branch->{$this->db_filed['service_branch_createby']} = 1;     
         
              $this->db->set($service_branch)->insert($this->table);
 
            return $this->db->insert_id(); 
            
       }else{
           
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $service_branch->${'value'} = $data[$value] ;
             }   
            $service_branch->{$this->db_filed['service_branch_updatedate']} = date("Y-m-d H:i:s");
            $service_branch->{$this->db_filed['service_branch_updateby']} =  1;
               
             return $this->db->where($this->key, $data['service_branch_id'])
            ->set($service_branch)
            ->update($this->table);     
       }                      
       
    }    
        
 }
?>

<?php
    class Job_mod extends MY_Model{
        
        private $table='job';
        private $key='job_id';
        private $db_filed=array(
            "job_id"=>"job_id",
            "job_name"=>"job_name",
            "job_startdate"=>"job_startdate",
            "job_enddate"=>"job_enddate",
            "job_type"=>"job_type",
            "employee_id"=>"employee_id",
            "job_summarry"=>"job_summarry",
            "subcontract_id"=>"subcontract_id",
            "job_summarry"=>"job_summarry",
            "customer_id"=>"customer_id",
            "service_id"=>"service_id",
            "job_path"=>"job_path",
            "job_desc"=>"job_desc",
            "job_visible"=>"job_visible",
            "job_createby"=>"job_createby",
            "job_createdate"=>"job_createdate",
            "job_updateby"=>"job_updateby",
            "job_updatedate"=>"job_updatedate"
            );
                      
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
       function get_jobStatus($job_visible=''){
           $array_status=array(1=>'กำลังดำเนินการ',2=>'ยกเลิก',3=>'คืนเงาน',4=>'สำเร็จ');
           if($job_visible && isset($array_status[$job_visible])) return   $array_status[$job_visible] ;
           else return $array_status ; 
       }
        
 
      function get_job($service_id=NULL,$job_id=NULL,$filed=NULL){      
            $cfg = "";
               if(empty($service_id)) return false ;
                $cfg->where['service_id'] = $service_id;
               if($job_id) $cfg->where['job_id'] = $job_id;
                $data  = $this->get_data($this->table,'',$cfg) ;     
                return $data ;      
        }  
        
      function save($data=NULL,$job_id=NULL){
       $job = (object) array();   
       if(!$job_id){
                            
             if(@$data['job_id']) $job->job_id = $data['job_id']  ;
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $job->${'value'} = $data[$value] ;
             } 
              
             $job->{$this->db_filed['job_createdate']} = date("Y-m-d H:i:s");
             $job->{$this->db_filed['job_createby']} = 1;     
         
              $this->db->set($job)->insert($this->table);
 
            return $this->db->insert_id(); 
            
       }else{
           
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $job->${'value'} = $data[$value] ;
             }   
            $job->{$this->db_filed['job_updatedate']} = date("Y-m-d H:i:s");
            $job->{$this->db_filed['job_updateby']} =  1;

             return $this->db->where($this->key, $data['job_id'])
            ->set($job)
            ->update($this->table);     
       }                      
       
    } 
    
    function show_sourch($source=''){
           switch($source){
               case "customer" : return "ลูกค้า";
               case "supplier" : return "ตัวแทนจำหน่าย";
               case "subcontract" : return "ผู้รับเหมา";
               case "employee" : return "พนักงาน";
           }
       }   
 }
?>

<?php
    class Expenses_mod extends MY_Model{
        
        private $table='expenses';
        private $key='expenses_id';
        private $db_filed=array(
            "expenses_id"=>"expenses_id",
            "expenses_name"=>"expenses_name",
            "expenses_date"=>"expenses_date",    
            "service_id"=>"service_id",   
            "expenses_amount"=>"expenses_amount",
            "expenses_path"=>"expenses_path",   
            "expenses_visible"=>"expenses_visible",
            "expenses_createby"=>"expenses_createby",
            "expenses_createdate"=>"expenses_createdate",
            "expenses_updateby"=>"expenses_updateby",
            "expenses_updatedate"=>"expenses_updatedate"   
            );
                      
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
        function get_expensesStatus($expenses_visible=''){
           $array_status=array(1=>'กำลังดำเนินการ',2=>'ไม่อนุมัติ',3=>'อนุมัติ');
           if($expenses_visible && isset($array_status[$expenses_visible])) return   $array_status[$expenses_visible] ;
           else return $array_status ; 
       }
 
        function get_expenses($service_id=NULL,$expenses_id=NULL,$filed=NULL){      
            $cfg = "";
               if(empty($service_id)) return false ;
                $cfg->where['service_id'] = $service_id;
               if($expenses_id) $cfg->where['expenses_id'] = $expenses_id;
                $data  = $this->get_data($this->table,'',$cfg) ;     
                return $data ;      
        } 
        
      function save($data=NULL,$expenses_id=NULL){
       $expenses = (object) array();   
       if(!$expenses_id){
                            
             if(@$data['expenses_id']) $expenses->service_id = $data['expenses_id']  ;
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $expenses->${'value'} = $data[$value] ;
             } 
              
 
             $expenses->{$this->db_filed['expenses_createdate']} = date("Y-m-d H:i:s");
             $expenses->{$this->db_filed['expenses_createby']} = 1;     
         
              $this->db->set($expenses)->insert($this->table);
 
            return $this->db->insert_id(); 
            
       }else{
           
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $expenses->${'value'} = $data[$value] ;
             }   
            $expenses->{$this->db_filed['expenses_updatedate']} = date("Y-m-d H:i:s");
            $expenses->{$this->db_filed['expenses_updateby']} =  1;

             return $this->db->where($this->key, $data['expenses_id'])
            ->set($expenses)
            ->update($this->table);     
       }                      
       
    }   
 }
?>

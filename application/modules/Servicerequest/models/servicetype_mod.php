<?php
    class Servicetype_mod extends MY_Model{
        
        private $table='type';
        private $key='type_id';
        private $db_filed=array(
            "type_id"=>"type_id",
            "type_name"=>"type_name",
            );
            
     
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
       function get_servicetype($servicetype_id=NULL,$filed=NULL){      
     
            if($servicetype_id){ 
                $data =  $this->get_data($this->table,$this->key,'',$servicetype_id);   
                if($filed){ return $data->{$this->db_filed[$filed]};  }
                return  $data ; 
            }
            
            else return $this->get_data($this->table) ;
        }   
 }
?>

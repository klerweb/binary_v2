<?php
    class Purchase_mod extends MY_Model{
        
        private $table='purchase';
        private $key='purchase_id';
        private $db_filed=array(
            "purchase_id"=>"purchase_id",
            "purchase_date"=>"purchase_date",
            "service_id"=>"service_id",
            "purchase_type"=>"purchase_type",
            "purchase_source"=>"purchase_source",
            "customer_id"=>"customer_id",
            "supplier_id"=>"supplier_id",
            "subcontract_id"=>"subcontract_id",
            "purchase_amount"=>"purchase_amount",
            "purchase_path"=>"purchase_path",
            "purchase_visible"=>"purchase_visible",
            "purchase_createby"=>"purchase_createby",
            "purchase_createdate"=>"purchase_createdate",
            "purchase_updateby"=>"purchase_updateby",
            "purchase_updateby"=>"purchase_updateby",
            "purchase_updatedate"=>"purchase_updatedate",
            );
 
 
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
        function get_purchase($service_id=NULL,$purchase_id=NULL,$filed=NULL){      
            $cfg = "";
               if(empty($service_id)) return false ;
                $cfg->where['service_id'] = $service_id;
               if($purchase_id) $cfg->where['purchase_id'] = $purchase_id;
                $data  = $this->get_data($this->table,'',$cfg) ;     
                return $data ;      
        }
        
       function save($data=NULL,$purchase_id=NULL){
       $purchase = (object) array();   
       if(!$purchase_id){
                            
             if(@$data['purchase_id']) $purchase->purchase_id = $data['purchase_id']  ;
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $purchase->${'value'} = $data[$value] ;
             } 
              
             $purchase->{$this->db_filed['purchase_visible']} = ''.STATUS_ACTIVE.'';
             $purchase->{$this->db_filed['purchase_createdate']} = date("Y-m-d H:i:s");
             $purchase->{$this->db_filed['purchase_createby']} = 1;     
         
              $this->db->set($purchase)->insert($this->table);
 
            return $this->db->insert_id(); 
            
       }else{
           
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $purchase->${'value'} = $data[$value] ;
             }   
            $purchase->{$this->db_filed['purchase_updatedate']} = date("Y-m-d H:i:s");
            $purchase->{$this->db_filed['purchase_updateby']} =  1;
               
             return $this->db->where($this->key, $data['purchase_id'])
            ->set($purchase)
            ->update($this->table);     
       }       
       
    } 
       
       function show_sourch($source=''){
           switch($source){
               case "customer" : return "ลูกค้า";
               case "supplier" : return "ตัวแทนจำหน่าย";
               case "subcontract" : return "ผู้รับเหมา";
           }
       }  
 }
?>

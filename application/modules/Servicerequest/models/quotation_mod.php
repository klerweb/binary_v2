<?php
    class Quotation_mod extends MY_Model{
        
        private $table='quotation';
        private $key='quotation_id';
        private $db_filed=array(
            "quotation_id"=>"quotation_id",
            "quotation_date"=>"quotation_date",
            "service_id"=>"service_id",
            "quotation_type"=>"quotation_type",
            "quotation_source"=>"quotation_source",
            "customer_id"=>"customer_id",
            "supplier_id"=>"supplier_id",
            "subcontract_id"=>"subcontract_id",
            "quotation_amount"=>"quotation_amount",
            "quotation_path"=>"quotation_path",
            "quotation_visible"=>"quotation_visible",
            "quotation_createby"=>"quotation_createby",
            "quotation_createdate"=>"quotation_createdate",
            "quotation_updateby"=>"quotation_updateby",
            "quotation_updateby"=>"quotation_updateby",
            "quotation_updatedate"=>"quotation_updatedate",
            );
 
 
       function get_dbfiled(){
            return  $this->db_filed ;
        }
        
       function get_quotation($service_id=NULL,$quotation_id=NULL,$filed=NULL){      
            $cfg = "";
               if(empty($service_id)) return false ;
                $cfg->where['service_id'] = $service_id;
               if($quotation_id) $cfg->where['quotation_id'] = $quotation_id;
                $data  = $this->get_data($this->table,'',$cfg) ;     
                return $data ;      
        }
        
      function save($data=NULL,$quotation_id=NULL){
       $quotation = (object) array();   
       if(!$quotation_id){
                            
             if(@$data['quotation_id']) $quotation->service_id = $data['quotation_id']  ;
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $quotation->${'value'} = $data[$value] ;
             } 
              
             $quotation->{$this->db_filed['quotation_visible']} = ''.STATUS_ACTIVE.'';
             $quotation->{$this->db_filed['quotation_createdate']} = date("Y-m-d H:i:s");
             $quotation->{$this->db_filed['quotation_createby']} = 1;     
         
              $this->db->set($quotation)->insert($this->table);
 
            return $this->db->insert_id(); 
            
       }else{
           
             foreach($this->db_filed as $key=>$value){
                 if(isset($data[$value])) $quotation->${'value'} = $data[$value] ;
             }   
            $quotation->{$this->db_filed['quotation_updatedate']} = date("Y-m-d H:i:s");
            $quotation->{$this->db_filed['quotation_updateby']} =  1;
               
             return $this->db->where($this->key, $data['quotation_id'])
            ->set($quotation)
            ->update($this->table);     
       }                      
       
    } 
    
       function show_sourch($source=''){
           switch($source){
               case "customer" : return "ลูกค้า";
               case "supplier" : return "ตัวแทนจำหน่าย";
               case "subcontract" : return "ผู้รับเหมา";
           }
       }  
 }
?>


  <?php
  $this->load->model('quotation_mod', 'quotation');  
  $this->load->model('supplier/supplier_mod', 'supplier');  
  $this->load->model('subcontract/subcontract_mod', 'subcontract');
      $current_page = $this->uri->segment(1);  
       
     if(isset($purchase) && $purchase !="")  { $purchase= $purchase[0];      }
     $url_save = $id =="" ? base_url().$current_page."/save_purchase" : base_url().$current_page."/save_purchase/".@$service_id."/".@$purchase_id;
     
           
         
  
 ?>
                 <form id="add_purchase"   class="form-horizontal form-label-left"   method="post"  enctype="multipart/form-data" action="<?php echo $url_save?>">
                   <div class="modal-body">
                     
                                    <input type="hidden" id="service_id" name="service_id" value="<?php echo @$service_id?>"> 
                                    <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">วันที่ <span class="red">*</span> </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="purchase_date"  required    readonly="readonly"  name="purchase_date"  value="<?php echo getShowDateFormat(@$purchase->purchase_date)?>"    class="date-picker form-control col-md-7 col-xs-12"   >
                                      </div>
                                    </div>
                                     <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ประเภท <span class="red">*</span> </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="radio" class="purchase_type" name="purchase_type"  value="in"   <?php echo @$purchase->purchase_type=="in"? "checked":""?>  required /> เข้า:
                                          <input type="radio" class="purchase_type" name="purchase_type"  value="out"  <?php echo @$purchase->purchase_type=="out"? "checked":""?> /> ออก
                                      </div>
                                    </div>
                                    <?php
                                     $style_display2 = (@$purchase->purchase_type == "in") ? "display:none" : "";  
                                         
                                    ?>
                                     <div class="form-group" id="Pdiv_customer" style="<?php echo $style_display2?>" >     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">รับข้อมูลจาก</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="radio" class="purchase_source" name="purchase_source"  value="customer"    <?php echo @$purchase->purchase_source=="customer"? "checked":""?>  /> ลูกค้า 
                                          <input type="radio" class="purchase_source" name="purchase_source"  value="supplier"   <?php echo @$purchase->purchase_source=="supplier"? "checked":""?> /> ตัวแทนจำหน่าย
                                          <input type="radio" class="purchase_source" name="purchase_source"  value="subcontract"  <?php echo @$purchase->purchase_source=="subcontract"? "checked":""?> /> ผู้รับเหมา
                                       </div>
                                       <?php
                                         $display_customer = (isset($purchase->customer_id) && $purchase->customer_id !=0) ? "" : "display:none";     
                                        ?>   
                                          <div id="Parea_customer" class="Parea_opt col-md-6 col-sm-6 col-xs-12" style="<?php echo $display_customer?>">
                                                 <select  class="select2_single form-control people_option"  tabindex="-1" name="Pcustomer_id" id="Pcustomer_id" style="width:50%;">        
                                                            <option value="">เลือกข้อมูลลูกค้า</option>    
                                                              <?php 
                                                                   if(isset($customer)){
                                                                       foreach($customer as $key=>$val){
                                                                           $selected ="";
                                                                            if(isset($purchase->customer_id) ){
                                                                                     $selected = $purchase->customer_id == $val->customer_id ? "selected" : "";
                                                                            }   
                                                                              if($val->customer_visible ==STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->customer_id.'" '.$selected.'>'.$val->customer_company.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?>  
                                                </select>
                                                <!--<button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_customer"><i class="fa fa-plus"></i> เพิ่ม</button> -->
                                          </div>
                                          <?php       
                                               $display_supplier  = @$purchase->supplier_id !=0 && isset($purchase->supplier_id) ? "" : "display:none"  ; 
                                          ?>
                                          <div id="Parea_supplier" class="Parea_opt col-md-6 col-sm-6 col-xs-12" style="<?php echo $display_supplier?>">
                                                 <select  class="select2_single form-control people_option"  tabindex="-1" name="Psupplier_id" id="Psupplier_id" style="width:50%;"> 
                                                    <option value="">เลือกข้อมูล ผูู้แทนจำหน่าย</option>         
                                                        <?php 
                                                                     
                                                                   if(isset($supplier)){
                                                                       foreach($supplier as $key=>$val){
                                                                            
                                                                           $selected ="";
                                                                            if(isset($purchase->supplier_id) ){
                                                                                     $selected = $purchase->supplier_id == $val->supplier_id ? "selected" : "";
                                                                            }   
                                                                              if($val->supplier_visible == STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->supplier_id.'" '.$selected.'>'.$val->supplier_fname.'&nbsp;'.$val->supplier_lname.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?> 
                                                </select>
                                                <!--<button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_supplier"><i class="fa fa-plus"></i> Add</button>-->
                                          </div>
                                           <?php       
                                               $display_subcontract  = @$purchase->subcontract_id !=0 && isset($purchase->subcontract_id) ? "" : "display:none"  ; 
                                          ?>
                                          <div id="Parea_subcontract" class="Parea_opt col-md-6 col-sm-6 col-xs-12 " style="<?php echo $display_subcontract?>">
                                                 <select  class="select2_single form-control people_option"  tabindex="-1" name="Psubcontract_id" id="Psubcontract_id" style="width:50%;">        
                                                 <option value="">เลือกข้อมูล ผู้รับเหมา</option>
                                                         <?php 
                                                                   if(isset($subcontract)){
                                                                       foreach($subcontract as $key=>$val){
                                                                           $selected ="";
                                                                             if(isset($purchase->subcontract_id) ){
                                                                                     $selected = $purchase->subcontract_id == $val->subcontract_id ? "selected" : "";
                                                                            }    
                                                                              if($val->subcontract_visible ==STATUS_ACTIVE){
                                                                                echo '<option value="'.$val->subcontract_id.'" '.$selected.'>'.$val->subcontract_fname.'&nbsp;'.$val->subcontract_lname.'</option>';  
                                                                             }  
                                                                       }      
                                                                   }   
                                                              ?> 
                                                </select>
                                                <!--<button type="button" class="btn btn-dark btn-xs" data-toggle="modal" data-target=".add_subcontract"><i class="fa fa-plus"></i> เพิ่ม</button> -->
                                          </div>
                                      
                                    </div>
                                    <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">จำนวนเงิน <span class="red">*</span> </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                         <input type="number"   step="1"   id="purchase_amount" name="purchase_amount"    required value="<?php echo @$purchase->purchase_amount ?>" class="form-control col-md-7 col-xs-12" required="required" > 
                                      </div>
                                    </div>
                                     <div class="form-group">     
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ไฟล์</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">      
                                         <input type="file" name="purchase_file"   id ="purchase_file" accept="image/*,application/pdf">
                                           <?php if(@$purchase->purchase_path !="") { echo "<a href='".base_url(). $purchase->purchase_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?>
                                        
                                      </div> 
                                    </div>      
                      
                   </div>
                   <div class="modal-footer">
                    <span id="btn_psave"><button type="button"  class="btn btn-primary" id="save_purchase">บันทึก</button> </span>
                    <span id="btn_psubmit" style="display: none;"><button type="submit"  class="btn btn-primary" >บันทึก</button> </span> 
                     <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                  </div>    
                </div>    
                </form>
                
              <script type="text/javascript">
 
               $('.purchase_type').change(function() {
                  var val = $(this).val();
                  if(val== 'out') 
                      $('#Pdiv_customer').show();
                   else 
                    $('#Pdiv_customer').hide();
               });
               
               $('.purchase_source').change(function() {
                    
                        var val = $(this).val();       
                       $('.people_option').val('');
                       $('.Parea_opt').hide(); 
                       $('#Parea_'+val).show();
 
                      
               });
               
               $('#save_purchase').click(function(){
                         if($("#add_purchase").valid())
                             {
                           var file2 = new FormData();
                            var countfile = 0;
                                jQuery.each(jQuery('#purchase_file')[0].files, function(i, file) {
                                    file2.append(i, file);
                                    countfile=1;
                                });     
                          $.ajax({                            
                            url:"<?php echo base_url()."service/save_session/purchase"?>",
                            data: $('#add_purchase').serialize()+ '&service_code=' + $('#service_code').val(),
                            method: "POST",
                            success:function(result){ 
                               $('.add_purchase').modal('toggle');        
                               
                                    var text_type = ($('.purchase_type:checked').val() == "in") ? "เข้า" : "ออก" ;
                                    var type_input = ($('.purchase_source:checked').val());
                                    var text_source = "";                                                         
                                    var name = "";
                                    
                                    if($('.purchase_type:checked').val() == "in" ) type_input = "" ;
                                    
                                    if(type_input=="customer"){
                                             text_source = "ลูกค้า";
                                             name = $('#Pcustomer_id option:selected').text();
                                    }else if(type_input=="supplier"){
                                             text_source = "ผู้แทนจำหน่าย";
                                             name = $('#Psupplier_id option:selected').text();
                                    }else if(type_input=="subcontract"){ 
                                            text_source = "ผู้รับเหมา";
                                            name = $('#Psubcontract_id option:selected').text();
                                    } 
                                         
                                    var prows =  'Prows_'+result ;      
                                    var html_file = '';
                                    var q_date =  $('#purchase_date').val();
                                    var q_amount =  $('#purchase_amount').val();    
                                    
                                    // ------------upload file
                                    
                                    if(countfile!=0){
                                      $.ajax({
                                            url:"<?php echo base_url()."service/save_session/purchase/"?>"+result+"?type="+$('.purchase_type:checked').val(),
                                            data: file2,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            type: 'POST',
                                            success: function(data){
                                                 var q_data = jQuery.parseJSON(data);              
                                                 if(q_data.code==200){     
                                                     var img = '<a href="'+q_data.path+'"  target="_blank"><i class="fa fa-file"></i></a>';
                                                         $('#tb_purchase tr:last').after('<tr id="'+prows+'"><td>'+q_date+'</td><td>'+text_type+'</td><td>'+text_source+'</td><td>'+name+'</td><td>'+q_amount+'</td><td>'+img+'</td><td><a href="javascript:delete_purchase(\''+prows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                            '<input type="hidden" name="purchase_session[]" value="'+result+'">'+'</td></tr>');   
                                                 }else{       
                                                      $('#tb_purchase tr:last').after('<tr id="'+prows+'"><td>'+q_date+'</td><td>'+text_type+'</td><td>'+text_source+'</td><td>'+name+'</td><td>'+q_amount+'</td><td>'+img+'</td><td><a href="javascript:delete_purchase(\''+prows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                                      '<input type="hidden" name="purchase_session[]" value="'+result+'">'+'</td></tr>'); 
                                                 }
                                            }
                                        });       
                                    }else{
                                       $('#tb_purchase tr:last').after('<tr id="'+prows+'"><td>'+q_date+'</td><td>'+text_type+'</td><td>'+text_source+'</td><td>'+name+'</td><td>'+q_amount+'</td><td></td><td><a href="javascript:delete_purchase(\''+prows+'\','+result+')"   onclick="return confirm(\'คุณต้องการลบข้อมูล ?\')"  class="btn btn-danger btn-xs"> ลบ </a>'+
                                            '<input type="hidden" name="purchase_session[]" value="'+result+'">'+'</td></tr>'); 
                                    }
                                    $('#purchase_date').val('');       
                                    $('#purchase_amount').val('');
                                    $('#purchase_file').val('');
                                    $('#Parea_customer').hide();         
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');  
                               
                            }
                        }); 
                             }    
                        
              }) ;
              
                function delete_purchase(div_id,id){         

                        $.ajax({                            
                            url:"<?php echo base_url().$current_page."/delete_session/purchase/"?>"+(id-1),        
                            method: "POST",   
                            success:function(result){ 
                                $('#'+div_id).remove();    
                            }
                        }); 
               }
               
               $('.date-picker').daterangepicker({
           format: 'DD-MM-YYYY' ,
          singleDatePicker: true,
          calender_style: "picker_4"
        
      });
               </script>
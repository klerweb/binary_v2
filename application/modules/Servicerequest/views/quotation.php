 <?php
 $this->load->model('quotation_mod', 'quotation');  
  $this->load->model('supplier/supplier_mod', 'supplier');  
 $this->load->model('subcontract/subcontract_mod', 'subcontract');
 
  $current_page = $this->uri->segment(1);  
 ?>
        
<div class="row" id="area_form">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
                  <h2>ใบเสนอราคา</h2>&nbsp;  
                  <button type="button" class="btn btn-dark btn-xs btn_action open_add_quotation" ><i class="fa fa-plus"></i>  เพิ่ม</button> 
                  
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>  
                  </ul>
                  <div class="clearfix"></div>
            </div>
         <div class="x_content"> 
           
                        <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tb_quotaion">
 
                        <tr>
 
                              <td>วันที่</td>
                              <td widtd="5%">ประเภท</td>
                              <td>รับข้อมูลจาก</td>
                              <td>ชื่อ - นามสกุล</td>
                              <td>จำนวนเงิน</td>
                              <td widtd="3%">ไฟล์</td>
                              <td widtd="20%">จัดการ</td>
                            </tr>
                            <?php
                           if(isset($service_id) && @$service_id !="")
                             {
                             $quotations = $this->quotation->get_quotation($service_id);
                                foreach($quotations as $key=>$val){
                                    if($val->quotation_visible !=0)
                                    {
                                   ?>
                                   <tr>
                                      <td><?php echo getShowDateFormat($val->quotation_date)?></td>  
                                      <td><?php echo $val->quotation_type == "in" ? "เข้า" : "ออก" ?></td>
                                      <td>
                                        <?php echo $this->quotation->show_sourch($val->quotation_source)?>
                                        </td>
                                        <td>
                                        <?php
                                            if($val->customer_id !=0){echo $this->customer->get_customer($val->customer_id,'customer_company') ; }
                                            if($val->supplier_id !=0){
                                                $supplier_data = $this->supplier->get_supplier($val->supplier_id);
                                                echo $supplier_data->supplier_fname."&nbsp;".$supplier_data->supplier_lname;     
                                            }
                                            if($val->subcontract_id !=0){echo $this->subcontract->get_subcontract($val->subcontract_id,'subcontract_fname')."&nbsp;".$this->subcontract->get_subcontract($val->subcontract_id,'subcontract_lname') ; }
                                              
                                        ?>
                                         
                                      
                                      </td>
                                      <td><?php echo $val->quotation_amount?></td>
                                      <td><?php if($val->quotation_path) { echo "<a href='".base_url(). $val->quotation_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?></td>
                                      <td>
                                         <a class="btn btn-info btn-xs modal2" data-id="<?php echo $val->quotation_id?>"><i class="fa fa-pencil"></i> แก้ไข </a>
                                         <a href="<?php echo base_url().$current_page."/quotation_delete/".$service_id."/".$val->quotation_id?>"  onclick="return confirm('คุณต้องการลบข้อมูล ?')" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"></i> ลบ </a>
                                      </td>
                                    </tr>
                                   <?php 
                                    }
                                }
                                }
                          ?>   
 
                        </table>                                                                                                                                             
                    </div>
                  </div>
                </div>
</div>
 
           
          <div class="modal fade edit_quotation" tabindex="-1" role="dialog"  >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Update profile</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <span class='ShowData'> </span>
            </div>
        </div>
    </div>
    </div>         
          
            <?php  //echo $this->load->view('servicerequest/popup_supplier',array(),true);  ?>   
            <?php  //echo $this->load->view('servicerequest/popup_subcontract',array(),true);  ?>   

           <script type="text/javascript">
           
                    $('.modal2').click(function(){
                            var id = $(this).attr('data-id');
                            $('#quotation_form').html(' <div align="center"><p>Loading ........</p></div> ');
                            $('.add_quotation').modal('show');
                            $.ajax({
                            url:"<?php echo base_url().$current_page."/quotation_form/".$service_id."/"?>"+id,
                            method: "POST",
                            success:function(result){
                                $('#quotation_form').html(result);
                                $('#btn_qsave').hide();
                                $('#btn_qsubmit').show();
                                 
                                $('.add_quotation').modal('show');        
                            }      
                            });        
                        });

              $('.open_add_quotation').click(function(){
                      $('#quotation_date').val("");
                      $('#quotation_amount').val("");  
                      $('.quotation_type').prop('checked', false);
                      $('.quotation_source').prop('checked', false);
                      $('.area_opt').hide();
                      $('#div_customer').hide();  
                      $('#btn_qsave').show();
                      $('#btn_qsubmit').hide();
                      $('.add_quotation').modal('show');        
               });
               
              $('#save_customer').click(function(){   
                   var fname = $('#customer_fname').val();
                   var lname = $('#customer_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."customer/save_popup"?>",
                            data: $('#add_customer').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                    $('.add_customer').modal('toggle');   
                                             $('#customer_id').append($("<option></option>").attr("value",result_data.id).text($('#customer_fname').val()+" "+$('#customer_lname').val())); 
                                             $('#customer_id').val(result_data.id);
                                             $('#customer_id').trigger('change');     
                                             $(".add_customer input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;
              
              $('#save_supplier').click(function(){   
                   var fname = $('#supplier_fname').val();
                   var lname = $('#supplier_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."supplier/save_popup"?>",
                            data: $('#add_supplier').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                    $('.add_supplier').modal('toggle');   
                                             $('#supplier_id').append($("<option></option>").attr("value",result_data.id).text($('#supplier_fname').val()+" "+$('#supplier_lname').val())); 
                                             $('#supplier_id').val(result_data.id);
                                             $('#supplier_id').trigger('change');     
                                             $(".add_supplier input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;
              
              $('#save_subcontract').click(function(){   
                   var fname = $('#subcontract_fname').val();
                   var lname = $('#subcontract_lname').val();
                   
                   if( fname=='' ||   lname =="") {
                       alert('ไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้งค่ะ') ;
                  }else{  
                    $.ajax({
                            url:"<?php echo base_url()."subcontract/save_popup"?>",
                            data: $('#add_subcontract').serialize(),
                            method: "POST",
                            success:function(result){
                                     
                                var result_data = jQuery.parseJSON(result);  
                                if(result_data.code == 200){
                                        $('.add_subcontract').modal('toggle');   
                                             $('#subcontract_id').append($("<option></option>").attr("value",result_data.id).text($('#subcontract_fname').val()+" "+$('#subcontract_lname').val())); 
                                             $('#subcontract_id').val(result_data.id);
                                             $('#subcontract_id').trigger('change');     
                                             $(".add_subcontract input").val("");                                                                                                                                                              
                                    alert('บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
                                }else{
                                   alert('ขออภัยไม่สามารถบันทึกข้อมูลได้ค่ะ')  ;
                                }                                                    
                            }
                        });  
                  }      
              }) ;
              
              <?php
              if($this->input->get('view') && $qt_id ==""){?>
                $('#area_form').hide();   
                $(".btn_action").hide();
                $("input[type=search]").prop('disabled', false);
               <?php } ?> 
              <?php   if($this->input->get('view') && $qt_id !=""){?>
                $("input").prop('disabled', true);
                $("textarea").prop('disabled', true);
                $("select").prop('disabled', true);
                $(".btn").prop('disabled', true);     
                $(".btn_action").hide();
                $("input[type=search]").prop('disabled', false);     
                
                <?php }?>
              
               function edit_quotation(id_edit){
                   alert(id_edit);
               }
        </script>

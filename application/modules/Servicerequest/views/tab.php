<?php $url_servicerq = base_url()."servicerequest/";
$array_tab = array(
  "description"=>array("name"=>"Description","link"=>$url_servicerq."description"),
  "quotation"=>array("name"=>"Quotation","link"=>$url_servicerq."quotation"),
  "purchase"=>array("name"=>"Purchase","link"=>$url_servicerq."purchase"),
  "job"=>array("name"=>"Job","link"=>$url_servicerq."job"),
  "expenses"=>array("name"=>"Expenses","link"=>$url_servicerq."expenses"),
);

?>
<div class="row">
<div class="x_content">
  <div class="" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
     <?php
         foreach($array_tab as $key=>$val){
             $link = $id ? $val['link']."/".$id : "#";
             $active_tab = $tab==$key ? 'active' : "";
             $class_tab = ($active_tab=="" && $id =="") ? "disabled" :  $active_tab ;
             if($this->input->get('view')) $link=$link."?view=1";
             ?>
              <li role="presentation" class="<?php echo $class_tab?>"><a href="<?php echo $link ?>" id="<?php echo $key?>-tab"><?php echo $val['name']?></a></li>
             <?php
         }
     ?>
      
    </ul>
  </div>
</div>
</div>
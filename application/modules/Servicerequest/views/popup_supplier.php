<!-------------- popup customer ------------------->           

            <div class="modal fade add_supplier" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มข้อมูลตัวแทนจำหน่าย</h4>
                  </div>
                   <div class="modal-body">
                     <form id="add_supplier" data-parsley-validate  class="form-horizontal form-label-left" action="" method="post">   
                         
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อ <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="supplier_fname" name="supplier_fname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">นามสกุล <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="supplier_lname" name="supplier_lname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">เบอร์โทร</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="supplier_tel" name="supplier_tel" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">อีเมล์</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="supplier_email" name="supplier_email" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ที่อยู่</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">   
                            <textarea class="form-control" rows="3" name="supplier_address" id="supplier_address" required="required"></textarea>
                          </div>
                        </div>
                  </div>
                    <div class="modal-footer">
                    <button type="button"  class="btn btn-primary" id="save_supplier">บันทึก</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button> 
                  </div>

                </div>    
                </form>
              </div>
            </div>
           
       <!-------------- popup customer ------------------->
            <?php
            $this->load->model('customer/customer_mod','customer');    
            $customer = $this->customer->get_customer();
            ?>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">ลูกค้า</label>
              <div class="col-md-6 col-sm-6 col-xs-12">    
                      <select  class="select2_single form-control"  tabindex="-1" name="customer_id" id="cutomerOpt" required>  
                      <option value="">เลือกข้อมูลลูกค้า</option>    
                      <?php 
                           if(isset($customer)){
                               foreach($customer as $key=>$val){
                                   $selected ="";
                                    if(isset($customer_id) ){
                                            $selected =$customer_id == $val->customer_id ? "selected" : "";
                                    }   
                                      if($val->customer_visible ==STATUS_ACTIVE){
                                        echo '<option value="'.$val->customer_id.'" '.$selected.'>'.$val->customer_company.'</option>';  
                                     }  
                               }      
                           }   
                      ?>  
                       </select>                                       
              </div>
        </div>
        
       <script>
            $(document).ready(function() {
                $(".select2_single").select2({});    
                
                $('#cutomerOpt').change(function(){
 
                    if($(this).val() !=""){
                        $.ajax({
                            url:"<?php echo base_url()."servicerequest/customerData/"?>"+$(this).val()+"?ajax=1",
                            success:function(result){
                                $('.customer_branch_text').remove(); 
                                $('.customer_id_text').remove(); 
                                
                                $('#area_customer').show();   
                                $('#area_customer').html(result);   
                                   
                            }
                        });        
                    }else{                                                                                    
                       $('#area_customer').hide(); 
                    }
              }) ;
              
            });    
                      
       </script>
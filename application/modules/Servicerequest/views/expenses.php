     <?php
 
       $this->load->model('expenses_mod', 'expenses');   
 
     ?>
   
    <div class="row" id="area_form">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
            <div class="x_title">
                      <h2>ค่าใช้จ่าย</h2> &nbsp;&nbsp; <button type="button" class="btn btn-dark btn-xs btn_action" data-toggle="modal" data-target=".add_expenses"><i class="fa fa-plus"></i>  เพิ่ม</button> 
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>  
                      </ul>
                      <div class="clearfix"></div>
                </div>
             <div class="x_content">
                     <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tb_expenses">  
                            <tr>  
                                  <th>ชื่อ</th>
                                  <th width="20%">วันที่</th>
                                  <th width="10%">จำนวนเงิน</th>  
                                  <th width="10%">สถานะ</th>
                                  <th width="10%">ไฟล์</th>
                                  <th width="15%">จัดการ</th>
                                </tr> 
                                <?php                    
                                if(isset($service_id) && $service_id !=""){
                                    $expenses  = $this->expenses->get_expenses($service_id);
                                    foreach($expenses as $key=>$val){
                                          if($val->expenses_visible != STATUS_DISACTIVE){
                                                 ?> <tr> 
                                                      <td><?php echo $val->expenses_name?></td>
                                                      <td><?php echo getShowDateFormat($val->expenses_date)?></td>   
                                                      <td align="right"><?php echo number_format($val->expenses_amount,2)?></td>
                                                      <td><?php echo $this->expenses->get_expensesStatus($val->expenses_visible)?></td>   
                                                      <td><?php if($val->expenses_path) { echo "<a href='".base_url(). $val->expenses_path."' target='_blank'><i class='fa fa-download'></i></a>"; } ?></td>
                                                      <td>          
                                                         <?php if($val->expenses_visible==1) {?>
                                                        <a  href="<?php echo base_url()."servicerequest/expenses_delete/".$service_id."/".$val->expenses_id?>"  onclick="return confirm('คุณต้องการลบข้อมูล ?')" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"></i> ลบ </a>
                                                        <?php }?>
                                                      </td>
                                                    </tr>
                                                 <?php
                                          }
                                    }
                                }   
                              ?> 
                               
                            </table>  
                                                                                                                                                                                          
                        </div>
                      </div>
                    </div>
    </div>

      
                 
                <script type="text/javascript">
                          
             <?php
              if($this->input->get('view') && $expenses_id==""){?>
                $('#area_form').hide();   
                $(".btn_action").hide();
                $("input[type=search]").prop('disabled', false);  
               <?php } ?> 
              <?php   if($this->input->get('view') && $expenses_id !=""){?>
                $("input").prop('disabled', true);
                $("textarea").prop('disabled', true);
                $("select").prop('disabled', true);
                $(".btn").prop('disabled', true);     
                $(".btn_action").hide();
                $("input[type=search]").prop('disabled', false);     
                <?php }?>          
        </script>
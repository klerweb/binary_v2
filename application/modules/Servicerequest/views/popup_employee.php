<!-------------- popup employee ------------------->           

            <div class="modal fade add_employee" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">?</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มข้อมูลผู้รับเหมา</h4>
                  </div>
                   <div class="modal-body">
                     <form id="add_employee" data-parsley-validate  class="form-horizontal form-label-left" action="" method="post">   
                          
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อ <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="employee_fname" name="employee_fname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">นามสกุล <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="employee_lname" name="employee_lname"  required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
 
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">เบอร์โทร</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="employee_tel" name="employee_tel" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">     
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">อีเมล์</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="employee_email" name="employee_email" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
 
                  </div>
                    <div class="modal-footer">
                    <button type="button"  class="btn btn-primary" id="save_employee">บันทึก</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button> 
                  </div>

                </div>    
                </form>
              </div>
            </div>
           
       <!-------------- popup employee ------------------->
       <script>
        $('.date-picker').daterangepicker({
           format: 'DD-MM-YYYY' ,
          singleDatePicker: true,
        calender_style: "picker_4"
        
      });        
       </script>
<?php
class Product_mod extends MY_Model
{
	private $table = 'product';
	private $key = 'product_id';
	private $db_filed = array(
			'product_id' => 'product_id',
			'product_name' => 'product_name',
			'product_photo' => 'product_photo',
			'product_desc' => 'product_desc',
			'category_id' => 'category_id',
			'unit_id' => 'unit_id',
			'product_visible' => 'product_visible',
			'product_createby' => 'product_createby',
			'product_createdate' => 'product_createdate',
			'product_updateby' => 'product_updateby',
			'product_updatedate' => 'product_updatedate'
	);
	
	function get_dbfiled()
	{
		return  $this->db_filed;
	}
	
	function get_product($product_id=NULL, $filed=NULL)
	{
		$cfg->where['product_visible'] = '1';
	
		if($product_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $product_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else
		{
			return $this->get_data($this->table, '', $cfg);
		}
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("category", "category.category_id = product.category_id", "LEFT");
		$this->db->join("unit", "unit.unit_id = product.unit_id", "LEFT");
		$this->db->where("product.product_visible", "1");
	
		$this->db->order_by("product.product_id", "desc");
	
		$query = $this->db->get();
	
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	function save($data=NULL, $product_id=NULL)
	{
		$product = (object) array();
		if(!$product_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $product->${
					'value'} = $data[$value] ;
			}
			
			unset($product->{$this->db_filed['product_id']});
	
			$product->{$this->db_filed['product_visible']} = '1';
			$product->{$this->db_filed['product_createdate']} = date(DATETIME_FORMAT_2DB);
			$product->{$this->db_filed['product_createby']} = 1;
			$product->{$this->db_filed['product_updatedate']} = date(DATETIME_FORMAT_2DB);
			$product->{$this->db_filed['product_updateby']} = 1;
	
			$this->db->set($product)->insert($this->table);
				
			$id = $this->db->insert_id();
			
			if(!empty($_SESSION['product_lot']))
			{
				$this->load->model('product/product_lot_mod', 'product_lot');
				
				foreach ($_SESSION['product_lot'] as $lot)
				{
					unset($lot['product_lot_id']);
					
					$lot['product_id'] = $id;
					$this->product_lot->save($lot);
				}
			}
			
			return $id;
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $product->${
					'value'} = $data[$value] ;
		}
			
		$product->{$this->db_filed['product_updatedate']} = date(DATETIME_FORMAT_2DB);
		$product->{$this->db_filed['product_updateby']} = 1;
			
		return $this->db->where($this->key, $data['product_id'])
				->set($product)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['product_id'] = '';
		$data['product_name'] = '';
		$data['product_photo'] = '';
		$data['product_desc'] = '';
		$data['catogory_id'] = '';
		$data['unit_id'] = '';
		$data['product_visible'] = '1';
		
		return $data;
	}
}
?>
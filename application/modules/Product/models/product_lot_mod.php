<?php
class Product_lot_mod extends MY_Model
{
	private $table = 'product_lot';
	private $key = 'product_lot_id';
	private $db_filed = array(
			"product_lot_id" => "product_lot_id",
			"product_id" => "product_id",
			"product_lot_date" => "product_lot_date",
			"product_lot_amount" => "product_lot_amount",
			"product_lot_price" => "product_lot_price",
			"product_lot_desc" => "product_lot_desc",
            "product_lot_visible" => "product_lot_visible",
            "product_lot_createby" => "product_lot_createby",
            "product_lot_createdate" => "product_lot_createdate",
            "product_lot_updateby" => "product_lot_updateby",
            "product_lot_updatedate" => "product_lot_updatedate"
		);
                     
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_product_lot($product_lot_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['product_lot_visible'] = '1';
		
		if($product_lot_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $product_lot_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function get_lotByProduct($product_id)
	{
		$cfg->where['product_id'] = $product_id;
		$cfg->where['product_lot_visible'] = '1';
	
		return $this->get_data($this->table, $this->key, $cfg);
	}
	
	function save($data=NULL, $product_lot_id=NULL)
	{
		$product_lot = (object) array();
		if(!$product_lot_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $product_lot->${'value'} = $data[$value] ;
			}
			
			unset($product_lot->{$this->db_filed['product_lot_id']});
	
			$product_lot->{$this->db_filed['product_lot_visible']} = '1';
			$product_lot->{$this->db_filed['product_lot_createdate']} = date(DATETIME_FORMAT_2DB);
			$product_lot->{$this->db_filed['product_lot_createby']} = 1;
			$product_lot->{$this->db_filed['product_lot_updatedate']} = date(DATETIME_FORMAT_2DB);
			$product_lot->{$this->db_filed['product_lot_updateby']} = 1;
			 
			$this->db->set($product_lot)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $product_lot->${'value'} = $data[$value] ;
			}
			
			$product_lot->{$this->db_filed['product_lot_updatedate']} = date(DATETIME_FORMAT_2DB);
			$product_lot->{$this->db_filed['product_lot_updateby']} = 1;
			
			return $this->db->where($this->key, $data['product_lot_id'])
				->set($product_lot)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['product_lot_id'] = '';
		$data['product_id'] = '';
		$data['product_lot_date'] = date('d-m-Y');
		$data['product_lot_amount'] = "0";
		$data['product_lot_price'] = "0.00";
		$data['product_lot_desc'] = '';
		$data['product_lot_visible'] = '1';
		
		return $data;
	}
 }
?>
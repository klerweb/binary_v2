<table id="tblLot" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>รายการ</th>
			<th>วันที่</th>
			<th>จำนวน</th>
			<th>ราคาทุน</th>
			<th>รายละเอียด</th>
			<th>จัดการ</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			if(!empty($product_lot))
			{
				$i_lot = 1;
				foreach ($product_lot as $key => $lot)
				{
					$lot = (array)$lot;
					
					if($lot['product_id']!='')
					{
						$key = $lot['product_lot_id'];
						$type = 'edit';
					}
					else
					{
						$lot['product_lot_id'] = $key;
						$type = 'add';
					}
		?>
		<tr>
			<td>
				<?php echo $i_lot++; ?>
				<input type="hidden" id="lot_id_<?php echo $key; ?>" name="lot_id_<?php echo $key; ?>" value="<?php echo $lot['product_lot_id']; ?>" />
			</td>
			<td>
				<?php echo getShowDateFormat($lot['product_lot_date']); ?>
				<input type="hidden" id="lot_date_<?php echo $key; ?>" name="lot_date_<?php echo $key; ?>" value="<?php echo getShowDateFormat($lot['product_lot_date']); ?>" />
			</td>
			<td>
				<?php echo number_format(str_replace(',', '', $lot['product_lot_amount'])); ?>
				<input type="hidden" id="lot_amount_<?php echo $key; ?>" name="lot_amount_<?php echo $key; ?>" value="<?php echo number_format(str_replace(',', '', $lot['product_lot_amount'])); ?>" />
			</td>
			<td>
				<?php echo number_format(str_replace(',', '', $lot['product_lot_price']), 2); ?>
				<input type="hidden" id="lot_price_<?php echo $key; ?>" name="lot_price_<?php echo $key; ?>" value="<?php echo number_format(str_replace(',', '', $lot['product_lot_price']), 2); ?>" />
			</td>
			<td>
				<?php echo $lot['product_lot_desc']; ?>
				<input type="hidden" id="lot_desc_<?php echo $key; ?>" name="lot_desc_<?php echo $key; ?>" value="<?php echo $lot['product_lot_desc']; ?>" />
			</td>
			<td>
				<a href="#" onclick="edit_lot('<?php echo $key; ?>');" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข</a>
				<a href="#" onclick="del_lot('<?php echo $type; ?>', '<?php echo $key; ?>');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ</a>
			</td>
		</tr>
	<?php
			}
		}
	?>
	</tbody>
</table>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="frmProduct" name="frmProduct" method="post" enctype="multipart/form-data" action="<?php echo base_url().'product/save'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="product_id" name="product_id" value="<?php echo $product_id; ?>" />
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">สินค้า <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="product_name" name="product_name" class="form-control" value="<?php echo $product_name; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">หน่วยนับ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="unit_id" name="unit_id" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($unit))
										{
											foreach ($unit as $list_unit) 
											{
												$selected = $list_unit->unit_id==$unit_id? ' selected' : '';
												echo '<option value="'.$list_unit->unit_id.'"'.$selected.'>'.$list_unit->unit_name.'</option>';
											}
										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">หมวดหมู่ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select id="category_id" name="category_id" class="form-control" required>
								<option value="">กรุณาเลือก</option>
								<?php 
										if(!empty($category))
										{
											foreach ($category as $list_category) 
											{
												$selected = $list_category->category_id==$category_id? ' selected' : '';
												echo '<option value="'.$list_category->category_id.'"'.$selected.'>'.$list_category->category_name.'</option>';
											}
										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<textarea id="textarea" id="product_desc" name="product_desc"  name="textarea" class="form-control"><?php echo $product_desc; ?></textarea>
						</div>
					</div>
					<hr/>
					<span style="font-size: 18px; font-weight: 400;">รอบสินค้า</span>
					<a href="#" class="btn btn-dark btn-xs" data-toggle="modal" data-target="#modal-lot"><i class="fa fa-plus"></i> เพิ่ม</a>
					<div id="divLot" style="margin-top:10px;">
						<table id="tblLot" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>รายการ</th>
									<th>วันที่</th>
									<th>จำนวน</th>
									<th>ราคาทุน</th>
									<th>รายละเอียด</th>
									<th>จัดการ</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								if(!empty($product_lot))
									{
										$i_lot = 1;
										foreach ($product_lot as $key => $lot)
										{
											$lot = (array)$lot;
					
											if($lot['product_id']!='')
											{
												$key = $lot['product_lot_id'];
												$type = 'edit';
											}
											else
											{
												$lot['product_lot_id'] = $key;
												$type = 'add';
											}
								?>
								<tr>
									<td>
										<?php echo $i_lot++; ?>
										<input type="hidden" id="lot_id_<?php echo $key; ?>" name="lot_id_<?php echo $key; ?>" value="<?php echo $lot['product_lot_id']; ?>" />
									</td>
									<td>
										<?php echo getShowDateFormat($lot['product_lot_date']); ?>
										<input type="hidden" id="lot_date_<?php echo $key; ?>" name="lot_date_<?php echo $key; ?>" value="<?php echo getShowDateFormat($lot['product_lot_date']); ?>" />
									</td>
									<td>
										<?php echo number_format(str_replace(',', '', $lot['product_lot_amount'])); ?>
										<input type="hidden" id="lot_amount_<?php echo $key; ?>" name="lot_amount_<?php echo $key; ?>" value="<?php echo number_format(str_replace(',', '', $lot['product_lot_amount'])); ?>" />
									</td>
									<td>
										<?php echo number_format(str_replace(',', '', $lot['product_lot_price']), 2); ?>
										<input type="hidden" id="lot_price_<?php echo $key; ?>" name="lot_price_<?php echo $key; ?>" value="<?php echo number_format(str_replace(',', '', $lot['product_lot_price']), 2); ?>" />
									</td>
									<td>
										<?php echo $lot['product_lot_desc']; ?>
										<input type="hidden" id="lot_desc_<?php echo $key; ?>" name="lot_desc_<?php echo $key; ?>" value="<?php echo $lot['product_lot_desc']; ?>" />
									</td>
									<td>
										<a href="#" onclick="edit_lot('<?php echo $key; ?>');" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข</a>
										<a href="#" onclick="del_lot('<?php echo $type; ?>', '<?php echo $key; ?>');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ</a>
									</td>
								</tr>
							<?php
									}
								}
							?>
							</tbody>
						</table>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>product';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="modal-lot" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form id="frmLot" name="frmLot" method="post" enctype="multipart/form-data" action="<?php echo base_url().'product/save_lot'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="product_id" name="product_id" value="<?php echo $product_id; ?>" />
					<input type="hidden" id="product_lot_id" name="product_lot_id" value="" />
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">วันที่ <span class="red">*</span></label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="product_lot_date" name="product_lot_date" class="form-control" value="<?php echo $lot_modal['product_lot_date']; ?>" required readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">จำนวน <span class="red">*</span></label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="product_lot_amount" name="product_lot_amount" class="form-control" value="<?php echo $lot_modal['product_lot_amount']; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">ราคา <span class="red">*</span></label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="product_lot_price" name="product_lot_price" class="form-control" value="<?php echo $lot_modal['product_lot_price']; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12">รายละเอียด</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<textarea id="product_lot_desc" name="product_lot_desc"  name="textarea" class="form-control"><?php echo $lot_modal['product_lot_desc']; ?></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnLot" class="btn btn-success">บันทึก</button>
				<button type="button" id="btnLotCancel" class="btn btn-primary">ยกเลิก</button>
			</div>
		</div>
	</div>
</div>
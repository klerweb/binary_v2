<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MX_Controller 
{
	public $title = 'สินค้า';
	public $menu = array('menu' => 'store', 'sub' => 'product');
	
	function __construct()
	{
		parent::__construct();
	
		if(empty($_SESSION['binary_login']))
		{
			header('Location: '.base_url());
			exit();
		}
	
		$this->load->model('product/product_mod', 'product');
		$this->load->model('product/product_lot_mod', 'product_lot');
		$this->load->model('category/category_mod', 'category');
		$this->load->model('unit/unit_mod', 'unit');
	}
	
	public function index()
	{
		$isSave = $this->input->get('isStatus');
		$popup_alert = '';
		if($isSave!='')
		{
			switch ($isSave)
			{
				case 'success_add': $popup_alert = popup_alert('success', 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_add': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถบันทึกข้อมูลได้');
					break;
				case 'success_edit': $popup_alert = popup_alert('success', 'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_edit': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_pass': $popup_alert = popup_alert('success', 'เปลี่ยนรหัสเรียบร้อยแล้วค่ะ');
					break;
				case 'error_pass': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถแก้ไขข้อมูลได้');
					break;
				case 'success_del': $popup_alert = popup_alert('success', 'ลบข้อมูลเรียบร้อยแล้วค่ะ');
					break;
				case 'error_del': $popup_alert = popup_alert('error', 'ขออภัยค่ะ ไม่สามารถลบข้อมูลได้');
					break;
			}
		}
	
		$data_content = array(
				'popup_alert' => $popup_alert,
				'product' => $this->product->all()
		);
	
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_product/product.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('index', $data_content, TRUE),
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function lot($id='')
	{
		if($id=='')
		{
			$data_content['product_lot'] = $_SESSION['product_lot'];
		}
		else
		{
			$data_content['product_lot'] = $this->product_lot->get_lotByProduct($id);
		}
	
		echo $this->parser->parse('lot', $data_content, TRUE);
	}
	
	public function form($id='')
	{
		if($id=='')
		{
			$data_content = $this->product->model();
			
			unset($_SESSION['product_lot']);
			
			$_SESSION['product_lot'] = array();
			$data_content['product_lot'] = $_SESSION['product_lot'];
		}
		else
		{
			$data_content = (array)$this->product->get_product($id);
			$data_content['product_lot'] = $this->product_lot->get_lotByProduct($id);
		}
	
		$data_content['lot_modal'] = $this->product_lot->model();
		
		$data_content['unit'] = $this->unit->get_unit();
		$data_content['category'] = $this->category->get_category();
	
		$data = array(
				'title' => $this->title,
				'title_sub' => '',
				'js_other' => array('modules_product/form.js'),
				'menu' => $this->parser->parse('page/menu', $this->menu, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE),
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$data = $this->input->post();
	
		if($data['product_id']=='')
		{
			$data['product_id'] = $this->product->save($data);
			$isSave = is_numeric($data['product_id'])? 'success_add' : 'error_add';
		}
		else
		{
			$isSave = $this->product->save($data, $data['product_id'])? 'success_edit' : 'error_edit';
		}
	
		redirect(base_url().'product?isStatus='.$isSave);
	}
	
	public function del($id)
	{
		$data['product_id'] = $id;
		$data['product_visible'] = '0';
	
		$isSave = $this->product->save($data, $data['product_id'])? 'success_del' : 'error_del';
	
		redirect(base_url().'product?isStatus='.$isSave);
	}
	
	public function del_all()
	{
		$isSave = 'error_del';
	
		if(!$this->input->post('chk'))
		{
			foreach ($this->input->post('chk') as $value)
			{
				$data['product_id'] = $value;
				$data['product_visible'] = '0';
	
				$this->product->save($data, $data['product_id']);
			}
				
			$isSave = 'success_del';
		}
	
		redirect(base_url().'product?isStatus='.$isSave);
	}
	
	public function save_lot()
	{
		$data = $this->input->post();
		
		$data['product_lot_date'] = setDateToDB($data['product_lot_date']);
		$data['product_lot_amount'] = str_replace(',', '', $data['product_lot_amount']);
		$data['product_lot_price'] = str_replace(',', '', $data['product_lot_price']);
	
		if($data['product_id']=='')
		{
			if($data['product_lot_id']=='')
			{
				$_SESSION['product_lot'][] = $data;
	
				echo 'success_add';
			}
			else
			{
				$_SESSION['product_lot'][$data['product_lot_id']] = $data;
	
				echo 'success_edit';
			}
		}
		else
		{	
			if($data['product_lot_id']=='')
			{
				$data['product_lot_id'] = $this->product_lot->save($data);
				echo is_numeric($data['product_lot_id'])? 'success_add' : 'error_add';
			}
			else
			{
				echo $this->product_lot->save($data, $data['product_lot_id'])? 'success_edit' : 'error_edit';
			}
		}
	}
	
	public function del_lot()
	{
		$data = $this->input->post();
	
		if($data['type']=='add')
		{
			unset($_SESSION['product_lot'][$data['id']]);
				
			echo 'success_del';
		}
		else
		{
			$data['product_lot_id'] = $data['id'];
			$data['product_lot_visible'] = '0';
				
			echo $this->product_lot->save($data, $data['product_lot_id'])? 'success_del' : 'error_del';
		}
	}
}
?>
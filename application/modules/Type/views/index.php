<div class="row"></div>
<div class="popup_alert"><?php echo $popup_alert; ?></div>    
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_content">
				<form id="frmType" name="frmType" method="post" action="<?php echo base_url(); ?>type/del_all" onsubmit="return confirm('คุณต้องการลบข้อมูล?');">
					<button type="button" class="btn btn-dark btn-sm" onclick="window.location.href='<?php echo base_url(); ?>type/form';"><i class="fa fa-plus"></i> เพิ่ม</button>
					<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i> ลบ</button>
					<hr/>
					<table id="tblType" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="padding-left:0px; padding-right:0px;"><center><input type="checkbox" id="chkAll" name="chkAll" class="flat" /></center></th>
								<th>ลำดับ</th>
								<th>ประเภทบริการ</th>
								<th>จัดการ</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							if(!empty($type))
							{ 
								$i_type = 1;
								foreach ($type as $list_type)
								{
						?>
							<tr>
								<td align="center"><input type="checkbox" name="chk[]" class="flat chk" value="<?php echo $list_type->type_id; ?>" /></td>
								<td align="center"><?php echo $i_type++; ?></td>
								<td><?php echo $list_type->type_name; ?></td>
								<td>   
	                                <a href="<?php echo base_url().'type/form/'.$list_type->type_id; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข</a>  
	                                <a href="<?php echo base_url().'type/del/'.$list_type->type_id; ?>" onclick="return confirm('คุณต้องการลบข้อมูล?');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ</a>
	                              </td>
							</tr>
						<?php 
								}
							} 
						?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
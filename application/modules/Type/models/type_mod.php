<?php
class Type_mod extends MY_Model
{
	private $table = "type";
	private $key = "type_id";
	private $db_filed = array(
			"type_id" => "type_id",
			"type_name" => "type_name",
			"type_visible" => "type_visible",
			"type_createby" => "type_createby",
			"type_createdate" => "type_createdate",
			"type_updateby" => "type_updateby",
			"type_updatedate" => "type_updatedate"
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_type($type_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['type_visible'] = '1';
		
		if($type_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $type_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function save($data=NULL, $type_id=NULL)
	{
		$type = (object) array();
		if(!$type_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $type->${'value'} = $data[$value] ;
			}
			
			unset($type->{$this->db_filed['type_id']});
	
			$type->{$this->db_filed['type_visible']} = '1';
			$type->{$this->db_filed['type_createdate']} = date(DATETIME_FORMAT_2DB);
			$type->{$this->db_filed['type_createby']} = 1;
			$type->{$this->db_filed['type_updatedate']} = date(DATETIME_FORMAT_2DB);
			$type->{$this->db_filed['type_updateby']} = 1;
			 
			$this->db->set($type)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $type->${'value'} = $data[$value] ;
			}
			
			$type->{$this->db_filed['type_updatedate']} = date(DATETIME_FORMAT_2DB);
			$type->{$this->db_filed['type_updateby']} = 1;
			
			return $this->db->where($this->key, $data['type_id'])
				->set($type)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['type_id'] = '';
		$data['type_name'] = '';
		$data['type_visible'] = '1';
		
		return $data;
	}
}
?>
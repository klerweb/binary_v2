<?php
class Category_mod extends MY_Model
{
	private $table = "category";
	private $key = "category_id";
	private $db_filed = array(
			"category_id" => "category_id",
			"category_name" => "category_name",
			"category_desc" => "category_desc",
			"category_parent" => "category_parent",
			"category_visible" => "category_visible",
			"category_createby" => "category_createby",
			"category_createdate" => "category_createdate",
			"category_updateby" => "category_updateby",
			"category_updatedate" => "category_updatedate"
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_category($category_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['category_visible'] = '1';
		
		if($category_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $category_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
	
	function save($data=NULL, $category_id=NULL)
	{
		$category = (object) array();
		if(!$category_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $category->${'value'} = $data[$value] ;
			}
			
			unset($category->{$this->db_filed['category_id']});
	
			$category->{$this->db_filed['category_visible']} = '1';
			$category->{$this->db_filed['category_createdate']} = date(DATETIME_FORMAT_2DB);
			$category->{$this->db_filed['category_createby']} = 1;
			$category->{$this->db_filed['category_updatedate']} = date(DATETIME_FORMAT_2DB);
			$category->{$this->db_filed['category_updateby']} = 1;
			 
			$this->db->set($category)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $category->${'value'} = $data[$value] ;
			}
			
			$category->{$this->db_filed['category_updatedate']} = date(DATETIME_FORMAT_2DB);
			$category->{$this->db_filed['category_updateby']} = 1;
			
			return $this->db->where($this->key, $data['category_id'])
				->set($category)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['category_id'] = '';
		$data['category_name'] = '';
		$data['category_desc'] = '';
		$data['category_parent'] = '';
		$data['category_visible'] = '1';
		
		return $data;
	}
}
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>รายละเอียด</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="frmCategory" name="frmCategory" method="post" enctype="multipart/form-data" action="<?php echo base_url().'category/save'; ?>" class="form-horizontal form-label-left"  >
					<input type="hidden" id="category_id" name="category_id" value="<?php echo $category_id; ?>" />
					<input type="hidden" id="category_parent" name="category_parent" value="<?php echo $category_parent; ?>" />
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">หมวดหมู่ <span class="red">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="category_name" name="category_name" class="form-control" value="<?php echo $category_name; ?>" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียด</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<textarea id="textarea" id="category_desc" name="category_desc"  name="textarea" class="form-control"><?php echo $category_desc; ?></textarea>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="submit" class="btn btn-success">บันทึก</button>
							<button type="button" onclick="window.location.href='<?php echo base_url(); ?>category';" class="btn btn-primary">ยกเลิก</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="row"></div>
<div class="popup_alert"><?php echo $popup_alert; ?></div>    
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_content">
				<form id="frmCategory" name="frmCategory" method="post" action="<?php echo base_url(); ?>category/del_all" onsubmit="return confirm('คุณต้องการลบข้อมูล?');">
					<button type="button" class="btn btn-dark btn-sm" onclick="window.location.href='<?php echo base_url(); ?>category/form';"><i class="fa fa-plus"></i> เพิ่ม</button>
					<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i> ลบ</button>
					<hr/>
					<table id="tblCategory" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="padding-left:0px; padding-right:0px;"><center><input type="checkbox" id="chkAll" name="chkAll" class="flat" /></center></th>
								<th>ลำดับ</th>
								<th>หมวดหมู่</th>
								<th>จัดการ</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							if(!empty($category))
							{ 
								$i_category = 1;
								foreach ($category as $list_category)
								{
						?>
							<tr>
								<td align="center"><input type="checkbox" name="chk[]" class="flat chk" value="<?php echo $list_category->category_id; ?>" /></td>
								<td align="center"><?php echo $i_category++; ?></td>
								<td><?php echo $list_category->category_name; ?></td>
								<td>   
	                                <a href="<?php echo base_url().'category/form/'.$list_category->category_id; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข</a>  
	                                <a href="<?php echo base_url().'category/del/'.$list_category->category_id; ?>" onclick="return confirm('คุณต้องการลบข้อมูล?');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ</a>
	                              </td>
							</tr>
						<?php 
								}
							} 
						?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
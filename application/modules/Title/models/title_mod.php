<?php
class Title_mod extends MY_Model
{
	private $table = "title";
	private $key = "title_id";
	private $db_filed = array(
			"title_id" => "title_id",
			"title_name" => "title_name"
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_title($title_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		if($title_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $title_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg) ;
		}
	}
}
?>
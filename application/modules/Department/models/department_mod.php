<?php
class Department_mod extends MY_Model
{
	private $table = 'department';
	private $key = 'department_id';
	private $db_filed = array(
			'department_id' => 'department_id',
			'department_name' => 'department_name',
			'department_visible' => 'department_visible',
			'department_createby' => 'department_createby',
			'department_createdate' => 'department_createdate',
			'department_updateby' => 'department_updateby',
			'department_updatedate' => 'department_updatedate'
	);
	 
	function get_dbfiled()
	{
		return $this->db_filed;
	}
	
	function get_department($department_id=NULL, $filed=NULL, $cfg=NULL)
	{	 
		$cfg->where['department_visible'] = '1';
		
		if($department_id)
		{
			$data =  $this->get_data($this->table, $this->key, $cfg, $department_id);
			if($filed)
			{
				return $data->{$this->db_filed[$filed]};
			}
			else
			{
				return  $data ;
			}
		}
		else 
		{
			return $this->get_data($this->table, '', $cfg);
		}
	}
	
	function save($data=NULL, $department_id=NULL)
	{
		$department = (object) array();
		if(!$department_id)
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $department->${'value'} = $data[$value] ;
			}
			
			unset($department->{$this->db_filed['department_id']});
	
			$department->{$this->db_filed['department_visible']} = '1';
			$department->{$this->db_filed['department_createdate']} = date(DATETIME_FORMAT_2DB);
			$department->{$this->db_filed['department_createby']} = 1;
			$department->{$this->db_filed['department_updatedate']} = date(DATETIME_FORMAT_2DB);
			$department->{$this->db_filed['department_updateby']} = 1;
			 
			$this->db->set($department)->insert($this->table);
			
			return $this->db->insert_id();
		}
		else
		{
			foreach($this->db_filed as $key=>$value)
			{
				if(isset($data[$value])) $department->${'value'} = $data[$value] ;
			}
			
			$department->{$this->db_filed['department_updatedate']} = date(DATETIME_FORMAT_2DB);
			$department->{$this->db_filed['department_updateby']} = 1;
			
			return $this->db->where($this->key, $data['department_id'])
				->set($department)
				->update($this->table);
		}
	}
	
	function model()
	{
		$data['department_id'] = '';
		$data['department_name'] = '';
		$data['department_visible'] = '1';
		
		return $data;
	}
}
?>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    function debug($debugged){
		echo '<pre style="text-align:left; font-family: tahoma; font-size: 13px;">';
		print_r($debugged);
		echo '</pre>';
    }
    
    function setFormatDate($date){
         if(strstr($date,"-"))   list($day,$month,$year) = explode('-',$date);
         if(strstr($date,"/"))   list($day,$month,$year) = explode('/',$date);
         return   $year."-".$month."-".$day;
    }
    
    function isDateNULL($date='0000-00-00'){
       return (!(int)strtotime($date) || strtotime($date) <0 ) ? true  : false;
    }
    
    function getShowDateTimeFormat($date='0000-00-00'){
        return (!isDateNULL($date)) ? date(DATETIME_FORMATSHOW,strtotime($date)) : $date ; 
    }
    
    function getShowDateFormat($date='0000-00-00'){
        return (!isDateNULL($date)) ? date(DATE_FORMATSHOW,strtotime($date)) : $date ; 
    }
    
    function getShowDateFormat2($date='0000-00-00'){
        return (!isDateNULL($date)) ? date(DATE_FORMATSHOW2,strtotime($date)) : $date ; 
    }
    function setDateToDB($date ='00-00-0000'){
        $return_date =  (!isDateNULL(setFormatDate($date))) ? date(DATE_FORMAT_2DB,strtotime(setFormatDate($date))) : setFormatDate($date) ; 
        return $return_date;
    }
    function setDateTimeToDB($date =''){                        
                        
        $return_date =  (!isDateNULL($date)) ? date(DATETIME_FORMAT_2DB,strtotime($date)) : $date ; 
        return $return_date;
    }
    
    function popup_alert($type='',$message=''){
         
        $array = array("success","info","warning","error");
        
        
        if(in_array($type,$array)){
                       if($type=="error") $type = "danger" ;  
                        $html='<div class="alert alert-'.$type.' alert-dismissible fade in" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                  </button>
                                     '.$message.'
                                </div>';
           
        }
        return $html;
    }
    
    
function service_format($number, $len=5)
{
	$len_number = strlen($number);
    return $len_number>=$len? $len :str_repeat('0', $len - $len_number).$number;
 }
 
     function service_format2($number, $len=5)
    {
            return "R".date("Y")."/".date("m")."-".service_format($number,$len);
     }
	
 
/* End of file common_helper.php */
/* Location: ./system/helpers/common_helper.php */

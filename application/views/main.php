<!DOCTYPE html>
<html lang="en">
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Binary System</title>
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/maps/jquery-jvectormap-2.0.3.css" />
    <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet" />    
    <link href="<?php echo base_url(); ?>assets/css/floatexamples.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/select/select2.min.css" rel="stylesheet"> 
    <!-- switchery -->
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/switchery/switchery.min.css" />
    <link href="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  	<link href="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  	<link href="<?php echo base_url(); ?>assets/js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  	<link href="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  	<link href="<?php echo base_url(); ?>assets/js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <style>
		label.error { color: #E74C3C; }
		input.error { border: 1px dotted red; }
	</style>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    
	<!-- Datatables-->
    <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/moment/moment.min.js"></script>   
    <script src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js" ></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/additional-methods.js"></script>
    <script type="text/javascript">
    	var base_url = "<?php echo base_url(); ?>";
    </script>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
     		<div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>Binary System</span></a>
                    </div>
                    <div class="clearfix"></div>
                    	<!-- menu prile quick info -->
                    		<div class="profile">
                    			<div class="profile_pic">
                            		<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="..." class="img-circle profile_img">
                            	</div>
                            	<div class="profile_info">
                            		<span>ยินดีต้อนรับ,</span>
                            		<h2>Empty user</h2>
                            	</div>
                            </div>
                           <!-- /menu prile quick info -->
                    <br />
                    {menu}
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class="right_col" role="main">
            	<div class="">
            		<div class="page-title">
            			<div class="title_left">
            				<h3>{title}<small>{title_sub}</small></h3>
            			</div>
            		</div>
         			{content}
					<br />
				</div>
			</div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">© Copyright 2017 Binary-Power. All Rights Reserved.</div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <!-- /footer content -->
	<!-- pace -->
    <script src="<?php echo base_url(); ?>assets/js/pace/pace.min.js"></script>
    <!-- icheck -->
  	<script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
    <!-- switchery -->
  	<script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
  	<!-- form validation -->
  	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
  	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.redirect.js"></script>
  	<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
  	<!-- JS Other -->
  	<?php if (isset($js_other)): ?>
  		<?php foreach ($js_other as $js): ?>
  			<script src="<?php echo base_url("assets/js/{$js}"); ?>"></script>
  		<?php endforeach; ?>
  	<?php endif; ?>
  	<script type="text/javascript">
		function logout()
		{
			alert("คุณได้ออกจากระบบเรียบร้อยแล้ว");
			$.redirect("<?php echo base_url()?>home/logout");
		}
  	</script>
</body>

</html>

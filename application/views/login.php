<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Binary System</title>
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">
	<!-- Custom styling plus plugins -->
	<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="<?php echo base_url(); ?>assets/assets/js/ie8-responsive-file-warning.js"></script>
	<![endif]-->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
    	var base_url = "<?php echo base_url(); ?>";
    </script>
</head>
<body style="background:#F7F7F7;">
	<div class="">
		<a class="hiddenanchor" id="toregister"></a>
    	<a class="hiddenanchor" id="tologin"></a>
    	<div id="wrapper">
    		<div id="login" class="animate form">
    			<section class="login_content">
    				<form id="frmLogin" name="frmLogin" method="post" data-parsley-validate>
    					<h1>Login Form</h1>
    					<div>
    						<input type="text" id="username" name="username" class="form-control" placeholder="Username" required="" />
    					</div>
	    				<div>
	    					<input type="password" id="password" name="password" class="form-control" placeholder="Password" required="" />
	            		</div>
	            		<div>
	            			<a id="btnSubmit" class="btn btn-default submit" href="#">Log in</a>
	            		</div>
	            		<div class="clearfix"></div>
	            		<div class="separator">
	            			<div>
								<p>© Copyright 2017 Binary-Power. All Rights Reserved.</p>
							</div>
						</div>
					</form>
					<!-- form -->
				</section>
				<!-- content -->
			</div>
		</div>
	</div>
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
	<script type="text/javascript">
		$("#btnSubmit").click(function() {
			if($("#frmLogin").parsley().validate())
			{
				$.post(base_url+"/home/check_login", { username: $("#username").val(), password: $("#password").val() }, function(data) {
					if(data=="200")
					{
						alert("คุณได้เข้าสู่ระบบเรียบร้อยแล้ว");
						window.location.href = base_url+"dashboard";
					}
					else
					{
						alert(data);
					}
				});
			}
		});
	</script>
</body>
</html>